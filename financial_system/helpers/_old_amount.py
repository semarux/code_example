
def get_convert_amount(amount, from_currency, to_currency):
    amount_in_main_currency = float(amount) / (from_currency.rate / from_currency.multiplicity)
    return Decimal(amount_in_main_currency * (to_currency.rate/to_currency.multiplicity))._rescale(-4, "ROUND_DOWN")


def get_commission_setting(user, payment_system, action_type):
    commission_setting = get_obj_or_none(CommissionSetting, action_type=action_type, payment_system=payment_system, payment_coupon_kind=None)
    try:
        escrow = user.profileaccountsettings.escrow
    except:
        pass
    else:
        commission_setting_e = get_obj_or_none(CommissionSetting, action_type=action_type, payment_system=payment_system, payment_coupon_kind="E")
        if commission_setting_e:
            commission_setting = commission_setting_e
    return commission_setting


def get_commission(amount, currency, to_currency, user, payment_system, action_type):
    getcontext().prec = 4
    commission_setting = get_commission_setting(user, payment_system, action_type)
    if commission_setting:
        if commission_setting.commissionrange_set.all().count() != 0:
            amount_in_main_currency = Decimal(amount)/Decimal(currency.rate/currency.multiplicity)
            given_amount = Decimal(amount_in_main_currency)*Decimal(commission_setting.currency_commission_range.rate/commission_setting.currency_commission_range.multiplicity)
            try:
                commission_range = CommissionRange.objects.get(commission_setting=commission_setting, min_amount__lt=given_amount, max_amount__gte=given_amount)
            except:
                commission_range = CommissionRange.objects.filter(commission_setting=commission_setting).latest("id")

            if commission_setting.type_commission == "P":
                return Decimal(float(float(amount_in_main_currency)*float(to_currency.rate/to_currency.multiplicity))*float(commission_range.commission/100))._rescale(-4, "ROUND_DOWN")
                # return Decimal(Decimal(Decimal(amount_in_main_currency)*Decimal(to_currency.rate/to_currency.multiplicity))*(commission_range.commission/100))
            elif commission_setting.type_commission == "M":
                # amount_commission_range_in_main_currency = Decimal(Decimal(commission_range.commission)/(Decimal(commission_setting.currency_commission_range.rate)/Decimal(commission_setting.currency_commission_range.multiplicity)))
                # return Decimal(amount_commission_range_in_main_currency*(Decimal(to_currency.rate)/Decimal(to_currency.multiplicity)))
                amount_commission_range_in_main_currency = float(float(commission_range.commission)/(float(commission_setting.currency_commission_range.rate)/float(commission_setting.currency_commission_range.multiplicity)))
                return Decimal((amount_commission_range_in_main_currency*(float(to_currency.rate)/float(to_currency.multiplicity))))._rescale(-4, "ROUND_DOWN")
            else:
                return 0
        else:
            return 0
    else:
        return 0


def get_billing_information(came_amount, currency, ps_currency, user, payment_system, type_payment):
    amount = get_convert_amount(Decimal(came_amount)._rescale(-4, "ROUND_DOWN"), currency, ps_currency)
    commission = get_commission(Decimal(came_amount)._rescale(-4, "ROUND_DOWN"), currency, ps_currency, user, payment_system, type_payment)
    # payment_amount_real = Decimal(float(amount) + float(commission))._rescale(-4, "ROUND_UP")
    payment_amount_real = Decimal(float(amount) + float(commission))._rescale(-4, "ROUND_DOWN")
    payment_amount = Decimal(payment_amount_real)._rescale(-2, "ROUND_UP")
    # amount_real = get_convert_amount(Decimal(float(payment_amount._rescale(-4, "ROUND_DOWN")) -  float(commission))._rescale(-4, "ROUND_DOWN"), ps_currency, currency)
    amount_real = get_convert_amount((float(payment_amount) - float(commission)), ps_currency, currency)
    # amount_with_commission = get_convert_amount(Decimal(payment_amount)._rescale(-4, "ROUND_DOWN"), ps_currency, currency)
    amount_with_commission = get_convert_amount(payment_amount, ps_currency, currency)

    return {
        "amount": amount,
        "commission": commission,
        "payment_amount_real": payment_amount_real,
        "payment_amount": Decimal(payment_amount_real)._rescale(-2, "ROUND_UP"),
        "amount_real": amount_real,
        "amount_with_commission": amount_with_commission
    }