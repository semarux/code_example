# --coding: utf-8
import datetime
import calendar


class DateTampering:
    somedate = datetime.datetime.today()

    def __init__(self, somedate):
        self.somedate = somedate

    def next_day(self):
        date = self.somedate
        date += datetime.timedelta(days=1)
        return date

    def next_week(self):
        date = self.somedate
        date += datetime.timedelta(weeks=1)
        return date

    def next_mounth(self):
        return self.add_months(1)

    def next_quarter(self):
        return self.add_months(3)

    def next_year(self):
        return datetime.date(self.somedate.year + 1, self.somedate.month, self.somedate.day)

    def add_months(self, months):
        month = self.somedate.month - 1 + months
        year = self.somedate.year + month / 12
        month = month % 12 + 1
        day = min(self.somedate.day, calendar.monthrange(year, month)[1])
        return datetime.date(year, month, day)

    def get_modified_date(self, key="D"):
        if key == "D":
            return self.next_day()
        elif key == "W":
            return self.next_week()
        elif key == "M":
            return self.next_mounth()
        elif key == "Q":
            return self.next_quarter()
        elif key == "Y":
            return self.next_year()
        else:
            return self.somedate
