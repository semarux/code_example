# --coding: utf-8
from django.shortcuts import _get_queryset

def get_obj_or_none(klass, *args, **kwargs):
    queryset = _get_queryset(klass)
    try:
        return queryset.get(*args, **kwargs)
    except:
        return None


def get_current_site_id():
    from django.contrib.sites.models import Site
    return Site.objects.get_current().id


from torgplace import settings
def save_data_to_file(file_name="file.txt", data_dict={}):
    f = open(settings.MEDIA_ROOT+"/" + file_name, "w+")
    for key in data_dict:
        f.write("%s = %s \n" % (key, data_dict[key]))
    f.close()
    return "Save"