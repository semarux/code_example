# --coding: utf-8
from datetime import datetime
from django.http import HttpResponse
from django.shortcuts import _get_queryset
from django.utils.translation import ugettext_lazy as _
import xlwt
from main.tasks import send_inform_mail
from financial_system.helpers.system_functs import get_obj_or_none
from decimal import *
from financial_system.models import *
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.sites.models import get_current_site
from django.db import transaction
from torgplace import settings
from torgplace.settings import MAIN_PAYMENT_SYSTEM
from financial_system.helpers.system_functs import get_current_site_id


currency_list = Currency.objects.all().values("id", "rate", "multiplicity")


# def get_convert_amount1(amount, from_currency, to_currency):
#     getcontext().prec = 4
#     amount_in_main_currency = Decimal(amount)/Decimal(from_currency.rate/from_currency.multiplicity)
#     return Decimal(amount_in_main_currency*Decimal(to_currency.rate/to_currency.multiplicity))._rescale(-4, "ROUND_UP")


def get_convert_amount(amount, exchange_rate):
    return Decimal(amount * exchange_rate)._rescale(-2, "ROUND_DOWN")


def get_commission_setting(user, payment_system, action_type):
    commission_setting = get_obj_or_none(CommissionSetting, action_type=action_type, payment_system=payment_system, payment_coupon_kind=None)
    try:
        escrow = user.profileaccountsettings.escrow
    except:
        pass
    else:
        commission_setting_e = get_obj_or_none(CommissionSetting, action_type=action_type, payment_system=payment_system, payment_coupon_kind="E")
        if commission_setting_e:
            commission_setting = commission_setting_e
    return commission_setting


def get_exchange_rate(currency_from, currency_to):
    if currency_from.iso_code == currency_to.iso_code:
        return Decimal("1.0000")
    else:
        exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency_from.iso_code, currency_to.iso_code))
        if exchange_rate:
            return exchange_rate.rate
        else:
            return None


def get_commission(amount, exchange_rate, user, payment_system, action_type):
    commission_setting = get_commission_setting(user, payment_system, action_type)
    if commission_setting:
        if commission_setting.commissionrange_set.all().count() != 0:
            # find needed sum commission by min_max amount range in Commission Range
            commission_range_exchange_rate = get_exchange_rate(exchange_rate.currency_from, commission_setting.currency_commission_range)
            if not commission_range_exchange_rate:
                return 0
            amount_range = get_convert_amount(amount, commission_range_exchange_rate)
            try:
                commission_range = CommissionRange.objects.get(commission_setting=commission_setting, min_amount__lt=amount_range, max_amount__gte=amount_range)
            except:
                commission_range = CommissionRange.objects.filter(commission_setting=commission_setting).latest("id")

            # return commission in payment system currency
            if commission_setting.type_commission == "P":
                to_calculate_exchange_rate = get_exchange_rate(exchange_rate.currency_from, exchange_rate.currency_to)
                if not to_calculate_exchange_rate:
                    return 0
                else:
                    return get_convert_amount(Decimal(float(commission_range.commission/100))._rescale(-4, "ROUND_DOWN"), to_calculate_exchange_rate)
            elif commission_setting.type_commission == "M":
                to_calculate_exchange_rate = get_exchange_rate(commission_setting.currency_commission, exchange_rate.currency_to)
                if not to_calculate_exchange_rate:
                    return 0
                else:
                    return get_convert_amount(Decimal(float(commission_range.commission))._rescale(-2, "ROUND_DOWN"), to_calculate_exchange_rate)
            else:
                return 0
        else:
            return 0
    else:
        return 0


def get_billing_information(came_amount, exchange_rate, opposite_exchange_rate, user, payment_system, type_payment):
    # amount = get_convert_amount(Decimal(came_amount), exchange_rate.rate)
    # commission = get_commission(Decimal(came_amount)._rescale(-2, "ROUND_DOWN"), exchange_rate, user, payment_system, type_payment)
    # commission_real = get_commission(Decimal(came_amount)._rescale(-, "ROUND_DOWN"), opposite_exchange_rate, user, payment_system, type_payment)
    #
    # payment_amount_real = Decimal(amount + commission)._rescale(-4, "ROUND_DOWN")
    # payment_amount = Decimal(payment_amount_real)._rescale(-2, "ROUND_UP")
    # amount_real = get_convert_amount(Decimal(payment_amount_real - commission)._rescale(-4, "ROUND_UP"), opposite_exchange_rate.rate)._rescale(-2, "ROUND_UP")
    # amount_with_commission = get_convert_amount(payment_amount_real, opposite_exchange_rate.rate)


    amount = get_convert_amount(Decimal(came_amount), exchange_rate.rate)
    amount_real = came_amount

    commission = get_commission(came_amount, exchange_rate, user, payment_system, type_payment)
    commission_real = get_commission(came_amount, opposite_exchange_rate, user, payment_system, type_payment)
    getcontext().prec = 4
    payment_amount_real = Decimal(amount + commission)

    payment_amount = Decimal(payment_amount_real)._rescale(-2, "ROUND_UP")

    amount_with_commission = Decimal(amount_real + commission_real)
    # amount_with_commission = get_convert_amount(payment_amount_real, opposite_exchange_rate.rate)

    return {
        "came_amount": came_amount,
        "amount": amount,
        "commission": commission,
        "payment_amount_real": payment_amount_real,
        "payment_amount": payment_amount,
        "amount_real": amount_real._rescale(-2, "ROUND_UP"),
        "commission_real": commission_real,
        "amount_with_commission": amount_with_commission
    }





def get_operation_mode():
    return get_obj_or_none(OperatingMode, active=1)


# def deposit_money(user, currency, amount, commission, amount_with_commission, payment_system, purse_from, purse_to, ps_amount, ps_currency, order):
#     order.status = "success"
#     order.paid = 1
#     order.date_pay = datetime.today()
#     order.save()
#     try:
#         ub = UserBalance.objects.get(user=user, currency=currency, payment_coupon=None)
#     except:
#         ub = UserBalance(user=user, profile=user.profile, state=amount, currency=currency)
#         ub.save()
#     else:
#         ub.state += amount
#         ub.save()
#
#     ph = PaymentHistory(user=user, profile=user.profile, content_object=order, action_type="adding_funds",
#                         shop=None, amount=amount, commission=commission, amount_with_commission=amount_with_commission, currency=currency,
#                         payment_system=payment_system,
#                         payment_kind="I")
#     ph.save()
#
#     aph = RealPurseHistory(payment_system=payment_system,
#                              purse_from=purse_from,
#                              purse_to=purse_to,
#                              amount=ps_amount,
#                              payment_kind="I",
#                              currency=ps_currency)
#     aph.save()
#
#     send_inform_mail.delay("account/adding_funds/notification/add_fund_subject.txt",
#                      "account/adding_funds/notification/add_fund.html",
#                      user.email,
#                      options={
#                          "amount": amount,
#                          "currency": currency,
#                      })
#     #send sms to user
#     return ub


def deposit_money(order):
    getcontext().prec = 5
    order.status = "success"
    order.date_pay = datetime.today()
    order.save()
    ub = None
    if not order.shop:
        try:
            ub = UserBalance.objects.get(user=order.user, currency=order.currency, payment_coupon=None)
        except:
            ub = UserBalance(user=order.user, profile=order.user.profile, state=order.amount, currency=order.currency)
            ub.save()
        else:
            ub.state += order.amount
            ub.save()

    if not order.shop:
        ph = PaymentHistory(user=order.user, profile=order.user.profile, content_object=order, action_type="adding_funds",
                            shop=None, amount=order.amount, commission=order.commission, amount_with_commission=order.amount_with_commission, currency=order.currency,
                            payment_system=order.payment_system,
                            payment_kind="I")
        ph.save()

    aph = RealPurseHistory(payment_system=order.payment_system,
                             purse_from=order.purse_from,
                             purse_to=order.purse_to,
                             amount=order.ps_amount,
                             payment_kind="I",
                             currency=order.ps_currency)
    aph.save()

    send_inform_mail.delay("account/adding_funds/notification/add_fund_subject.txt",
                     "account/adding_funds/notification/add_fund.html",
                     order.user.email,
                     options={
                         "amount": order.amount,
                         "currency": order.currency,
                     })
    #send sms to user
    return ub

def get_orders(request, object_columns):
    orders = []
    obj_list = [column.get("order_name", "") for column in object_columns]
    for elem, val in request.GET.items():
        try:
            val = int(val)
        except:
            pass
        else:
            if elem in obj_list:
                if int(val) == 1:
                    orders.append(elem)
                elif int(val) == -1:
                    orders.append("-%s" % elem)
    # Old code (not sorted by get parameters)
    # for column in object_columns:
    #     if request.GET.has_key(column.get("order_name", "")):
    #         if int(request.GET.get(column.get("order_name", 0))) == 1:
    #             orders.append(column.get("order_name", ""))
    #         elif int(request.GET.get(column.get("order_name", 0))) == -1:
    #             orders.append("-%s" % column.get("order_name", ""))

    return orders


def get_column_data(item, data_name):
    try:
        if isinstance(data_name, str):
            return item.__getattribute__(data_name)
        elif isinstance(data_name, list):
            return " ".join(["%s" % eval("item" + "".join([".__getattribute__('%s')" % name for name in dn.split(".")])) for dn in data_name])
        else:
            return item
    except:
        return ""


def prepare_xls_for_download(object_list, object_columns, sheet_name, file_name):
    # if not os.path.exists(settings.TMP_ROOT):
    #     os.makedirs(settings.TMP_ROOT)
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.colour_index = 2
    font0.bold = True
    style0 = xlwt.XFStyle()
    style0.font = font0
    wb = xlwt.Workbook()
    ws = wb.add_sheet(u"%s" % sheet_name)
    for i, column in enumerate(object_columns):
        ws.write(0, i, column.get("title", "").format(), style0)
    for y, item in enumerate(object_list):
        for x, column in enumerate(object_columns):
            cell = get_column_data(item, column.get("name"))
            if not cell:
                ws.write(y+1, x, "")
            else:
                if type(cell) is datetime:
                    date_style = xlwt.XFStyle()
                    date_style.num_format_str = "YY-MM-D H:m:s"
                    cell = cell.strftime("%Y-%m-%d %H:%M:%S")
                    ws.write(y+1, x, cell, date_style)
                else:
                    try:
                        cell = u"%s" % cell
                    except:
                        cell = u"%s" % cell.decode("UTF-8")
                    ws.write(y+1, x, cell)
                    # try:
                    #     #format
                    #     ws.write(y+1, x, u"%s" % cell.fromstring())
                    # except:
                    #     # ws.write(y+1, x, u"%s" % cell.format())
                    #     try:
                    #         ws.write(y+1, x, u"%s" % cell.get_data_name())
                    #     except:


    # file_name = settings.TMP_ROOT + "/%s_%s.xls" % (type_content, uuid.uuid4())
    response = HttpResponse(mimetype='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % file_name
    wb.save(response)
    return response


def check_operation_mode_for_profile_currency(redirect_to="/"):
    def decorator(func):
        def wrapped(request, *args, **kwargs):
            operation_mode = get_obj_or_none(OperatingMode, active=True)
            if operation_mode:
                try:
                    request.user.profile.currency
                except:
                    messages.info(request, _("The user has not selected currency. Please, select your currency."))
                    return HttpResponseRedirect(redirect_to)
                else:
                    if operation_mode.code == "PUC" and not request.user.profile.currency:
                        messages.info(request, _("The user has not selected currency. Please, select your currency."))
                        return HttpResponseRedirect(redirect_to)
            return func(request, *args, **kwargs)
        return wrapped
    return decorator


def check_system(redirect_to="/"):
    def decorator(func):
        def wrapped(request, *args, **kwargs):
            system_owner = get_obj_or_none(Profile, system_owner=True)
            operation_mode = get_obj_or_none(OperatingMode, active=True)
            if not system_owner:
                messages.info(request, _("Before you get started, necessary determine the system owner."))
                return HttpResponseRedirect(redirect_to)
            if not operation_mode:
                messages.info(request, _("Not selected operation mode of the system."))
                return HttpResponseRedirect(redirect_to)
            return func(request, *args, **kwargs)
        return wrapped
    return decorator

def get_success_url(url):
    if get_current_site_id() == 1:
        url = "/%s%s" % ("account", url)
    return url

# def identity_doc(redirect_to="/", obj=None):
#     def decorator(func):
#         def wrapped(request, *args, **kwargs):
#             if get_current_site_id() == 1:
#                 redirect_to = "/%s%s" % ("account", redirect_to)
#
#
#             # system_owner = get_obj_or_none(Profile, system_owner=True)
#             # operation_mode = get_obj_or_none(OperatingMode, active=True)
#             # if not system_owner:
#             #     messages.info(request, _("Before you get started, necessary determine the system owner."))
#             #     return HttpResponseRedirect(redirect_to)
#             # if not operation_mode:
#             #     messages.info(request, _("Not selected operation mode of the system."))
#             #     return HttpResponseRedirect(redirect_to)
#             return func(request, *args, **kwargs)
#
#
#         return wrapped
#     return decorator
#





@transaction.commit_on_success
def invoice_complete_in_favor_owner(invoice, system_owner, system_owner_balance):
    invoice.completed = True
    invoice.save()
    """ Withdrawal money from system owner """
    system_owner_balance.state -= invoice.amount
    system_owner_balance.save()
    system_owner_balance_ph = PaymentHistory.objects.create(user=system_owner.user, profile=system_owner,
                        content_object=invoice, action_type="foot_invoice",
                        shop=None, amount=invoice.amount,
                        commission=0,
                        amount_with_commission=invoice.amount,
                        currency=invoice.currency,
                        payment_system=get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM),
                        payment_kind="E")

    """ Adding funds for owner invoice """
    try:
        owner_invoice_balance = UserBalance.objects.get(user=invoice.owner, currency=invoice.currency, payment_coupon=None)
    except:
        ub = UserBalance(user=invoice.owner, profile=invoice.owner.profile, state=invoice.amount, currency=invoice.currency, payment_coupon=None)
        ub.save()
    else:
        owner_invoice_balance.state += invoice.amount
        owner_invoice_balance.save()

    owner_invoice_ph = PaymentHistory.objects.create(user=invoice.owner, profile=invoice.owner.profile,
        content_object=invoice, action_type="foot_invoice",
        shop=None, amount=invoice.amount,
        commission=0,
        amount_with_commission=invoice.amount,
        currency=invoice.currency,
        payment_system=get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM),
        payment_kind="I")

    """ Send mail to owner """
    send_inform_mail.delay("account/invoice/notification/invoice_complete_in_favor_owner_send_owner_subject.txt",
                           "account/invoice/invoice_complete_in_favor_owner_to_owner.html",
                           invoice.owner.email,
                           options={
                               "invoice": invoice,
                           })
    """ Send mail to payer """
    send_inform_mail.delay("account/invoice/notification/invoice_complete_in_favor_owner_send_payer_subject.txt",
                           "account/invoice/invoice_complete_in_favor_owner_to_payer.html",
                           invoice.payer.email,
                           options={
                               "invoice": invoice,
                           })
    return True


def invoice_complete_in_favor_payer(invoice, system_owner, system_owner_balance):
    invoice.paid = 0
    invoice.status = "canceled"
    invoice.completed = True
    invoice.save()
    """ Withdrawal money from system owner """
    system_owner_balance.state -= invoice.amount
    system_owner_balance.save()
    system_owner_balance_ph = PaymentHistory.objects.create(user=system_owner.user, profile=system_owner,
                        content_object=invoice, action_type="foot_invoice",
                        shop=None, amount=invoice.amount,
                        commission=0,
                        amount_with_commission=invoice.amount,
                        currency=invoice.currency,
                        payment_system=get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM),
                        payment_kind="E")


    """ adding funds for payer invoice """
    try:
        payer_invoice_balance = UserBalance.objects.get(user=invoice.payer, currency=invoice.currency, payment_coupon=None)
    except:
        ub = UserBalance.objects.create(user=invoice.payer, profile=invoice.owner.profile, state=invoice.amount, currency=invoice.currency, payment_coupon=None)
    else:
        payer_invoice_balance.state += invoice.amount
        payer_invoice_balance.save()

    payer_invoice_ph = PaymentHistory.objects.create(user=invoice.payer, profile=invoice.payer.profile,
        content_object=invoice, action_type="foot_invoice",
        shop=None, amount=invoice.amount,
        commission=0,
        amount_with_commission=invoice.amount,
        currency=invoice.currency,
        payment_system=get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM),
        payment_kind="I")

    """ Send mail to payer """
    send_inform_mail.delay("account/invoice/notification/invoice_complete_in_favor_payer_send_payer_subject.txt",
                           "account/invoice/invoice_complete_in_favor_payer_send_payer.html",
                           invoice.payer.email,
                           options={
                               "invoice": invoice,
                           })

    """ Send mail to owner """
    send_inform_mail.delay("account/invoice/notification/invoice_complete_in_favor_payer_send_owner_subject.txt",
                           "account/invoice/invoice_complete_in_favor_payer_send_owner.html",
                           invoice.payer.email,
                           options={
                               "invoice": invoice,
                           })