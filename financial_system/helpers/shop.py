# --coding: utf-8
import hashlib


def get_secret_key(obj_id, obj_password):
    secret_key = hashlib.md5()
    secret_key.update("%s%s" % (obj_id, obj_password))
    return secret_key.hexdigest()
