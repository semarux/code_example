# --coding: utf-8
from selectable.forms import *

from financial_system.models import *
from financial_system.helpers.system_functs import get_obj_or_none
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.conf import settings





class PsCurrencyField(forms.ChoiceField):
    ps_sys_code = None
    ps_currency = None
    purse = None

    def __init__(self, ps_sys_code=None, widget=None, *args, **kwargs):
        self.ps_sys_code = ps_sys_code
        if not widget:
            if ps_sys_code == "YM":
               widget = forms.HiddenInput(attrs={"hidden": "hidden", "value": "YAR"})
            else:
                widget = forms.Select(attrs={"class": "custom-sel all_width"})
        if ps_sys_code == "WM":
            super(PsCurrencyField, self).__init__(required=False, choices=WM_TYPE_PURSE_CHOICE, widget=widget, label=_("WM purse type"), help_text=_("Choose your type of purse"))
        elif ps_sys_code == "YM":
            super(PsCurrencyField, self).__init__(required=False, widget=widget, label=_("WM purse Type"), help_text=_("Choose your type of purse"))
        elif ps_sys_code == "QM":
            super(PsCurrencyField, self).__init__(required=False, choices=QM_TYPE_PURSE_CHOICE, widget=widget, label=_("Qiwi purse type"), help_text=_("Choose your type of purse"))
        elif ps_sys_code == "LPM":
            super(PsCurrencyField, self).__init__(required=False, choices=LP_TYPE_CURRENCY_CHOICE, widget=widget, label=_("Liqpay currency type"), help_text=_("Choose your currency type"))
        elif ps_sys_code == "BT":
            super(PsCurrencyField, self).__init__(required=False, choices=[(c.iso_code, c.name) for c in Currency.objects.all().exclude(ps_special=1)], widget=widget, label=_("Bank transfer currency"), help_text=_("Choose your currency type"))
        else:
            super(PsCurrencyField, self).__init__(required=False, choices=[("", _("Not selected payment system")), ], widget=widget, label=_("Currency type"))

    def clean(self, ps_currency_type):
        self.ps_currency = ps_currency = get_obj_or_none(Currency, iso_code=ps_currency_type)
        if self.ps_sys_code == "LPM" or self.ps_sys_code == "QM":
            self.purse = get_obj_or_none(Purse, type=self.ps_sys_code, is_work=True, active=True)
        else:
            self.purse = get_obj_or_none(Purse, type=ps_currency_type, is_work=True, active=True)

        if not ps_currency:
            raise forms.ValidationError(_("This currency of payment system working in system"))

        if not self.purse and self.ps_sys_code != "BT":
            raise forms.ValidationError(_("This type of purse or currency not working in system"))

        if not self.ps_currency:
            raise forms.ValidationError(_("Do not set the currency for this payment system"))
        return ps_currency


class BankCurrencyField(forms.ChoiceField):
    bank = None
    ps_currency = None
    def __init__(self, bank=None, widget=None, *args, **kwargs):
        self.bank = bank
        if not widget:
            widget = forms.Select(attrs={"class": "custom-sel all_width"})
        if self.bank:
            super(BankCurrencyField, self).__init__(required=False, label=_("Bank transfer currency"), choices=[("", _("Choose currency"))] + [(c.iso_code, c.name) for c in self.bank.currencies.all()],
                                                    widget=widget, help_text=_("Choose currency type"))

    def clean(self, ps_currency_type):
        if not self.bank:
            raise forms.ValidationError(_("You didn't choose the bank"))

        self.ps_currency = ps_currency = get_obj_or_none(Currency, iso_code=ps_currency_type)
        if not ps_currency:
            raise forms.ValidationError(_("This currency of payment system working in system"))
        return ps_currency


class OperationModeCurrencyField(forms.ModelChoiceField):
    user_currency = None
    currency_val = None

    def __init__(self, user=None, label=None, use_user_currency=False, *args, **kwargs):
        operation_mode = get_obj_or_none(OperatingMode, active=True)
        currency_list = Currency.objects.all().exclude(ps_special=1)
        if user and use_user_currency:
            currency_list = Currency.objects.filter(id__in=[ub.currency.id for ub in user.userbalance_set.all() if ub.state > 0])

        if not label:
            label = _("Currency")
        if operation_mode:
            if operation_mode.code == "PUC":
                if user:
                    self.user_currency = user.profile.currency
                    super(OperationModeCurrencyField, self).__init__(required=True, label=label, empty_label=_("Choose currency"),
                                        initial=user.profile.currency.id,
                                        queryset=currency_list,
                                        widget=forms.HiddenInput(attrs={}),
                                        help_text=user.profile.currency.name
                    )
                else:
                    super(OperationModeCurrencyField, self).__init__(required=True, label=label, empty_label=_("Choose currency"),
                                      queryset=currency_list,
                                      widget=forms.Select(attrs={"class": "custom-sel all_width", }),
                    )

            elif operation_mode.code == "PSC":
                currency = get_obj_or_none(Currency, iso_code=settings.SYSTEM_CURRENCY)
                if currency:
                    currency_initial = currency.id
                    super(OperationModeCurrencyField, self).__init__(required=True, label=label, empty_label=_("Choose currency"),
                                      queryset=currency_list,
                                      initial=currency_initial,
                                      widget=forms.HiddenInput(attrs={"class": "", }),
                                      help_text=currency.name,
                    )
            else:
                super(OperationModeCurrencyField, self).__init__(required=True, label=label, empty_label=_("Choose currency"),
                                      queryset=currency_list,
                                      widget=forms.Select(attrs={"class": "custom-sel all_width", }),
                )
        else:
            super(OperationModeCurrencyField, self).__init__(required=True, label=label, empty_label=_("Choose currency"),
                                      queryset=currency_list,
                                      widget=forms.Select(attrs={"class": "custom-sel all_width", }),
            )

    def validate(self, currency):
        self.currency_val = currency
        operation_mode = get_obj_or_none(OperatingMode, active=True)
        if currency == "" or not currency:
            raise forms.ValidationError(_("This field is required"))
        if operation_mode and currency:
            if operation_mode.code == "PUC":
                if currency.id != self.user_currency.id:
                    raise forms.ValidationError(_("Selected currency does not correspond to the system"))
            if operation_mode.code == "PSC":
                operation_mode_currency = get_obj_or_none(Currency, iso_code=settings.SYSTEM_CURRENCY)
                if currency.id != operation_mode_currency.id:
                    raise forms.ValidationError(_("Selected currency does not correspond to the system"))
        return currency
