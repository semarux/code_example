# -*- coding: utf-8 -*-
from threading import _BoundedSemaphore
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from financial_system.models import *
from torgplace.settings import LANGUAGE_CODE
from financial_system.forms.admin import CommissionRangeInlineFormSet, CommissionSettingAdminForm, \
    CommissionSettingAddAdminForm, OperatingModeAdminForm, SystemOwnerBalanceAdminForm, WithdrawalAdminForm, InvoiceDiscussionSendAdminForm
from helpers.system_functs import get_obj_or_none
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from operator import itemgetter
from django.db.models import Sum, Count
from django.db.models.query import QuerySet
from django.contrib.admin import SimpleListFilter
from daterange_filter.filter import DateRangeFilter
from django.conf.urls import patterns
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponseRedirect
from torgplace.settings import MAIN_PAYMENT_SYSTEM
from main.tasks import send_inform_mail
from django.db import transaction


class __TranslationAdmin(TranslationAdmin):
    class Media:
        js = (
            "/static/modeltranslation/js/force_jquery.js",
            "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js",
            "/static/modeltranslation/js/tabbed_translation_fields.js",
            "/static/main/js/core.js",
            "/static/main/js/transliterate.js",
            "/static/main/js/locale/" + LANGUAGE_CODE + "/transliterate.js",
        )
        css = {
            "screen": ("/static/modeltranslation/css/tabbed_translation_fields.css",),
        }


class InformationBlockAdmin(__TranslationAdmin):
    list_display = ("name", "title", "active", "type")


class PaymentSystemAdmin(__TranslationAdmin):
    list_display = ("get_image", "name", "description", "active")


class CommissionRangeAddInline(admin.TabularInline):
    model = CommissionRange
    extra = 1
    formset = CommissionRangeInlineFormSet


class CommissionRangeUpdateInline(admin.TabularInline):
    model = CommissionRange
    extra = 0
    formset = CommissionRangeInlineFormSet


class AddingFundAdmin(admin.ModelAdmin):
    list_display = ("user", "get_amount", "payment_system", "transaction", "date_create", "status", "paid")
    search_fields = ("user__username", "user__email", "transaction")
    list_filter = (("date_create", DateRangeFilter), "status", "paid", "payment_system")
    model = AddingFund

    def get_readonly_fields(self, request, obj=None):
        fields = [f.name for f in self.model._meta.fields]
        if obj.paid == False and obj.payment_system.sys_code == "BT":
            fields.remove("paid")
            fields.remove("purse_from")
        return fields

    def save_model(self, request, obj, form, change):
        if obj.payment_system.sys_code == "BT":
            obj.purse_to = obj.bank.account_number
        obj.save()

    def has_add_permission(self, request):
        return False


amount_addingfund_sum_list = {}


class AFDateRangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        global amount_addingfund_sum_list

        if self.form.is_valid():
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))
            amount_addingfund_sum_list = queryset.filter(**filter_params).values("payment_system", "currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
            return queryset.filter(**filter_params)
        else:
            return queryset


class AFPaymentSystemListFilter(SimpleListFilter):
    title = _("payment system")
    parameter_name = "payment_system"

    def lookups(self, request, model_admin):
        return [(c.id, c.name) for c in PaymentSystem.objects.filter(active=1)]

    def queryset(self, request, queryset):
        global amount_addingfund_sum_list
        if self.value():
            amount_addingfund_sum_list = queryset.filter(payment_system_id=self.value()).values("payment_system", "currency").values_list("currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
            return queryset.filter(payment_system_id=self.value())


class AFPaidListFilter(SimpleListFilter):
    title = _("Paid")
    parameter_name = "paid"

    def lookups(self, request, model_admin):
        return (
            ("0", _("Yes")),
            ("1", _("No")),
        )

    def queryset(self, request, queryset):
        global amount_addingfund_sum_list
        if self.value():
            amount_addingfund_sum_list = queryset.filter(paid=self.value()).values("payment_system", "currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
            return queryset.filter(paid=self.value())


class StatisticAddingFundAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(StatisticAddingFundAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        return []

    def queryset(self, request):
        global amount_addingfund_sum_list
        qs = super(StatisticAddingFundAdmin, self).queryset(request)
        amount_addingfund_sum_list = qs.values("payment_system", "currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
        query = qs.query
        query.group_by = ["payment_system_id", "currency_id"]
        results = QuerySet(query=query, model=StatisticAddingFund)
        return results

    def sum(self, obj):
        try:
            return filter(lambda x: x.get('currency') == obj.currency_id and x.get('payment_system') == obj.payment_system.id, amount_addingfund_sum_list)[0].get("amount_sum")
        except:
            return 0

    def get_payment_system(self, obj):
        return u"%s" % obj.payment_system.name

    def get_currency(self, obj):
        return "%s (%s)" % (obj.currency.name, obj.currency.iso_code)

    list_display = ("get_payment_system", "get_currency", "sum")
    list_filter = (('date_create', AFDateRangeFilter), AFPaymentSystemListFilter, AFPaidListFilter)

    class Media:
        js = ("/admin/jsi18n/",)


class CommissionSettingAdmin(admin.ModelAdmin):
    list_display = ("action_type", "payment_system", "payment_coupon_kind")
    form = CommissionSettingAdminForm

    def change_view(self, request, obj_id):
        self.form = CommissionSettingAdminForm
        self.inlines = [CommissionRangeUpdateInline, ]
        return super(CommissionSettingAdmin, self).change_view(request, obj_id)

    def add_view(self, request):
        self.form = CommissionSettingAddAdminForm
        self.inlines = [CommissionRangeAddInline, ]
        return super(CommissionSettingAdmin, self).add_view(request)


class PurseAdmin(admin.ModelAdmin):
    list_display = ("username", "type", "is_test", "is_work", "active")


class OperatingModeAdmin(admin.ModelAdmin):
    form = OperatingModeAdminForm
    list_display = ("name", "code", "description", "active")


class UserEventMessageAdmin(__TranslationAdmin):
    list_display = ("user", "action_type", "message_type", "subject", "date_create", "is_read")


class SystemOwnerBalanceAdmin(admin.ModelAdmin):
    list_display = ("get_state", "payment_coupon")
    system_owner = get_obj_or_none(Profile, system_owner=1)
    form = SystemOwnerBalanceAdminForm

    list_filter = ("payment_coupon", )

    def queryset(self, request):
        qs = super(SystemOwnerBalanceAdmin, self).queryset(request)
        if self.system_owner:
            return qs.filter(user=self.system_owner.user)

        return []

#
# class CurrencyListFilter(admin.RelatedFieldListFilter):
#     title = _("Currencies")
#     parameter_name = "currency"
#
#     def __init__(self, field, request, params, model, model_admin, field_path):
#         super(CurrencyListFilter, self).__init__(
#             field, request, params, model, model_admin, field_path)
#         self.lookup_choices = Currency.objects.values_list('currency', 'currency__name',)


class CurrencyListFilter(SimpleListFilter):
    title = _("Currency")
    parameter_name = "currency"

    def lookups(self, request, model_admin):
        return [(c.id, "%s (%s)" % (c.name, c.iso_code)) for c in Currency.objects.all().exclude(ps_special=1)]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(currency_id=self.value())


class UserBalanceAdmin(admin.ModelAdmin):
    list_display = ("user", "user_email", "get_state", "payment_coupon")
    # list_filter = ("currency",)
    # list_filter = (("currency", "CurrencyListFilter"),)
    list_filter = (CurrencyListFilter, "payment_coupon")
    search_fields = ("user__username", "user__email")


class ExchangeRateAdmin(admin.ModelAdmin):
    list_display = ("iso_code", "currency_from", "currency_to", "rate", "date_update")
    search_fields = ("iso_code",)


class RealPurseHistoryAdmin(admin.ModelAdmin):
    list_display = ("amount", "currency", "purse_from", "purse_to", "payment_kind", "date_create")
    list_filter = (("date_create", DateRangeFilter), "payment_system", "payment_kind")

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False


class PaymentHistoryAdmin(admin.ModelAdmin):
    list_display = ("user", "get_email", "get_amount", "get_commission", "get_amount_with_commission", "payment_kind", "payment_system", "date_create")

    search_fields = ("user__username", "user__email")
    list_filter = (("date_create", DateRangeFilter), "payment_kind", "payment_system", "action_type")

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False


amount_conversionfund_sum_list = {}
class CFDateRangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        global amount_conversionfund_sum_list

        if self.form.is_valid():
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))
            amount_conversionfund_sum_list = queryset.filter(**filter_params).values("exchange_rate").annotate(credit_amount_sum=Sum('credit_amount'), debit_amount_sum=Sum('debit_amount')).order_by('-credit_amount_sum')
            return queryset.filter(**filter_params)
        else:
            return queryset


class CFCurrencyListFilter(SimpleListFilter):
    title = _("Currency from")
    parameter_name = "credit_currency"

    def lookups(self, request, model_admin):
        return [(c.id, c.name) for c in Currency.objects.all().exclude(ps_special=1)]

    def queryset(self, request, queryset):
        global amount_conversionfund_sum_list
        if self.value():
            amount_conversionfund_sum_list = queryset.filter(credit_currency_id=self.value()).values("exchange_rate").annotate(credit_amount_sum=Sum('credit_amount'), debit_amount_sum=Sum('debit_amount')).order_by('-credit_amount_sum')
            return queryset.filter(credit_currency_id=self.value())


class StatisticConversionFundAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(StatisticConversionFundAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    list_per_page = 100000

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        return []

    def queryset(self, request):
        global amount_conversionfund_sum_list
        qs = super(StatisticConversionFundAdmin, self).queryset(request)
        amount_conversionfund_sum_list = qs.values("exchange_rate").annotate(credit_amount_sum=Sum('credit_amount'), debit_amount_sum=Sum('debit_amount')).order_by('-credit_amount_sum')
        query = qs.query
        query.group_by = ["exchange_rate_id", ]
        results = QuerySet(query=query, model=StatisticConversionFund)
        return results

    def credit_amount_sum(self, obj):
        try:
            return filter(lambda x: x.get('exchange_rate') == obj.exchange_rate.id, amount_conversionfund_sum_list)[0].get("credit_amount_sum")
        except:
            return 0

    def debit_amount_sum(self, obj):
        try:
            return filter(lambda x: x.get('exchange_rate') == obj.exchange_rate.id, amount_conversionfund_sum_list)[0].get("debit_amount_sum")
        except:
            return 0

    def get_exchange_rate(self, obj):
        return u"%s" % obj.exchange_rate.iso_code

    list_display = ("get_exchange_rate", "credit_amount_sum", "debit_amount_sum")
    list_filter = (('date_create', CFDateRangeFilter), CFCurrencyListFilter)

    class Media:
        js = ("/admin/jsi18n/",)


class ConversionFundCurrencyListFilter(SimpleListFilter):
    title = _("Currency from")
    parameter_name = "credit_currency"

    def lookups(self, request, model_admin):
        return [(c.id, c.name) for c in Currency.objects.all().exclude(ps_special=1)]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(credit_currency_id=self.value())


class ConversionFundAdmin(admin.ModelAdmin):
    list_display = ("user", "get_exchange_rate", "value_exchange_rate", "get_credit_amount", "get_credit_commission", "get_credit_amount_with_commission",  "get_debit_amount", "date_create")
    search_fields = ("user__username", "user__email")
    list_filter = (("date_create", DateRangeFilter), ConversionFundCurrencyListFilter)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

amount_withdrawal_sum_list = {}


class SWPaymentSystemListFilter(SimpleListFilter):
    title = _("payment system")
    parameter_name = "payment_system"

    def lookups(self, request, model_admin):
        return [(c.id, c.name) for c in PaymentSystem.objects.filter(active=1)]

    def queryset(self, request, queryset):
        global amount_withdrawal_sum_list
        if self.value():
            amount_withdrawal_sum_list = queryset.filter(payment_system_id=self.value()).values("payment_system", "currency_to_withdrawal").annotate(amount_sum=Sum('amount_to_withdrawal')).order_by('-amount_sum')
            return queryset.filter(payment_system_id=self.value())


class SWPaidListFilter(SimpleListFilter):
    title = _("Paid")
    parameter_name = "paid"

    def lookups(self, request, model_admin):
        return (
            ("0", _("Yes")),
            ("1", _("No")),
        )

    def queryset(self, request, queryset):
        global amount_withdrawal_sum_list
        if self.value():
            amount_withdrawal_sum_list = queryset.filter(paid=self.value()).values("payment_system", "currency_to_withdrawal").annotate(amount_sum=Sum('amount_to_withdrawal')).order_by('-amount_sum')
            return queryset.filter(paid=self.value())


class SWDateRangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        global amount_withdrawal_sum_list

        if self.form.is_valid():
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))
            amount_withdrawal_sum_list = queryset.filter(**filter_params).values("payment_system", "currency_to_withdrawal").annotate(amount_sum=Sum('amount_to_withdrawal')).order_by('-amount_sum')
            return queryset.filter(**filter_params)
        else:
            return queryset


class StatisticWithdrawalAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(StatisticWithdrawalAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        return []

    def queryset(self, request):
        global amount_withdrawal_sum_list
        qs = super(StatisticWithdrawalAdmin, self).queryset(request)
        amount_withdrawal_sum_list = qs.values("payment_system", "currency_to_withdrawal",).annotate(amount_sum=Sum("amount_to_withdrawal")).order_by('-amount_sum')
        query = qs.query
        query.group_by = ["payment_system_id", "currency_to_withdrawal_id"]
        results = QuerySet(query=query, model=StatisticWithdrawal)
        return results

    def sum(self, obj):
        try:
            return filter(lambda x: x.get('currency_to_withdrawal') == obj.currency_to_withdrawal_id and x.get('payment_system') == obj.payment_system.id, amount_withdrawal_sum_list)[0].get("amount_sum")
        except:
            return 0
        # return dict(amount_withdrawal_sum_list).get(obj.currency_to_withdrawal_id, "")

    list_display = ("payment_system", "currency_to_withdrawal", "sum")
    list_filter = (('date_create', SWDateRangeFilter), SWPaidListFilter, SWPaymentSystemListFilter)

    class Media:
        js = ("/admin/jsi18n/",)


class WithdrawalAdmin(admin.ModelAdmin):
    # TODO: change bank_requisite
    form = WithdrawalAdminForm

    list_display = (
        "user", "get_amount_with_commission_to_withdrawal", "get_amount_to_withdrawal", "get_commission_to_withdrawal", "get_amount_to_obtain", "payment_system",
        "purse", "date_pay", "date_create", "withdrawn", "paid", "status"
    )

    readonly_fields = [
        "payment_system", "purse", "withdrawn", "user", "profile", "manager", "amount_to_withdrawal", "commission_to_withdrawal", "amount_with_commission_to_withdrawal",
        "amount_to_obtain", "currency_to_withdrawal", "currency_to_obtain", "purse", "comment", "bank_requisite", "date_pay"
    ]

    search_fields = ("user__username", "user__email")
    list_filter = (("date_create", DateRangeFilter), "status", "paid", "withdrawn", "payment_system")

    # def get_readonly_fields(self, request, obj=None):
        # if obj.paid:
        #     return super(WithdrawalAdmin, self).get_readonly_fields(request, obj) + ["paid", "status", "purse_from"]
        # else:
        #     return super(WithdrawalAdmin, self).get_readonly_fields(request, obj)

    def change_view(self, request, obj_id):
        obj = self.get_object(request, object_id=obj_id)
        fields = ["paid", "status", "purse_from"]

        if obj.paid == True:
            for field in fields:
                if field not in self.readonly_fields:
                    self.readonly_fields.append(field)
        else:
            for field in fields:
                if field in self.readonly_fields:
                    self.readonly_fields.remove(field)

        return super(WithdrawalAdmin, self).change_view(request, obj_id)

    def save_model(self, request, obj, form, change):
        if obj.paid == True:
            obj.status = "paid"
        obj.manager = request.user
        obj.date_pay = datetime.today()
        obj.save()

        # if form.cleaned_data.get("paid"):
        #     if obj.status == True:
        #         try:
        #             ub = UserBalance.objects.get(user=obj.user, currency=obj.currency_to_withdrawal, payment_coupon=None)
        #         except:
        #             pass
        #         else:
        #             ub.state -= obj.amount_with_commission_to_withdrawal
        #             ub.save()
        #
        #     ph = PaymentHistory(user=obj.user, profile=obj.user.profile, content_object=obj, action_type="withdrawal",
        #                         shop=None, amount=obj.amount_to_withdrawal,
        #                         commission=obj.commission_to_withdrawal, amount_with_commission=obj.amount_with_commission_to_withdrawal,
        #                         currency=obj.currency_to_withdrawal,
        #                         payment_system=obj.payment_system,
        #                         payment_kind="E")
        #     ph.save()

    def has_add_permission(self, request):
        return False

    # TODO: uncoment this code
    # def has_delete_permission(self, request, obj=None):
    #     return False


class RegularPaymentAdmin(admin.ModelAdmin):
    list_display = ("sender", "recipient", "get_amount", "type_periodicity",  "date_start_payment", "date_next_payment", "date_last_payment", "date_create", "active")


class TransferFundAdmin(admin.ModelAdmin):
    list_display = ("sender", "recipient", "get_amount", "get_amount",  "get_amount_with_commission", "date_create")
    search_fields = ("sender__username", "sender__email", "recipient__username", "recipient__email")
    list_filter = (("date_create", DateRangeFilter), CurrencyListFilter)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

amount_transferfund_sum_list = {}
class TFDateRangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        global amount_transferfund_sum_list

        if self.form.is_valid():
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))
            amount_transferfund_sum_list = queryset.filter(**filter_params).values("currency").values_list("currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
            return queryset.filter(**filter_params)
        else:
            return queryset


class StatisticTransferFundAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        return []

    def queryset(self, request):
        global amount_transferfund_sum_list
        qs = super(StatisticTransferFundAdmin, self).queryset(request)
        amount_transferfund_sum_list = qs.values("currency").values_list("currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
        query = qs.query
        query.group_by = ["currency_id",]
        return QuerySet(query=query, model=StatisticTransferFund)

    def sum(self, obj):
        return dict(amount_transferfund_sum_list).get(obj.currency_id, "")

    def get_currency(self, obj):
        return "%s (%s)" % (obj.currency.name, obj.currency.iso_code)

    list_display = ("get_currency", "sum")
    list_filter = (('date_create', TFDateRangeFilter),)

    def __init__(self, *args, **kwargs):
        super(StatisticTransferFundAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    class Media:
        js = ("/admin/jsi18n/",)


class OrderRegularPaymentAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(OrderRegularPaymentAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    # def _amount(self):
    #     return u"%s %s" % (self.amount, self.currency)
    #
    # def _commission(self):
    #     return u"%s %s" % (self.commission, self.currency)
    #
    # def _amount_with_commission(self):
    #     return u"%s %s" % (self.amount_with_commission, self.currency)

    list_display = ("get_sender", "get_recipient", "get_amount", "get_commission", "get_amount_with_commission", "paid", "date_create" )
    search_fields = ("regular_payment__user__username", "regular_payment__user__email")
    list_filter = ("paid", ("date_create", DateRangeFilter))

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

amount_orderregular_payment_sum_list = {}
class SORPDateRangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        global amount_orderregular_payment_sum_list
        if self.form.is_valid():
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))
            amount_orderregular_payment_sum_list = queryset.filter(**filter_params).values("currency").annotate(amount_sum=Sum('amount')).order_by('-amount_sum')
            return queryset.filter(**filter_params)
        else:
            return queryset


class SORDPPaidListFilter(SimpleListFilter):
    title = _("Paid")
    parameter_name = "paid"

    def lookups(self, request, model_admin):
        return (
            ("0", _("Yes")),
            ("1", _("No")),
        )

    def queryset(self, request, queryset):
        global amount_orderregular_payment_sum_list
        if self.value():
            amount_orderregular_payment_sum_list = queryset.filter(paid=self.value()).values("currency").annotate(amount_sum=Sum("amount")).order_by('-amount_sum')
            return queryset.filter(paid=self.value())

class StatisticOrderRegularPaymentAdmin(OrderRegularPaymentAdmin):
    def __init__(self, *args, **kwargs):
        super(OrderRegularPaymentAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
        self.readonly_fields = self.model._meta.get_all_field_names()

    def has_add_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        return []

    def queryset(self, request):
        global amount_orderregular_payment_sum_list
        qs = super(StatisticOrderRegularPaymentAdmin, self).queryset(request)
        amount_orderregular_payment_sum_list = qs.values("currency").annotate(amount_sum=Sum("amount")).order_by('-amount_sum')
        query = qs.query
        query.group_by = ["currency_id", ]
        results = QuerySet(query=query, model=StatisticOrderRegularPayment)
        return results

    def sum(self, obj):
        try:
            return filter(lambda x: x.get('currency') == obj.currency_id, amount_orderregular_payment_sum_list)[0].get("amount_sum")
        except:
            return 0

    def get_currency(self, obj):
        return "%s (%s)" % (obj.currency.name, obj.currency.iso_code)

    list_display = ("get_currency", "sum")
    list_filter = (('date_create', SORPDateRangeFilter), SORDPPaidListFilter)

    class Media:
        js = ("/admin/jsi18n/",)


# class InvoiceDiscussionInline(admin.TabularInline):
#     model = InvoiceDiscussion
#     extra = 0
#     # formset = CommissionRangeInlineFormSet


from django.shortcuts import get_object_or_404
from financial_system.helpers.amount import invoice_complete_in_favor_owner, invoice_complete_in_favor_payer

class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
                "owner", "discussion", "payer", "get_amount", "get_payment_amount", "get_payment_commission", "get_payment_amount_with_commission",
                "get_shop", "get_status_for_admin", "paid", "completed", "date_create"
    )

    search_fields = ("owner__username", "owner__email", "payer__username", "payer__email")
    list_filter = (("date_create", DateRangeFilter), "status", "paid", "completed")

    def get_urls(self):
        urls = super(InvoiceAdmin, self).get_urls()
        my_urls = patterns('',
                           (r'^discussion/(?P<pk>\d+)/', self.admin_site.admin_view(self.InvoiceDiscussionView)),
                           (r'^invoice_complete_in_favor_owner/(?P<pk>\d+)/', self.admin_site.admin_view(self.InvoiceCompleteInFavorOwner)),
                           (r'^invoice_complete_in_favor_payer/(?P<pk>\d+)/', self.admin_site.admin_view(self.InvoiceCompleteInFavorPayer)),
        )
        return my_urls + urls


    @transaction.commit_on_success
    def InvoiceCompleteInFavorOwner(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        system_owner = get_obj_or_none(Profile, system_owner=1)
        success_url = "/admin/financial_system/invoice/"

        system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
        if not invoice.completed:
            if system_owner_balance and system_owner_balance.state >= invoice.amount:
                invoice_complete_in_favor_owner(invoice, system_owner, system_owner_balance)
                messages.info(request, _("Invoice complete in favor owner."))
            else:
                messages.error(request, _("Sorry but system owner haven't enough money in this currency. Please try the operation later."))
        else:
            messages.error(request, _("The invoice completed."))

        return HttpResponseRedirect(success_url)

    @transaction.commit_on_success
    def InvoiceCompleteInFavorPayer(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        success_url = "/admin/financial_system/invoice/"
        system_owner = get_obj_or_none(Profile, system_owner=1)
        system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
        if not invoice.completed:
            if system_owner_balance and system_owner_balance.state >= invoice.amount:
                invoice_complete_in_favor_payer(invoice, system_owner, system_owner_balance)
                messages.info(request, _("Invoice complete in favor payer."))
            else:
                messages.error(request, _("Sorry but system owner haven't enough money in this currency. Please try the operation later."))
        else:
            messages.error(request, _("The invoice completed."))
        return HttpResponseRedirect(success_url)

    def InvoiceDiscussionView(self, request, pk):
        invoice = get_object_or_404(Invoice, pk=pk)
        moderator = invoice.moderator

        if request.method == "POST":
            form = InvoiceDiscussionSendAdminForm(invoice=invoice, user=request.user, data=request.POST)
            if form.is_valid():
                invoice_discussion = InvoiceDiscussion.objects.create(
                    invoice=invoice,
                    user=request.user,
                    comment=form.cleaned_data["comment"],
                    is_moderator=1,
                )
        else:
            form = InvoiceDiscussionSendAdminForm(invoice=invoice, user=request.user)
        return render_to_response("admin/financial_system/invoice/discussion/view.html", locals(), RequestContext(request))

    def discussion(self, obj):
        if obj.completed:
            return _("Invoice completed")
        else:
            return u"<a href='discussion/%s/'>%s</a>" % (obj.id, "Discussion")

    discussion.allow_tags = True
    discussion.short_description = _("Discussion")


class BankAdmin(admin.ModelAdmin):
    list_display = ("name", "address", "account_number", "active")

    search_fields = ("name", "account_number",)
    list_filter = ("active", )


class ShopAdmin(admin.ModelAdmin):
    list_display = ("name", "date_create")

    search_fields = ("name",)


class ShopPaymentAdmin(admin.ModelAdmin):
    list_display = ("sender", "recipient", "get_amount", "get_commission", "get_amount_with_commission", "date_create")
    search_fields = ("sender__username", "sender__email", "recipient__username", "recipient__email")
    list_filter = (("date_create", DateRangeFilter), CurrencyListFilter)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False




admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Bank, BankAdmin)
# admin.site.register(InvoiceModerator, InvoiceModeratorAdmin)
# admin.site.register(RegularPaymentHistory, RegularPaymentHistoryAdmin)
admin.site.register(OrderRegularPayment, OrderRegularPaymentAdmin)
admin.site.register(TransferFund, TransferFundAdmin)
admin.site.register(RegularPayment, RegularPaymentAdmin)
admin.site.register(UserEventMessage, UserEventMessageAdmin)
admin.site.register(AddingFund, AddingFundAdmin)
admin.site.register(InformationBlock, InformationBlockAdmin)
admin.site.register(PaymentSystem, PaymentSystemAdmin)
admin.site.register(CommissionSetting, CommissionSettingAdmin)
admin.site.register(Purse, PurseAdmin)
admin.site.register(OperatingMode, OperatingModeAdmin)
admin.site.register(SystemOwnerBalance, SystemOwnerBalanceAdmin)
admin.site.register(RealPurseHistory, RealPurseHistoryAdmin)
# admin.site.register(ExchangeRate, ExchangeRateAdmin)
admin.site.register(UserBalance, UserBalanceAdmin)
admin.site.register(PaymentHistory, PaymentHistoryAdmin)
admin.site.register(Withdrawal, WithdrawalAdmin)
admin.site.register(ConversionFund, ConversionFundAdmin)
admin.site.register(StatisticWithdrawal, StatisticWithdrawalAdmin)
admin.site.register(StatisticTransferFund, StatisticTransferFundAdmin)
admin.site.register(StatisticAddingFund, StatisticAddingFundAdmin)
admin.site.register(StatisticConversionFund, StatisticConversionFundAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(StatisticOrderRegularPayment, StatisticOrderRegularPaymentAdmin)
admin.site.register(ShopPayment, ShopPaymentAdmin)

