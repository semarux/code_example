# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.template import loader
from django.core.mail import EmailMessage
from main.models import Currency

from financial_system.celery import app
from torgplace import settings

from financial_system.models import UserBalance, PaymentHistory, PaymentSystem, ExchangeRate, UserEventMessage
from torgplace.settings import SYSTEM_CURRENCY, MAIN_PAYMENT_SYSTEM
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.helpers.amount import get_billing_information

from django.core.mail import send_mail
from torgplace.settings import EMAIL_FROM
from django.contrib.sites.models import Site


@app.task(bind=True)
def debug_task(self):
    print "---------------"
    print('Request: {0!r}'.format(self.request))


@app.task
def finance_send_mail():
    send_mail('Subject here', 'Here is the message.', EMAIL_FROM, ['alexandr.okunovich@gmail.com',])
    return True



@app.task
def send_event_message(user, action_type, message_type, subject, content):
    UserEventMessage.objects.create(
                user=user,
                action_type=action_type,
                message_type=message_type,
                subject=subject,
                content=content
            )
    return True


@app.task
def perform_regular_payment(regular_payment, user_balance, billing_information, operation_mode, payment_system):
    from financial_system.helpers.amount import get_exchange_rate
    from financial_system.helpers.amount import get_convert_amount
    user_balance.state -= billing_information.get("amount_with_commission", 0)
    user_balance.save()
    sph = PaymentHistory(user=regular_payment.sender, profile=regular_payment.sender.profile,
                        content_object=regular_payment, action_type="regular_payment",
                        shop=None, amount=regular_payment.amount, commission=billing_information.get("commission", 0), amount_with_commission=billing_information.get("amount_with_commission", 0),
                        currency=regular_payment.currency,
                        payment_system=payment_system,
                        payment_kind="E")
    sph.save()

    if operation_mode.code == "PUC":
        puc_echange_rate = get_exchange_rate(regular_payment.currency, regular_payment.recipient.profile.currency)
        recipient_amount = get_convert_amount(regular_payment.amount,  puc_echange_rate)
        recipient_currency = regular_payment.recipient.profile.currency
    else:
        recipient_amount = regular_payment.amount
        recipient_currency = regular_payment.currency

    try:
        recipient_balance = UserBalance.objects.get(user=regular_payment.recipient, currency=recipient_currency, payment_coupon=None)
    except:
        recipient_balance = UserBalance(user=regular_payment.recipient, profile=regular_payment.recipient.profile, state=recipient_amount, currency=recipient_currency, payment_coupon=None)
        recipient_balance.save()
    else:
        recipient_balance.state += recipient_amount
        recipient_balance.save()

    rph = PaymentHistory(user=regular_payment.recipient, profile=regular_payment.recipient.profile,
                        content_object=regular_payment, action_type="regular_payment",
                        shop=None, amount=recipient_amount, commission=0, amount_with_commission=recipient_amount,
                        currency=recipient_currency,
                        payment_system=payment_system,
                        payment_kind="I")
    rph.save()
    return True


# @app.task
# def send_inform_mail(subject, template, email, options={}):
#     try:
#         subject_template_name = subject
#         email_template_name = template
#         current_site = Site.objects.get_current()
#         site_name = current_site.name
#         domain = current_site.domain
#         user = options.get('user', None)
#         is_feedback = options.get('is_feedback', None)
#
#         subject = loader.render_to_string(subject_template_name, locals())
#         subject = ''.join(subject.splitlines())
#         html_content = loader.render_to_string(email_template_name, locals())
#         if is_feedback:
#             msg = EmailMessage(subject, html_content, email, [settings.EMAIL_FROM])
#             msg.content_subtype = "html"
#             msg.send()
#         else:
#             msg = EmailMessage(subject, html_content, settings.EMAIL_FROM, [email])
#             msg.content_subtype = "html"
#             msg.send()
#         return True
#     except:
#         return False


@app.task
def reformat_user_balance(profile, system_mode):
    system_currency = Currency.objects.get(iso_code=SYSTEM_CURRENCY)
    if not profile.currency:
        profile.currency = system_currency
        profile.save()
    if system_mode == "PUC":
        main_currency = profile.currency
    elif system_mode == "PSC":
        main_currency = system_currency

    user_balances = UserBalance.objects.filter(user=profile.user, payment_coupon=None)
    payment_system = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)

    try:
        main_user_balance = [ub for ub in user_balances if ub.currency == main_currency][0]
    except:
        if user_balances.count() != 0: # if we have balance and haven't main_user_balance  than we need create main_user_balance
            main_user_balance = UserBalance(user=profile.user, profile=profile, state=0, currency=main_currency, payment_coupon=None)
            main_user_balance.save()


    for ub in user_balances:
        exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (ub.currency.iso_code, main_currency.iso_code))
        opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (main_currency.iso_code, ub.currency.iso_code))
        billing_information = get_billing_information(ub.state, exchange_rate, opposite_exchange_rate, profile.user, payment_system, "change_mode_system")
        billing_information_with_no_commission = get_billing_information(ub.state - billing_information["commission_real"], exchange_rate, opposite_exchange_rate, profile.user, payment_system, "change_mode_system")

        if ub.currency != main_currency:
            """ save to history withdrawal funds in standart currency """
            ph = PaymentHistory(user=profile.user, profile=profile, content_object=ub, action_type="change_mode_system",
                        shop=None, amount=ub.state - billing_information["commission_real"], commission=billing_information["commission_real"], amount_with_commission=ub.state, currency=ub.currency,
                        payment_system=payment_system,
                        payment_kind="E")
            ph.save()

            main_user_balance.state += billing_information_with_no_commission["amount"]

            """ save to history addding funds in main currency """
            ph = PaymentHistory(user=profile.user, profile=profile, content_object=main_user_balance, action_type="change_mode_system",
                        shop=None, amount=billing_information_with_no_commission['amount'], commission=0, amount_with_commission=billing_information_with_no_commission['amount'], currency=main_currency,
                        payment_system=payment_system,
                        payment_kind="I")
            ph.save()
            ub.delete()
        main_user_balance.save()
    return True

