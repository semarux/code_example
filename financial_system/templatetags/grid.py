from financial_system.helpers.amount import get_column_data
from django import template
register = template.Library()

@register.filter
def get_data(item, data_name):
    return get_column_data(item, data_name)

@register.filter
def check_rule(item, data_name):
    r = ""
    for name in data_name.split(" "):
        if name == "&&":
            r += "and "
        elif name == "||":
            r += "or "
        else:
            r += "%s " % get_column_data(item, name)
    return eval(r)


def get_change_value(column, order_name, order_value):
    try:
        order_value = int(order_value)
        if column.get("order_name") == order_name:
            if order_value == 0:
                return 1
            elif order_value == -1:
                return 0
            else:
                return order_value * -1
        return order_value
    except:
        return 0
#
# def get_change_value(order_value):
#     try:
#         order_value = int(order_value)
#     except:
#         return 0
#     else:
#         if order_value == 0:
#             return 1
#         elif order_value == -1:
#             return 0
#         else:
#             return order_value * -1


def get_href(column, object_columns):
    href = ""
    url = "&".join(["%s=%s" % (order_c.get("order_name", ""),
                                      get_change_value(column, order_c.get("order_name", ""), order_c.get("order_value", 0)))
                           for order_c in object_columns
                           if get_change_value(column, order_c.get("order_name", ""), order_c.get("order_value", 0)) != 0
                           and column.get("order_name", "") != order_c.get("order_name", "")
    ])

    start_url = "?"
    if get_change_value(column, column.get("order_name", ""), column.get("order_value", 0)) != 0:
        start_url = "?" + "%s=%s" % (column.get("order_name", ""), get_change_value(column, column.get("order_name", ""), column.get("order_value", 0)))

    if len(url) > 0:
        if len(start_url) > 1:
            href = start_url + "&" + url
        else:
            href = start_url + url
    else:
        href = start_url
    return href


@register.inclusion_tag('includes/grid/view.html', takes_context=True)
def grid_view(context, object_list=[], object_columns=[], object_options=[], multiple_action=False, items_on_page=5, filter_form=None):
    request = context["request"]
    for column in object_columns:
        column.update({
                "order_value": request.GET.get(column.get("order_name", ""), 0)
        })

    for column in object_columns:
        column.update({
            "href": get_href(column, object_columns),
        })

    return {
        "request": request,
        "object_list": object_list,
        "multiple_action": multiple_action,
        "object_columns": object_columns,
        "items_on_page": items_on_page,
        "object_list_count": object_list.count(),
        "filter_form": filter_form,
        "check_item_class": "mic_check",
        "object_options": object_options,
    }

@register.filter
def is_false(arg):
    return arg is False

@register.filter
def is_true(arg):
    return arg is True