import re
from django import template

register = template.Library()

@register.simple_tag
def active(request, pattern, class_name='active'):
    if re.search(str(pattern), request.path):
        return class_name
    return ''
