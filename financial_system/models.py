# --coding: utf-8
from decimal import *
import os
from tarfile import _section
import uuid
from django.db import models
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from main.models import Currency, Profile
from django.contrib.contenttypes.models import ContentType
from sorl.thumbnail import get_thumbnail
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from financial_system.helpers.datetampering import DateTampering
from financial_system.helpers.system_functs import get_obj_or_none
from django.core.validators import MinValueValidator
from phonenumber_field.modelfields import PhoneNumberField



def get_file_name(filename):
    ext = filename.split(".")[-1]
    return u"%s.%s" % (uuid.uuid4(), ext)


def get_slider_file_path(instance, filename):
    return os.path.join("slider", get_file_name(filename))


def get_payment_sys_file_path(instance, filename):
    return os.path.join("payment_sys", get_file_name(filename))


INFORMATION_BLOCK_TYPE_CHOICE = [
    ("MS", _("Main Slider")),
    ("MIB", _("Main Information Block"))
]

SHOP_STATUS_CHOICE = [
    ("t", _("Test")),
    ("a", _("Active")),
    ("na", _("Not active"))
]


SHOP_PAYMENT_STATUS_CHOICE = [
    ("t", _("Test")),
    ("r", _("Real")),
]



LANGUAGES = (
    ('ru', _('Russian')),
    ('en', _('English')),
)


WITHDRAWAL_STATUS_CHOICE = [
    ("paid", _("Paid: payment is paid")),
    ("created", _("Created: payment is created")),
    ("frozen", _("Frozen: payment is frozen")),
    ("canceled", _("Canceled: payment canceled by the sender")),
    ("offset", _("Offset: payment was offset to sender")),
]


INVOICE_STATUS_CHOICE = [
    ("created", _("Created: bill is created")),
    ("paid", _("Paid: bill is paid")),
    ("canceled", _("Canceled: bill canceled")),
]

ADDINGFUND_STATUS_CHOICE = [
    ("created", _("Created: payment was created")),
    ("failure", _("Failure: payment was failure")),
    ("wait_secure", _("Wait secure: payment was wait secure")),
    ("success", _("Success: payment  is made successfully")),
    ("paid", _("Success: payment  is made successfully")),
    ("rejected", _("Rejected: payment is was reject")),
    ("unpaid", _("Unpaid: Error in the payment.")),
    ("expired", _("Expired: Lifetime account has expired.")),
]

TYPE_PERIODICITY_CHOICE = [
    ("D", _("Day")),
    ("W", _("Week")),
    ("M", _("Month")),
    ("Q", _("Quarter")),
    ("Y", _("Year")),
]

PAYMENT_KIND_CHOICE = [
    ("I", _("Income")),
    ("E", _("Expenditure")),
]

PAYMENT_COUPON_KIND_CHOICE = [
    ("E", _("Escrow")),
]


TYPE_COMMISSION_CHOICE = [
    ("P", _("Percent")),
    ("M", _("Money")),
]

ACTION_TYPE_CHOICE = [
    ("regular_payment", _("Regular payment")),
    ("adding_funds", _("Adding funds")),
    ("withdrawal", _("Withdrawal")),
    ("transfer_funds", _("Transfer funds within the system")),
    ("bill_paying", _("Bill paying")),
    ("exchange_funds", _("Exchange Funds")),
    ("change_mode_system", _("Change operation mode system")),
    ("foot_invoice", _("Foot the invoice")),
    ("shop_payment", _("Shop payment"))
]

MESSAGE_TYPE_CHOICE = [
    ("insufficient_funds", _("Your account is not enough money")),
    ("not_funds_by_this_currency", _("You haven't funds in this currency")),
]

BANK_REQUISITES = [
    ("reciplient_name", _("Name of reciplient")),
    ("bank_name", _("Name of bank")),
    ("swift_name", _("SWIFT of bank")),
    ("inn", _("INN")),
    ("mfo", _("MFO")),
]


class OperatingMode(models.Model):
    name = models.CharField(verbose_name=_("Name"), help_text=_("Enter the operation mode of financial system"), max_length=100)
    code = models.CharField(verbose_name=_("Code"), help_text=_("Enter code of operation system"), unique=True, max_length=100)
    description = models.TextField(verbose_name=_("Description"), help_text=_("Enter description of the operation mode"), blank=True, null=True)
    active = models.BooleanField(verbose_name=_("Is activated"), help_text=_("Put activity of the operation mode"), default=False)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Operation mode")
        verbose_name_plural = _("Operation modes")
        ordering = ("name",)

    def get_code(self):
        return u"%s" %self.code

    def __unicode__(self):
        return u"%s" % self.name


def reformat_users_balances(sender, instance, **kwargs):
    from tasks import reformat_user_balance
    from torgplace.settings import SYSTEM_CURRENCY
    post_save.disconnect(reformat_users_balances, sender=sender)
    instance.save()
    profiles = Profile.objects.all().exclude(system_owner=1)

    if (instance.code == "PUC" and instance.active) or (instance.code == "PSC" and instance.active):
        for profile in profiles:
            reformat_user_balance.delay(profile, instance.code)

    post_save.connect(reformat_users_balances, sender=sender)

post_save.connect(reformat_users_balances, sender=OperatingMode)



class InformationBlock(models.Model):
    name = models.CharField(verbose_name=_("Name"), help_text=_("Enter  name of the information block"), max_length=100)
    title = models.CharField(verbose_name=_("Title"), help_text=_("Enter title of the information block"), max_length=250)
    short_description = models.TextField(verbose_name=_("Short description"), help_text=_("Enter short description of the information block"), max_length=500, blank=True, null=True)
    description = models.TextField(verbose_name=_("Description"), help_text=_("Enter description  of the information block"), blank=True, null=True)
    image = models.ImageField(verbose_name=_("Image"), help_text=_("Choose the image of the information block"), upload_to=get_slider_file_path, blank=True, null=True)
    active = models.BooleanField(verbose_name=_("Is activated"), help_text=_("Put activity of the information block"), default=True)
    order = models.IntegerField(verbose_name=_("Order"), help_text=_("Output order"), default=0)
    type = models.CharField(verbose_name=_("Type information block"), help_text=_("Choose type of the information block"), choices=INFORMATION_BLOCK_TYPE_CHOICE, max_length=5, blank=True, null=True)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Information block")
        verbose_name_plural = _("Information blocks")
        ordering = ("name", "order")

    def __unicode__(self):
        return u"%s" % self.name


class PaymentSystem(models.Model):
    name = models.CharField(verbose_name=_("Name"), help_text=_("Enter name of the payment system"), max_length=100)
    sys_code = models.CharField(verbose_name=_("System code"), help_text=_("Enter system code"), unique=True, max_length=100)
    description = models.TextField(verbose_name=_("Description"), help_text=_("Enter description of the payment system"), blank=True, null=True)
    description_payment = models.TextField(verbose_name=_("Description for payment"), help_text=_("Enter description of the payment system"),)
    image = models.ImageField(verbose_name=_("Image"), help_text=_("Choose the image of the payment system"), upload_to=get_payment_sys_file_path, blank=True, null=True)
    withdrawals_min = models.DecimalField(verbose_name=_("Min amount for withdrawals"), max_digits=11, decimal_places=2, help_text=_("Minimum amount limit for withdrawal"))
    withdrawals_max = models.DecimalField(verbose_name=_("Max amount for withdrawals"), max_digits=11, decimal_places=2, help_text=_("Maximum amount limit for withdrawal"))
    withdrawals_currency = models.ForeignKey(Currency, related_name="withdrawals_currency", verbose_name=_("Currency widthdrawal"), help_text=_("Currency restrictions withdrawal"))
    adding_funds_min = models.DecimalField(verbose_name=_("Min amount for adding funds"), max_digits=11, decimal_places=2, help_text=_("Minimum amount limit for  adding funds"))
    adding_funds_max = models.DecimalField(verbose_name=_("Max amount for adding funds"), max_digits=11, decimal_places=2, help_text=_("Maximum amount limit for adding funds"))
    adding_funds_currency = models.ForeignKey(Currency, related_name="adding_funds_currency", verbose_name=_("Currency adding funds"), help_text=_("Currency restrictions adding funds"))
    for_adding_funds = models.BooleanField(verbose_name=_("For adding funds"), default=True)
    for_withdrawal_funds = models.BooleanField(verbose_name=_("For withdrawal funds"), default=True)
    for_shop_payment = models.BooleanField(verbose_name=_("For shop payment"), default=True)
    active = models.BooleanField(verbose_name=_("Is activated"), help_text=_("Put activity of the payment system"), default=True)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Payment system")
        verbose_name_plural = _("Payment systems")
        ordering = ("id",)

    def __unicode__(self):
        return u"%s" % self.name


    def get_data_name(self):
        return "%s" % self.name

    def get_image(self):
        if self.image:
            return u"<img src='%s' />" % get_thumbnail(self.image, "50x50", crop="center", quality=90).url
        else:
            return _("Sin imagen")
    get_image.short_description = _("Image")
    get_image.allow_tags = True


class CommissionSetting(models.Model):
    action_type = models.CharField(verbose_name=_("Type action"), choices=ACTION_TYPE_CHOICE, max_length=50)
    payment_coupon_kind = models.CharField(verbose_name=_("Kind coupon of payment"), choices=PAYMENT_COUPON_KIND_CHOICE, max_length=2, blank=True, null=True)
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"))
    type_commission = models.CharField(verbose_name=_("Type commission"), choices=TYPE_COMMISSION_CHOICE, max_length=1)
    currency_commission = models.ForeignKey(Currency, related_name="currency_commission", verbose_name=_("Currency"), blank=True, null=True)
    currency_commission_range = models.ForeignKey(Currency, related_name="currency", verbose_name=_("Currency range amount"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Commission setting")
        verbose_name_plural = _("Commission settings")
        # ordering = ("commission",)
        unique_together = (("action_type", "payment_coupon_kind", "payment_system"),)

    def __unicode__(self):
        if self.type_commission == "P":
            return u"%s" % (self.action_type,)
        else:
            return u"%s %s" % (self.action_type, self.type_commission)

    # def get_commission(self):
    #     if self.commission:
    #         if self.type_commission == "P":
    #             return u"%s %s" % (self.commission, "%")
    #         else:
    #             return u"%s %s" % (self.commission, self.currency_commission.iso_code)
    #     else:
    #         return u"0"
    #
    # get_commission.short_description = _("Commission")
    # get_commission.allow_tags = True
    #
    # def get_range_amount(self):
    #     return u"%s - %s" % (self.range_amount, self.currency_range_amount.iso_code)
    #
    # get_range_amount.short_description = _("Range amount")
    # get_range_amount.allow_tags = True


class CommissionRange(models.Model):
    commission_setting = models.ForeignKey(CommissionSetting, verbose_name="commission_setting")
    commission = models.DecimalField(verbose_name=_("Sum commission"),  max_digits=11, decimal_places=5)
    min_amount = models.DecimalField(verbose_name=_("Min amount"), max_digits=11, decimal_places=2, help_text="1.00")
    max_amount = models.DecimalField(verbose_name=_("Max amount"), max_digits=11, decimal_places=2, help_text="1000.00")


    class Meta:
        app_label = "financial_system"
        verbose_name = _("Commission range")
        verbose_name_plural = _("commission ranges")

    def __unicode__(self):
        return u"%s-%s-%s" % (self.commission_setting.action_type, self.min_amount, self.max_amount)


class UserBalance(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))
    state = models.DecimalField(verbose_name=_("State of the user balance"), default="0.00", max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("currency"))
    payment_coupon = models.CharField(verbose_name=_("Kind coupon of payment"), choices=PAYMENT_COUPON_KIND_CHOICE, max_length=2, blank=True, null=True)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("User balance")
        verbose_name_plural = _("Users balance")
        unique_together = (("user", "currency", "payment_coupon"),) #"payment_coupon"
        ordering = ("-state",)

    def __unicode__(self):
        return u"%s-%s-%s" % (self.user.username, self.state, self.currency.name)


    @property
    def user_email(self):
        return u"%s" % self.user.email

    # @property
    # def currency_iso_code(self):
    #     return u"#s" % self.currency.iso_code

    def get_state(self):
        return "%s %s" % (self.state, self.currency.iso_code)



class SystemOwnerBalance(UserBalance):
    class Meta:
        proxy = True
        verbose_name = _("System owner balance")
        verbose_name_plural = _("System owner balance")


class Withdrawal(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))
    manager = models.ForeignKey(User, verbose_name=_("Manager"), related_name="withdrawal_manager", blank=True, null=True)

    amount_to_withdrawal = models.DecimalField(verbose_name=_("Amount to withdrawal"), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    commission_to_withdrawal = models.DecimalField(verbose_name=_("Commission to withdrawal"), default=Decimal(0),  max_digits=11, decimal_places=2)
    amount_with_commission_to_withdrawal = models.DecimalField(verbose_name=_("Amount with commission to withdrawal"), default=Decimal(0),  max_digits=11, decimal_places=2)
    currency_to_withdrawal = models.ForeignKey(Currency, related_name="currency_to_withdrawal", verbose_name=_("Currency to withdrawal"))

    amount_to_obtain = models.DecimalField(verbose_name=_("Amount to obtain "), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    currency_to_obtain = models.ForeignKey(Currency, related_name="currency_to_obtain", verbose_name=_("Currency to obtain"))

    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"))
    purse_from = models.CharField(verbose_name=_("Number from/Bill payer"), blank=True, null=True, max_length=50)
    purse = models.CharField(verbose_name=_("Number purse/Bill"), max_length=50)
    bank_requisite = models.TextField(blank=True, null=True)
    comment = models.TextField(verbose_name=_("Comment"))
    paid = models.BooleanField(verbose_name=_("Paid"), default=0)
    status = models.CharField(verbose_name=_("Status"), choices=WITHDRAWAL_STATUS_CHOICE, default="created", max_length=15, blank=True, null=True)
    withdrawn = models.BooleanField(verbose_name=_("withdrawn"), default=0)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create withdrawal"))
    date_pay = models.DateTimeField(blank=True, null=True, verbose_name=_("Date pay"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Withdrawal")
        verbose_name_plural = _("Withdrawals")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s-%s" % (self.user.username, self.amount_to_obtain, self.status)

    @property
    def get_status(self):
        return u"%s" % dict(WITHDRAWAL_STATUS_CHOICE).get(self.status, "")

    def get_amount_to_withdrawal(self):
        return u"%s %s" % (self.amount_to_withdrawal, self.currency_to_withdrawal.iso_code)

    def get_amount_with_commission_to_withdrawal(self):
        return u"%s %s" % (self.amount_with_commission_to_withdrawal, self.currency_to_withdrawal.iso_code)

    def get_commission_to_withdrawal(self):
        return u"%s %s" % (self.commission_to_withdrawal, self.currency_to_withdrawal.iso_code)

    def get_amount_to_obtain(self):
        return u"%s %s" % (self.amount_to_obtain, self.currency_to_obtain.iso_code)


def withdrawal_funds(sender, instance, **kwargs):
    post_save.disconnect(withdrawal_funds, sender=sender)
    if instance.paid == True and instance.withdrawn == False:
        instance.withdrawn = True
        try:
            ub = UserBalance.objects.get(user=instance.user, currency=instance.currency_to_withdrawal, payment_coupon=None)
        except:
            pass
        else:
            ub.state -= instance.amount_with_commission_to_withdrawal
            ub.save()
            ph = PaymentHistory(user=instance.user, profile=instance.profile, content_object=instance, action_type="withdrawal",
                                shop=None, amount=instance.amount_to_withdrawal,
                                commission=instance.commission_to_withdrawal,
                                amount_with_commission=instance.amount_with_commission_to_withdrawal, currency=instance.currency_to_withdrawal,
                                payment_system=instance.payment_system,
                                payment_kind="E")
            ph.save()

            rh = RealPurseHistory(payment_system=instance.payment_system,
                                  purse_from=instance.purse_from,
                                  purse_to=instance.purse,
                                  amount=instance.amount_to_obtain,
                                  currency=instance.currency_to_obtain,
                                  payment_kind="E")
            rh.save()

    instance.save()
    post_save.connect(withdrawal_funds, sender=sender)
post_save.connect(withdrawal_funds, sender=Withdrawal)


class StatisticWithdrawal(Withdrawal):
    def __unicode__(self):
        return u"------"

    class Meta:
        proxy = True
        verbose_name = _("Statistic withdrawal")
        verbose_name_plural = _("Statistic withdrawal")


class TransferFund(models.Model):
    sender = models.ForeignKey(User, related_name="sender_transfer", verbose_name=_("User"))
    recipient = models.ForeignKey(User, related_name="recipient_transfer", verbose_name=_("Recipient"))
    amount = models.DecimalField(verbose_name=_("Amount"), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    commission = models.DecimalField(verbose_name=_("Commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    amount_with_commission = models.DecimalField(verbose_name=_("Amount with commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"))
    comment = models.TextField(verbose_name=_("Comment"))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Transfer fund")
        verbose_name_plural = _("Transfer funds")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s" % (self.amount, self.currency.iso_code)

    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency.iso_code)

    def get_amount_with_commission(self):
        return u"%s %s" % (self.amount_with_commission, self.currency.iso_code)


class StatisticTransferFund(TransferFund):
    sum = 0

    def __unicode__(self):
        return u"%s %s" % (self.currency, self.sum)

    class Meta:
        proxy = True
        verbose_name = _("Statistic transfer funds")
        verbose_name_plural = _("Statistic transfer funds")

# Maybe this class we need in future
# class PaymentSystemField(models.Model):
#     payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"))
#     action_type = models.CharField(verbose_name=_("Type action"), choices=ACTION_TYPE_CHOICE, max_length=50)
#     name = models.CharField(verbose_name=_("Name"), help_text=_("Enter name of the shop"), max_length=100)
#     date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create withdrawal"))
#     active = models.BooleanField(verbose_name=_("Is activated"), help_text=_("Is you want to field be active"), default=True)
#
#     class Meta:
#         app_label = "financial_system"
#         verbose_name = _("Payment System Field")
#         verbose_name_plural = _("Payment System Fields")
#         ordering = ("-date_create",)
#
#     def __unicode__(self):
#         return u"%s" % (self.name,)
#
#
# class PaymentSystemFieldValue(models.Model):
#     payment_system_field = models.ForeignKey(PaymentSystemField, verbose_name=_("Payment system field"))
#
#     content_type = models.ForeignKey(ContentType)
#     object_id = models.PositiveIntegerField()
#     content_object = generic.GenericForeignKey("content_type", "object_id")
#
#     value = models.CharField(verbose_name=_("Value"), help_text=_("Value"), max_length=100)
#
#     class Meta:
#         app_label = "financial_system"
#         verbose_name = _("Payment system field")
#         verbose_name_plural = _("Payment system field Value")
#
#
#     def __unicode__(self):
#         return u"%s" % (self.name,)


class RegularPayment(models.Model):
    sender = models.ForeignKey(User, related_name="user", verbose_name=_("User"))
    recipient = models.ForeignKey(User, related_name="recipient", verbose_name=_("Recipient"))
    amount = models.DecimalField(verbose_name=_("Amount"), help_text=_("Sum regular payment"),  max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"))
    comment = models.TextField(verbose_name=_("Comment"))
    type_periodicity = models.CharField(verbose_name=_("Type periodicity"), choices=TYPE_PERIODICITY_CHOICE, max_length=1)
    date_start_payment = models.DateField(blank=True, null=True, verbose_name=_("Date start payment"))
    date_next_payment = models.DateField(blank=True, null=True, verbose_name=_("Date next payment"))
    date_last_payment = models.DateField(blank=True, null=True, verbose_name=_("Date last payment"))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create regular payment"))
    active = models.BooleanField(verbose_name=_("Is activated"), help_text=_("Active payment"), default=True)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Regular payment")
        verbose_name_plural = _("Regular payments")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s-%s" % (self.sender.username, self.amount, self.date_create)

    @property
    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency)

    @property
    def get_date_next_payment(self):
        return u"get_date_next_payment"


class OrderRegularPayment(models.Model):
    regular_payment = models.ForeignKey(RegularPayment, related_name="regular_payment", verbose_name=_("Regular payment"))
    amount = models.DecimalField(verbose_name=_("Amount"), help_text=_("Sum regular payment"),  max_digits=11, decimal_places=2)
    commission = models.DecimalField(verbose_name=_("Commission"), default=Decimal(0), null=True, blank=True,  max_digits=11, decimal_places=2)
    amount_with_commission = models.DecimalField(verbose_name=_("Amount with commission"), null=True, blank=True, default=Decimal(0),  max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"))
    paid = models.BooleanField(verbose_name=_("Paid"), default=0)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date regular payment"))

    def __unicode__(self):
        return u"%s-%s-%s" % (self.regular_payment.sender.username, self.amount, self.date_create)

    def get_sender(self):
        return u"%s" % self.regular_payment.sender.username

    def get_recipient(self):
        return u"%s" % self.regular_payment.recipient.username
    @property
    def get_recipient_name(self):
        return u"%s" % self.regular_payment.recipient.username

    @property
    def get_sender_name(self):
        return u"%s" % self.regular_payment.sender.username

    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency.iso_code)

    def get_commission(self):
        return u"%s %s" % (self.commission, self.currency.iso_code)

    def get_amount_with_commission(self):
        return u"%s %s" % (self.amount_with_commission, self.currency.iso_code)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("order of regular payment ")
        verbose_name_plural = _("Orders of regular payments")
        ordering = ("-date_create",)

    get_sender.short_description = _("Sender")
    get_recipient.short_description = _("Recipient")
    get_amount.short_description = _("Amount")
    get_commission.short_description = _("Commission")
    get_amount_with_commission.short_description = _("Amount with commission")
    get_amount_with_commission.short_description = _("Amount with commission")


class StatisticOrderRegularPayment(OrderRegularPayment):
    def __unicode__(self):
        return u"------"

    class Meta:
        proxy = True
        verbose_name = _("Statistic order regular payment")
        verbose_name_plural = _("Statistic orders regular payment")


def set_date_next_payment(sender, instance, **kwargs):
    post_save.disconnect(set_date_next_payment, sender=sender)
    if instance.date_last_payment:
        instance.date_next_payment = DateTampering(instance.date_last_payment).get_modified_date(instance.type_periodicity)
    else:
        instance.date_next_payment = DateTampering(instance.date_start_payment).get_modified_date(instance.type_periodicity)
    instance.save()
    post_save.connect(set_date_next_payment, sender=sender)
post_save.connect(set_date_next_payment, sender=RegularPayment)


class Shop(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))
    name = models.CharField(verbose_name=_("Name"), help_text=_("Enter name of the shop"), max_length=100)
    description = models.TextField(verbose_name=_("Description"), help_text=_("Enter description of shop"), blank=True, null=True)
    shop_url = models.URLField(verbose_name=_("Url"), help_text=_("Enter shop url"), max_length=100, unique=True)
    password = models.CharField(verbose_name=_("Password"), help_text=_("Enter password"), max_length=100)
    secret_key = models.CharField(verbose_name=_("Secret key"), help_text=_("Secret key"), max_length=255, unique=True)
    success_url = models.URLField(verbose_name=_("Success url"), blank=True, null=True,  max_length=100)
    fail_url = models.URLField(verbose_name=_("Fail url"), blank=True, null=True, max_length=100)
    status = models.CharField(verbose_name=_("Shop status"), choices=SHOP_STATUS_CHOICE, max_length=2)
    shop_informer = models.BooleanField(verbose_name=_("Shop informer"), help_text=_("Check to active"))
    informer_language = models.CharField(_('Default Language'), choices=LANGUAGES, default='en', max_length=3)
    informer_css = models.TextField(verbose_name=_("Informer css"), help_text=_("Informer css"), default="", blank=True, null=True)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create shop"))
    date_update = models.DateTimeField(auto_now=True, verbose_name=_("Date update"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Shop")
        verbose_name_plural = _("Shops")
        ordering = ("date_create",)

    def __unicode__(self):
        return u"%s" % self.name

    @property
    def get_status(self):
        return u"%s" % dict(SHOP_STATUS_CHOICE).get(self.status, "")


class Invoice(models.Model):
    owner = models.ForeignKey(User, verbose_name=_("Invoice owner"), related_name="owner")
    payer = models.ForeignKey(User, verbose_name=_("Invoice payer"),  related_name="payer")
    moderator = models.ForeignKey(User, verbose_name=_("Invoice moderator"), blank=True, null=True,  related_name="moderator")

    amount = models.DecimalField(verbose_name=_("Sum invoice payment"), max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"),)

    payment_amount = models.DecimalField(verbose_name=_("Sum invoice payment"), max_digits=11, decimal_places=2, blank=True, null=True)
    payment_commission = models.DecimalField(verbose_name=_("Commission"), help_text=_("Commission"), max_digits=11, decimal_places=2, blank=True, null=True)
    payment_amount_with_commission = models.DecimalField(verbose_name=_("Amount width commission"), help_text=_("Amount payment with commission"), max_digits=11, decimal_places=2, blank=True, null=True)
    payment_currency = models.ForeignKey(Currency, verbose_name=_("Payment currency"), related_name="payment_currency", blank=True, null=True)

    short_description = models.CharField(verbose_name=_("Short description"), max_length=500)
    comment = models.TextField(verbose_name=_("Comment"), blank=True, null=True)

    shop = models.ForeignKey(Shop, verbose_name=_("Shop"), blank=True, null=True)
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = generic.GenericForeignKey("content_type", "object_id")

    status = models.CharField(verbose_name=_("Status"), choices=INVOICE_STATUS_CHOICE, default="created", max_length=15, blank=True, null=True)
    paid = models.BooleanField(verbose_name=_("Paid"), default=0)
    # active = models.BooleanField(verbose_name=_("Is active"), default=True)
    completed = models.BooleanField(verbose_name=_("Completed"), default=False)
    payment_coupon = models.CharField(verbose_name=_("Kind coupon of payment"), choices=PAYMENT_COUPON_KIND_CHOICE, max_length=2, blank=True, null=True)

    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create payment invoice"))
    date_pay = models.DateTimeField(verbose_name=_("Date paid"), blank=True, null=True)
    date_update = models.DateTimeField(auto_now=True, verbose_name=_("Date update payment invoice"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Invoice")
        verbose_name_plural = _("Invoices")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s-%s" % (self.owner.username, self.amount, self.date_create)

    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency.iso_code)

    def get_payment_amount(self):
        if self.payment_amount and self.payment_currency.iso_code:
            return u"%s %s" % (self.payment_amount, self.payment_currency.iso_code)
        else:
            return None

    def get_payment_commission(self):
        if self.payment_commission and self.payment_currency.iso_code:
            return u"%s %s" % (self.payment_commission, self.payment_currency.iso_code)
        else:
            return None

    def get_payment_amount_with_commission(self):
        if self.payment_amount_with_commission and self.payment_currency.iso_code:
            return u"%s %s" % (self.payment_amount_with_commission, self.payment_currency.iso_code)
        else:
            return None

    def get_shop(self):
        if self.shop:
            return u"%s" % self.shop.name
        else:
            return None

    @property
    def not_paid(self):
        return self.paid == False

    @property
    def not_completed(self):
        return self.completed == False

    @property
    def get_status(self):
        return u"%s" % dict(INVOICE_STATUS_CHOICE).get(self.status, "")

    def get_status_for_admin(self):
        return self.get_status

    def get_username(self):
        return u"%s" % self.recipient.username

    get_amount.short_description = _("Amount")
    get_payment_amount.short_description = _("Payment amount")
    get_payment_commission.short_description = _("Payment commission")
    get_payment_amount_with_commission.short_description = _("Payment amount with commission")
    get_shop.short_description = _("Shop")
    get_status_for_admin.short_description = _("Status")



class InvoiceDiscussion(models.Model):
    invoice = models.ForeignKey(Invoice, verbose_name=_("Invoice"))
    user = models.ForeignKey(User, verbose_name=_("User"))
    comment = models.TextField(verbose_name=_("Comment"))
    is_moderator = models.BooleanField(verbose_name="Is moderator?", default=False)
    date_create = models.DateTimeField(auto_now_add=True,  verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Invoice discussion")
        verbose_name_plural = _("Invoice discussions")
        ordering = ["date_create"]

    def __unicode__(self):
        return u"%s" % self.comment

class Bank(models.Model):
    name = models.CharField(verbose_name=_("Name"), help_text=_("Enter bank name"), max_length=100)
    description = models.TextField(verbose_name=_("Description"), help_text=_("Enter description of bank"), blank=True, null=True)
    address = models.CharField(verbose_name=_("Address"), help_text=_("Address"), max_length=100)
    account_number = models.CharField(verbose_name=_("Account number"), help_text=_("Account number"), max_length=100)
    swift = models.CharField(verbose_name=_("SWIFT "), help_text=_("SWIFT"), max_length=100)
    active = models.BooleanField(verbose_name=_("Is activated"), default=True)
    currencies = models.ManyToManyField(Currency, verbose_name=_("Currency"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Bank")
        verbose_name_plural = _("Banks")

    def __unicode__(self):
        return u"%s" % self.name



class AddingFund(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))
    transaction = models.CharField(verbose_name=_("Transaction"),  max_length=500, blank=True, null=True)
    amount = models.DecimalField(verbose_name=_("Amount payment"), max_digits=11, decimal_places=2)
    commission = models.DecimalField(verbose_name=_("Commission"), help_text=_("Commission"), max_digits=11, decimal_places=2)
    amount_with_commission = models.DecimalField(verbose_name=_("Amount width commission"), help_text=_("Amount payment with commission"), max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"),)
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"), blank=True, null=True)
    bank = models.ForeignKey(Bank, verbose_name=_("Bank"), blank=True, null=True)
    purse_from = models.CharField(verbose_name=_("Purse from"), max_length=50, blank=True, null=True)
    purse_to = models.CharField(verbose_name=_("Purse to"), max_length=50, blank=True, null=True)
    ps_amount = models.DecimalField(verbose_name=_("PS sum"), max_digits=11, decimal_places=2, blank=True, null=True)
    ps_currency = models.ForeignKey(Currency, verbose_name=_("Currency"), related_name="ps_currency",  blank=True, null=True)
    shop = models.ForeignKey(Shop, verbose_name=_("Shop"), blank=True, null=True)
    status = models.CharField(verbose_name=_("Status"), choices=ADDINGFUND_STATUS_CHOICE, default="created", max_length=15, blank=True, null=True)
    paid = models.BooleanField(verbose_name=_("Paid"), default=0)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))
    date_pay = models.DateTimeField(blank=True, null=True, verbose_name=_("Date pay"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Adding fund")
        verbose_name_plural = _("Adding funds")
        ordering = ["-date_create"]

    def __unicode__(self):
        return u"%s" % self.amount

    def get_amount(self):
        return "%s %s" % (self.amount, self.currency.iso_code)

    @property
    def get_status(self):
        return u"%s" % dict(ADDINGFUND_STATUS_CHOICE).get(self.status, "")

def adding_funds(sender, instance, **kwargs):
    from helpers.amount import deposit_money
    post_save.disconnect(adding_funds, sender=sender)
    if instance.paid == True and not instance.date_pay:
        deposit_money(order=instance)
    instance.save()
    post_save.connect(adding_funds, sender=sender)
post_save.connect(adding_funds, sender=AddingFund)




class StatisticAddingFund(AddingFund):
    def __unicode__(self):
        return u"------"

    class Meta:
        proxy = True
        verbose_name = _("Statistic adding funds")
        verbose_name_plural = _("Statistic adding funds")


class PaymentHistory(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    profile = models.ForeignKey(Profile, verbose_name=_("Profile"))

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey("content_type", "object_id")

    action_type = models.CharField(verbose_name=_("Type action"), choices=ACTION_TYPE_CHOICE, max_length=50)

    shop = models.ForeignKey(Shop, blank=True, null=True)
    amount = models.DecimalField(verbose_name=_("Sum payment"), max_digits=11, decimal_places=2)
    commission = models.DecimalField(verbose_name=_("Commission"), max_digits=11, decimal_places=2)
    amount_with_commission = models.DecimalField(verbose_name=_("Amount with commission"), help_text=_("Sum payment with commission"), max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"),)
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"), blank=True, null=True)
    # purse_from = models.CharField(verbose_name=_("Purse from"), max_length=50, blank=True, null=True)
    # purse_to = models.CharField(verbose_name=_("Purse to"), max_length=50, blank=True, null=True)
    # ps_amount = models.DecimalField(verbose_name=_("PS sum"), max_digits=11, decimal_places=2, blank=True, null=True)
    # ps_currency = models.ForeignKey(Currency, verbose_name=_("Currency"), related_name="ph_ps_currency", blank=True, null=True)
    payment_kind = models.CharField(verbose_name=_("Kind payment"), choices=PAYMENT_KIND_CHOICE, max_length=1)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Payment history")
        verbose_name_plural = _("Payments history")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s-%s" % (self.user, self.amount, self.date_create)
    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency.iso_code)

    def get_commission(self):
        return u"%s %s" % (self.commission, self.currency.iso_code)

    def get_amount_with_commission(self):
        return u"%s %s" % (self.amount_with_commission, self.currency.iso_code)

    @property
    def get_action_type(self):
        return dict(ACTION_TYPE_CHOICE).get(self.action_type, "")

    @property
    def get_payment_kind(self):
        return dict(PAYMENT_KIND_CHOICE).get(self.payment_kind, "")

    @property
    def get_email(self):
        return u"%s" % self.user.email


    get_amount.short_description = _("Amount")
    get_commission.short_description = _("Commission")
    get_amount_with_commission.short_description = _("Amount with commission")


def save_payment_history_for_system_owner(sender, instance, **kwargs):
    # function for calculating commissions for owner system
    post_save.disconnect(save_payment_history_for_system_owner, sender=sender)
    system_owner = get_obj_or_none(Profile, system_owner=1)
    if system_owner and instance.profile != system_owner:
        if instance.commission > 0:
            #reserve commission in the payment history system owner
            system_owner_ph = PaymentHistory(user=system_owner.user, profile=system_owner, content_object=instance.content_object, action_type=instance.action_type,
                                shop=instance.shop, amount=instance.commission, commission=0, amount_with_commission=instance.commission, currency=instance.currency,
                                payment_system=instance.payment_system,
                                payment_kind="I")
            system_owner_ph.save()
            try:
                ub = UserBalance.objects.get(user=system_owner.user, currency=instance.currency, payment_coupon=None)
            except:
                ub = UserBalance(user=system_owner.user, profile=system_owner, state=instance.commission, currency=instance.currency, payment_coupon=None)
                ub.save()
            else:
                ub.state += instance.commission
                ub.save()

    post_save.connect(save_payment_history_for_system_owner, sender=sender)
post_save.connect(save_payment_history_for_system_owner, sender=PaymentHistory)


WM_TYPE_PURSE_CHOICE = [
    ("WMR", _("WMR  - funds equivalent to Russian rubles")),
    ("WMZ", _("WMZ -  funds equivalent to USA dollars")),
    ("WME", _("WME -  funds equivalent to Euro")),
    ("WMU", _("WMU -  funds equivalent to Ukrainian hryvnia")),
    ("WMB", _("WMB -  funds equivalent to Belarusian ruble")),
    ("WMX", _("WMX -  funds equivalent to in bitcoin.org")),
]


QM_TYPE_PURSE_CHOICE = [
    ("QMR", _("RUB  - qiwi rubles")),
    ("QME", _("EUR -  qiwi euro")),
    ("QMK", _("KZT -  qiwi tenge")),
    ("QMU", _("USD -  qiwi dollars")),
]

QM_REAL_CURRENCY_CHOICE = {
    "QMR": "RUB",
    "QME": "EUR",
    "QMK": "KZT",
    "QMU": "USD",
}

LP_TYPE_CURRENCY_CHOICE = [
    ("LPRUB", _("RUB  - liqpay Russian rubles")),
    ("LPEUR", _("EUR -  liqpay euro")),
    ("LPUSD", _("USD -  liqpay USA dollars")),
    ("LPUAH", _("UAH -  liqpay Ukrainian grivna")),
]
LP_REAL_CURRENCY_CHOICE = {
    "LPRUB": "RUB",
    "LPEUR": "EUR",
    "LPUSD": "USD",
    "LPUAH": "UAH",
}


class Purse(models.Model):
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"))
    type = models.CharField(verbose_name=_("Type of purse"), max_length=5, unique=True)
    username = models.CharField(verbose_name=_("Username or number"), max_length=25)
    password = models.CharField(verbose_name=_("Password or secret key"), max_length=100)
    signature = models.CharField(verbose_name=_("Signature"), max_length=1000, blank=True, null=True)
    url = models.CharField(verbose_name=_("Url"), max_length=100)
    result_url = models.CharField(verbose_name=_("Result url"), max_length=100)
    success_url = models.CharField(verbose_name=_("Success url"), blank=True, null=True, max_length=100)
    fail_url = models.CharField(verbose_name=_("Fail url"), blank=True, null=True, max_length=100)
    server_url = models.CharField(verbose_name=_("Server url"), max_length=100, blank=True, null=True)
    description = models.TextField(verbose_name=_("Description"), max_length=100)
    is_test = models.BooleanField(verbose_name=_("Is test"))
    is_work = models.BooleanField(verbose_name=_("Is work"))
    active = models.BooleanField(verbose_name=_("Active"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Purse")
        verbose_name_plural = _("Purses")
        unique_together = (("type", "is_test"), ("type", "is_work"))

    def __unicode__(self):
        return u"%s" % self.username


class ProfileAccountSettings(models.Model):
    user = models.OneToOneField(User)
    profile = models.OneToOneField(Profile)
    escrow = models.BooleanField(verbose_name=_("Escrow"), help_text=_("You work in escrow mode"))
    # shop_informer = models.BooleanField(verbose_name=_("Shop informer"), help_text=_("Check you shop informer"))


class UserEventMessage(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    action_type = models.CharField(verbose_name=_("Type action"), choices=ACTION_TYPE_CHOICE, max_length=50)
    message_type = models.CharField(verbose_name=_("Type action"), choices=MESSAGE_TYPE_CHOICE, max_length=50)
    subject = models.CharField(verbose_name=_("Subject"), max_length=250)
    content = models.TextField(verbose_name=_("Content"), max_length=1000)
    read_at = models.DateTimeField(verbose_name=_("Read at"), blank=True, null=True)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("User event message")
        verbose_name_plural = _("User event messsages")

    def __unicode__(self):
        return u"%s" % self.action_type

    def is_read(self):
        if self.read_at:
            return True
        return False


class ExchangeRate(models.Model):
    iso_code = models.CharField(_('ISO code'), max_length=10, unique=True)
    currency_from = models.ForeignKey(Currency, verbose_name=_("Currency from"), related_name="currency_from")
    currency_to = models.ForeignKey(Currency, verbose_name=_("Currency to"), related_name="currency_to")
    rate = models.DecimalField(verbose_name=_("Rate"), max_digits=21, decimal_places=9, help_text=_("Rate"))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))
    date_update = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Exchange rate")
        verbose_name_plural = _("Exchange rates")

    def __unicode__(self):
        return u"%s" % self.rate


class RealPurseHistory(models.Model):
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"), blank=True, null=True)
    purse_from = models.CharField(verbose_name=_("Purse from"), max_length=50, blank=True, null=True)
    purse_to = models.CharField(verbose_name=_("Purse to"), max_length=50, blank=True, null=True)
    amount = models.DecimalField(verbose_name=_("Amount"), max_digits=11, decimal_places=2, blank=True, null=True)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"),  blank=True, null=True)
    payment_kind = models.CharField(verbose_name=_("Kind payment"), choices=PAYMENT_KIND_CHOICE, max_length=1)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Real purse history")
        verbose_name_plural = _("Real purse history")

    def __unicode__(self):
        return u"%s" % self.amount



class ConversionFund(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    exchange_rate = models.ForeignKey(ExchangeRate, verbose_name=_("Exchange rate"))
    value_exchange_rate = models.DecimalField(verbose_name=_("Rate"), max_digits=21, decimal_places=9, help_text=_("Rate"))
    credit_amount = models.DecimalField(verbose_name=_("Amount"), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    credit_commission = models.DecimalField(verbose_name=_("Commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    credit_amount_with_commission = models.DecimalField(verbose_name=_("Amount with commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    credit_currency = models.ForeignKey(Currency, related_name="debit_currency", verbose_name=_("Currency"))

    debit_amount = models.DecimalField(verbose_name=_("Amount"), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    debit_currency = models.ForeignKey(Currency, related_name="credit_currency", verbose_name=_("Currency"))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Conversion fund")
        verbose_name_plural = _("Conversion funds")
        ordering = ("-date_create",)

    def __unicode__(self):
        return u"%s-%s" % (self.credit_amount, self.exchange_rate)

    def get_credit_amount(self):
        return u"%s %s" % (self.credit_amount, self.credit_currency.iso_code)

    def get_credit_commission(self):
        return u"%s %s" % (self.credit_commission, self.credit_currency.iso_code)

    def get_credit_amount_with_commission(self):
        return u"%s %s" % (self.credit_amount_with_commission, self.credit_currency.iso_code)

    def get_debit_amount(self):
        return u"%s %s" % (self.debit_amount, self.debit_currency.iso_code)

    def get_exchange_rate(self):
        return u"%s" % self.exchange_rate.iso_code


class StatisticConversionFund(ConversionFund):
    def __unicode__(self):
        return u"-----------"

    class Meta:
        proxy = True
        verbose_name = _("Statistic conversion fund")
        verbose_name_plural = _("Statistic conversion fund")


class ShopPayment(models.Model):
    recipient = models.ForeignKey(User, related_name="recipient_payment", verbose_name=_("Recipient"))
    sender = models.ForeignKey(User, related_name="sender_payment", verbose_name=_("User"), blank=None, null=True)
    amount = models.DecimalField(verbose_name=_("Amount"), validators=[MinValueValidator(Decimal("0.01"))],  max_digits=11, decimal_places=2)
    commission = models.DecimalField(verbose_name=_("Commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    amount_with_commission = models.DecimalField(verbose_name=_("Amount with commission"), default=Decimal(0),  max_digits=11, decimal_places=2)
    currency = models.ForeignKey(Currency, verbose_name=_("Currency"))
    shop = models.ForeignKey(Shop, verbose_name=_("Shop"), blank=True, null=True)
    shop_order = models.IntegerField(verbose_name=_("Shop order"))
    payment_signature = models.CharField(verbose_name=_("Payment signature"),  max_length="100") #TODO this field must be unique=True
    payment_system = models.ForeignKey(PaymentSystem, verbose_name=_("Payment system"))
    phone = PhoneNumberField(verbose_name=_('Phone'), blank=True, null=True)
    status = models.CharField(verbose_name=_("Shop payment status"), choices=SHOP_PAYMENT_STATUS_CHOICE, default="t", max_length=2)
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_("Date create"))

    @property
    def get_status(self):
        return u"%s" % dict(SHOP_PAYMENT_STATUS_CHOICE).get(self.status, "")

    def get_sender(self):
        return u"%s" % self.sender.username

    def get_recipient(self):
        return u"%s" % self.recipient.username

    @property
    def get_recipient_name(self):
        return u"%s" % self.recipient.username

    @property
    def get_sender_name(self):
        return u"%s" % self.sender.username

    def get_amount(self):
        return u"%s %s" % (self.amount, self.currency.iso_code)

    def get_commission(self):
        return u"%s %s" % (self.commission, self.currency.iso_code)

    def get_amount_with_commission(self):
        return u"%s %s" % (self.amount_with_commission, self.currency.iso_code)

    class Meta:
        app_label = "financial_system"
        verbose_name = _("Shop payment")
        verbose_name_plural = _("Shop payment")
        ordering = ("-date_create",)

    get_sender.short_description = _("Sender")
    get_recipient.short_description = _("Recipient")
    get_amount.short_description = _("Amount")
    get_commission.short_description = _("Commission")
    get_amount_with_commission.short_description = _("Amount with commission")
    get_amount_with_commission.short_description = _("Amount with commission")