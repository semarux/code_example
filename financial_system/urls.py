# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

# from financial_system.api import api
from financial_system.views import site, member, test, shop, balance_informer
from financial_system.views.account import adding_funds, conversion_funds, financial_statement, regular_payment, withdrawal_funds, payment_system, transfer_funds, invoice
from main.views import member as main_member

from api import shop as api_shop
from api import user as api_user


urlpatterns = patterns("",
                       url(r"^$", site.SiteIndex.as_view()),
                       url(r"^profile/$", member.ProfileDetail.as_view(), name="profile"),
                       url(r"^settings/$", site.Settings.as_view(), name="settings"),
                       url(r"^generate_api_token/$", member.TokenView.as_view(), name="generate_api_token"),
                       # url(r"^get_balance_informer/$", member.BalanceInformerView.as_view(), name="get_balance_informer"),
                       url(r"^balance_informer/soc_auth/$", balance_informer.SocAuthView.as_view(), name="balance_informer_soc_auth"),
                       url(r"^balance_informer/soc_auth/(?P<shop_id>\d+)/$", balance_informer.SocAuthView.as_view(), name="balance_informer_soc_auth"),
                       url(r"^balance_informer/login/(?P<shop_id>\d+)/$", balance_informer.Login.as_view(), name="balance_informer_login"),
                       url(r"^balance_informer/logout/(?P<shop_id>\d+)/$", balance_informer.Logout.as_view(), name="balance_informer_logout"),
                       url(r"^balance_informer/(?P<shop_id>\d+)/$", balance_informer.BalanceInformerView.as_view(), name="balance_informer"),


                       url(r"^change_currency/$", site.ChangeCurrency.as_view(), name="change_currency"),
                       url(r"^inform_block/(?P<pk>\d+)/$", site.InformationBlockDetail.as_view(), name="inform_block_detail"),
                       url(r"^registration/$", member.Registration.as_view(), name="registration"),
                       url(r"^registration/complete/$", TemplateView.as_view(template_name="member/registration/registration_complete.html") , name="registration_complete"),
                       url(r"^login/$", member.Login.as_view(), name="login"),
                       url(r"^logout/$", "django.contrib.auth.views.logout", {"next_page": "/"}, name="logout"),
                       url(r"^logout/(?P<next_page>.*)/$", "django.contrib.auth.views.logout", name="logout_next"),
                       url(r"^account/$", financial_statement.FinancialStatement.as_view(), name="account"),
                       url(r"^account_settings/$", member.ProfileSettings.as_view(), name="account_settings"),
                       url(r"^financial_statement/$", financial_statement.FinancialStatement.as_view(), name="financial_statement"),

                       url(r"^regular_payment/list/$", regular_payment.RegularPaymentList.as_view(), name="regular_payment_list"),
                       url(r"^regular_payment/orders/(?P<filter>\w+)/$", regular_payment.RegularPaymentOrderList.as_view(), name="regular_payment_order_list"),
                       url(r"^regular_payment/create/$", regular_payment.RegularPaymentCreate.as_view(), name="regular_payment_create"),
                       url(r"^regular_payment/update/(?P<pk>\d+)/$", regular_payment.RegularPaymentUpdate.as_view(), name="regular_payment_update"),
                       url(r"^regular_payment/order/view/(?P<pk>\d+)/$", regular_payment.RegularPaymentOrderView.as_view(), name="regular_payment_order_view"),

                       url(r"^adding_funds/create/$", adding_funds.AddingFundCreate.as_view(), name="adding_funds_create"),
                       url(r"^adding_funds/create/(?P<payment_sys_code>[^/]+)/$", adding_funds.AddingFundCreate.as_view(), name="adding_funds_create_by_ps"),
                       url(r"^adding_funds/list/$", adding_funds.AddingFundsList.as_view(), name="adding_funds_list"),
                       url(r"^adding_funds/view/(?P<pk>\d+)/$", adding_funds.AddingFundsView.as_view(), name="adding_funds_view"),
                       url(r"^adding_funds/bank_payment_blank_view/(?P<pk>\d+)/$", adding_funds.BankPaymentBlankView.as_view(), name="bank_payment_blank_view"),
                       # url(r"^adding_funds/$", adding_funds.AddingFunds.as_view(), name="adding_funds_create"),
                       # url(r"^adding_funds/(?P<payment_sys_code>[^/]+)/$", adding_funds.AddingFunds.as_view(), name="adding_funds_by_ps"),


                       url(r"^withdrawal_funds/create/$", withdrawal_funds.WithdrawalFundsCreate.as_view(), name="withdrawal_funds_create"),
                       url(r"^withdrawal_funds/create/(?P<payment_system_id>\d+)/$", withdrawal_funds.WithdrawalFundsCreate.as_view(), name="withdrawal_funds_create_by_ps"),
                       url(r"^withdrawal_funds/list/$", withdrawal_funds.WithdrawalFundsList.as_view(), name="withdrawal_funds_list"),
                       url(r"^withdrawal_funds/view/(?P<pk>\d+)/$", withdrawal_funds.WithdrawalFundsView.as_view(), name="withdrawal_funds_view"),


                       url(r"^transfer_funds/create/$", transfer_funds.TransferFundsCreate.as_view(), name="transfer_funds_create"),
                       url(r"^transfer_funds/create/(?P<recipient_id>\d+)/$", transfer_funds.TransferFundsCreate.as_view(), name="transfer_funds_create_by_recipient"),
                       url(r"^transfer_funds/list/(?P<filter>\w+)/$", transfer_funds.TransferFundsList.as_view(), name="transfer_funds_list"),



                       url(r"^transfer_funds/view/(?P<pk>\d+)/$", transfer_funds.TransferFundsView.as_view(), name="transfer_funds_view"),


                       url(r"^invoice/create/$", invoice.InvoiceCreate.as_view(), name="invoice_create"),
                       # url(r"^invoice/create/by_order/(?P<pk>\d+)/$", invoice.InvoiceCreateByOrder.as_view(), name="invoice_create_by_order"),
                       url(r"^invoice/list/(?P<filter>\w+)/$", invoice.InvoiceList.as_view(), name="invoice_list"),
                       url(r"^invoice/list/$", invoice.InvoiceList.as_view(), name="invoice_list"),
                       url(r"^invoice/view/(?P<pk>\d+)/$", invoice.InvoiceView.as_view(), name="invoice_view"),
                       url(r"^invoice/pay/(?P<pk>\d+)/$", invoice.InvoicePay.as_view(), name="invoice_pay"),
                       url(r"^invoice/complete_in_favor_owner/(?P<pk>\d+)/$", invoice.InvoiceCompleteInFavorOwner.as_view(), name="invoice_complete_in_favor_owner"),
                       url(r"^invoice/complete_in_favor_payer/(?P<pk>\d+)/$", invoice.InvoiceCompleteInFavorPayer.as_view(), name="invoice_complete_in_favor_payer"),
                       url(r"^invoice/discussion/send/(?P<pk>\d+)/$", invoice.InvoiceDiscussionView.as_view(), name="invoice_discussion_send"),
                       url(r"^invoice/discussion/(?P<pk>\d+)/$", invoice.InvoiceDiscussionView.as_view(), name="invoice_discussion"),

                       url(r"^conversion_funds/create/$", conversion_funds.ConversionFundsCreate.as_view(), name="conversion_funds_create"),
                       url(r"^conversion_funds/list/$", conversion_funds.ConversionFundsList.as_view(), name="conversion_funds_list"),
                       url(r"^conversion_funds/view/(?P<pk>\d+)/$", conversion_funds.ConversionFundsView.as_view(), name="conversion_funds_view"),

                       url(r"^webmoney_success/$", payment_system.WebmoneySuccess.as_view(), name="webmoney_success"),
                       url(r"^webmoney_result/$", payment_system.WebmoneyResult.as_view(), name="webmoney_result"),
                       url(r"^webmoney_fail/$", payment_system.WebmoneyFail.as_view(), name="webmoney_fail"),
                       url(r"^yandex_result/$", payment_system.YandexResult.as_view(), name="yandex_result"),

                       url(r"^qiwi_result/$", payment_system.QiwiResult.as_view(), name="qiwi_result"),
                       url(r"^qiwi_success/$", payment_system.QiwiSuccess.as_view(), name="qiwi_success"),
                       url(r"^qiwi_fail/$", payment_system.QiwiFail.as_view(), name="qiwi_fail"),

                       url(r"^liqpay_result/$", payment_system.LiqpayResult.as_view(), name="liqpay_result"),
                       url(r"^liqpay_server/$", payment_system.LiqpayServer.as_view(), name="liqpay_server"),

                       url(r"^shop/create/$", shop.ShopCreate.as_view(), name="shop_create"),
                       url(r"^shop/update/(?P<pk>\d+)/$", shop.ShopUpdate.as_view(), name="shop_update"),
                       url(r"^shop/view/(?P<pk>\d+)/$", shop.ShopView.as_view(), name="shop_view"),
                       url(r"^shop/list/$", shop.ShopList.as_view(), name="shop_list"),
                       url(r"^shop/get_perform_payment/$", shop.GetShopPerformPayment.as_view(), name="get_perform_payment"),
                       url(r"^shop/perform_payment/$", shop.ShopPerformPayment.as_view(), name="shop_perform_payment"),

                       url(r"^shop/payment_list/$", shop.ShopPaymentList.as_view(), name="shop_payment_list"),
                       url(r"^shop/payment/view/(?P<pk>\d+)/$", shop.ShopPaymentView.as_view(), name="shop_payment_view"),

                       # url(r"^api/user_balance_list/$", api.UserBalanceList.as_view() , name="api_user_balance_list"),
                       # url(r"^api/user_balance_detail/(?P<pk>\d+)/$", api.UserBalanceDetail.as_view(), name="api_user_balance_detail"),

                       url(r'^new-error-url/$', main_member.LoginSocialBackEndError,  name='new-error-url'),
                       url(r'^new-association-redirect-url/$', main_member.NewAssociation,  name='new-association-redirect-url'),

                       #url(r'^fast_login/$', main_member.FastLogin,  name='fast_login'),
                       url(r'^logout/$', main_member.Logout, name='logout'),
                       url(r'^social_users_new_user/$', main_member.SocialCreateProfile, name='social_users_new_user'),
                       url(r'^social_users_login/$', member.SocialUserLogin.as_view(), name='social_users_login'),
                       url(r'^new_associationredirect-url/$', main_member.SocialCreateProfile, name='social_users_redirect_url'),

                       url(r"^api/shop/list/$", api_shop.ShopList.as_view(), name="api_shop_list"),
                       url(r"^api/shop/detail/(?P<pk>\d+)/$", api_shop.ShopDetail.as_view(), name="api_shop_detail"),
                       url(r"^api/shop/check/(?P<pk>\d+)/(?P<password>[^/]+)/$", api_shop.ShopCheck.as_view(), name="api_check_shop"),

                       url(r"^api/user/list/$", api_user.UserList.as_view(), name="api_user_list"),
                       url(r"^api/user/detail/(?P<pk>\d+)/$", api_user.UserDetail.as_view(), name="api_user_detail"),
                       url(r"^api/user/balance/$", api_user.UserBalanceList.as_view(), name="api_user_balance"),
                       url(r"^api/user/get_auth_token/$", api_user.UserGetAuthToken.as_view(), name="api_user_get_auth_token"),
                       url(r"^api/user/get_auth_token_by_auth_login/$", api_user.UserGetAuthTokenByAuthLogin.as_view(), name="api_user_get_auth_token_by_auth_login"),
                       url(r"^api/login/google/$", api_user.UserLoginGoogle.as_view(), name="api_user_login_google"),

                       # url(r"^recalculate_amount/$", account.RecalculateAmount.as_view(), name="recalculate_amount"),
       #url(r"^change_currency/$", site.ChangeCurrency, name="change_currency"),
       #url(r"^change_region_filter/$", site.ChangeRegionFilter, name="change_region_filter"),
    )

from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
# urls for testings
from main.views.test import SetCurrency

urlpatterns += patterns("",
    url(r"^run_regular_payment/$", test.run_regular_payment,        name="run_regular_payment"),
    url(r"^set_exchange_rate/$", test.set_exchange_rate,        name="set_exchange_rate"),
    url(r"^test_complete_invoice/$", test.complete_invoice,        name="test_complete_invoice"),


    url(r"^wm_test_success/$", test.success,        name="wm_sample-success"),
    url(r"^wm_test_result/$", test.result,        name="wm_sample-result"),
    url(r"^wm_test_fail/$",    test.fail,           name="wm_sample-fail"),

    url(r"^wm_test_payment/$", test.simple_payment, name="wm_sample-payment"),
    url(r"^test_save_post/$", test.test_save_post, name="test_save_post"),
    url(r"^test/set_currency/$", SetCurrency, name="test_set_currency"),
    url(r"^yandex_test_payment/$", test.yandex_payment, name="yandex_test_payment"),
    url(r"^liqpay_test_payment/$", test.liqpay_payment, name="liqpay_test_payment"),

)