# -*- coding: utf-8 -*-
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import View
from django.shortcuts import render
from django import http
from django.utils.http import is_safe_url

from financial_system.models import InformationBlock
from main.forms.main_form import ChangeCurrencyForm


class SiteIndex(ListView):
    model = InformationBlock
    template_name = "site/index.html"
    template_name_ajax = "site/_index.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self):
        return self.model.objects.filter(active=1)

    def get_context_data(self, **kwargs):
        context = super(SiteIndex, self).get_context_data(**kwargs)
        context['slides'] = self.get_queryset().filter(type="MS")
        context['information_blocks'] = self.get_queryset().filter(type="MIB")
        return context


class InformationBlockDetail(DetailView):
    model = InformationBlock
    template_name = "site/information_block_detail.html"

    def get_context_data(self, **kwargs):
        context = super(InformationBlockDetail, self).get_context_data(**kwargs)
        return context


class Settings(View):
    template_name = "settings.html"

    def get(self, request, **kwargs):
        change_currency_form = ChangeCurrencyForm(initial={'currency': request.session.get("selected_currency")})
        return render(request, self.template_name, {'change_currency_form': change_currency_form, 'language_code': request.LANGUAGE_CODE})


class ChangeCurrency(View):
    def get(self, request, **kwargs):
        return http.HttpResponseRedirect('/')

    def post(self, request, **kwargs):
        form = ChangeCurrencyForm(request.POST)
        if form.is_valid():
            request.session['selected_currency'] = form.cleaned_data.get('currency').id
        next = request.REQUEST.get('next')
        if not is_safe_url(url=next, host=request.get_host()):
            next = request.META.get('HTTP_REFERER')
            if not is_safe_url(url=next, host=request.get_host()):
                next = '/'
        response = http.HttpResponseRedirect(next)
        return response

