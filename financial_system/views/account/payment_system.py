# -*- coding: utf-8 -*-
import hashlib
import urllib
import urllib2
import json
import base64
from django.core.context_processors import request
from django.http import HttpResponseRedirect
from financial_system.helpers.system_functs import get_current_site_id

from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib import messages
from django.utils.translation import ugettext as _
from datetime import datetime
from main.helpers.main import profile_required
from financial_system.forms.account import *
from financial_system.forms.account.adding_funds import *
from financial_system.forms.account.payment_system import *
from financial_system.forms.account import DateFilterForm
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.helpers.amount import *
from financial_system.models import *
from main.models import Currency
from torgplace import settings
from torgplace.settings import YANDEX_URI, MAIN_PAYMENT_SYSTEM
import pycurl
import StringIO
import json as simplejson
from financial_system.helpers.datetampering import DateTampering
import webbrowser
from financial_system.helpers.system_functs import save_data_to_file


class WebmoneyResult(View):
    template_name = "account/adding_funds/create/webmoney/_success.html"
    purse = None

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WebmoneyResult, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request, **kwargs):
        order = None
        try:
            order = get_obj_or_none(AddingFund, id=request.POST.get("order_id", 0))
            if order:
                order.status = "wait_secure"
                order.transaction = request.POST.get("LMI_SYS_TRANS_NO", 0)
                order.purse_from = request.POST.get("LMI_PAYER_PURSE", 0)
                order.save()
            try:
                self.purse = Purse.objects.get(username=order.purse_to)
            except:
                return HttpResponse("ERR: THIS PURSE NOT REGISTERED IN SYSTEM OR NOT ACTIVATE")
        except:
            return HttpResponse("ERR: ORDER PAYMENT ORDER DOES NOT EXIST")
        else:
            save_data_to_file("wm_response.txt", {"ps_am": order.ps_amount, "payment_amount": request.POST.get("LMI_PAYMENT_AMOUNT",  0)})

            if float(order.ps_amount) != float(request.POST.get("LMI_PAYMENT_AMOUNT", 0)):
                return HttpResponse("ERR: AMOUNT OF PAYMENT WRONG")

        if int(request.POST.get("LMI_PREREQUEST", 0)) == 1:

            if request.POST.get("LMI_PAYEE_PURSE", "").strip() != order.purse_to:
                return HttpResponse("ERR: INVALID RECIPIENT' PURSE" + request.POST.get("LMI_PAYEE_PURSE", ""))
            return HttpResponse("YES")
        else:
            common_string = request.POST.get("LMI_PAYEE_PURSE", "") + request.POST.get("LMI_PAYMENT_AMOUNT", "") + request.POST.get("LMI_PAYMENT_NO", "") +\
                request.POST.get("LMI_MODE", "") + request.POST.get("LMI_SYS_INVS_NO", "") + request.POST.get("LMI_SYS_TRANS_NO", "") + \
                request.POST.get("LMI_SYS_TRANS_DATE", "") + self.purse.password + request.POST.get("LMI_PAYER_PURSE", "") + request.POST.get("LMI_PAYER_WM", "")
            m = hashlib.md5()
            m.update(common_string)
            hash = m.hexdigest().upper()


            if hash != request.POST.get("LMI_HASH", ""):
                return HttpResponse("ERR: COUNTERSIGNATURE NOT COINCIDE")

            if order.paid == False:
                order.paid = 1
                order.purse_from = request.POST.get("LMI_PAYER_PURSE", 0)
                order.purse_to = self.purse.username
                order.save()
            else:
                return HttpResponse("ERR: BILL WAS PAID EARLIER")
            return HttpResponse("YES")


class WebmoneyFail(FormView):
    template_name = "account/adding_funds/create/webmoney/_fail.html"
    form_class = AddingFundsCreateForm
    # @method_decorator(login_required())
    # @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WebmoneyFail, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(WebmoneyFail, self).get(request, *args, **kwargs)

    def post(self, request, **kwargs):
        order = get_obj_or_none(AddingFund, id=request.POST.get("order_id", 0))
        if order:
            order.status = "failure"
            order.paid = 0
            order.save()
        if self.request.session.has_key("shop_payment_form_data"):
            request.session["shop_payment_form_data"].update({"ps_payment_status": "fail"})
            return HttpResponseRedirect("/shop/perform_payment/")

        form = AddingFundsCreateForm(initial={"amount": request.POST.get("amount", 0), "currency": get_obj_or_none(Currency, pk=request.POST.get("currency", 0)), "ps_currency": request.POST.get("wm_type", "")})
        return render(request, self.template_name, {"form": form,
                                                    "menu_page": "adding_funds",
                                                    "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
            })

    def get_context_data(self, **kwargs):
        context = super(WebmoneyFail, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
            "payment_system": PaymentSystem.objects.get(sys_code="WM")

        })
        return context


class WebmoneySuccess(FormView):
    template_name = "account/adding_funds/create/webmoney/_success.html"
    form_class = AddingFundsCreateForm

    # @method_decorator(login_required())
    # @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WebmoneySuccess, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super(WebmoneySuccess, self).get(request, *args, **kwargs)

    def post(self, request, **kwargs):
        order = get_obj_or_none(AddingFund, id=request.POST.get("order_id", 0))
        if self.request.session.has_key("shop_payment_form_data"):
            request.session["shop_payment_form_data"].update({"ps_payment_status": "success"})
            return HttpResponseRedirect("/shop/perform_payment/")

        return render(request, self.template_name, {
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
            "payment_system": PaymentSystem.objects.get(sys_code="WM"),
            "id": request.POST.get("LMI_PAYMENT_NO", ""),
        })

    def get_context_data(self, **kwargs):
        context = super(WebmoneySuccess, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "payment_systems": PaymentSystem.objects.filter(active=1,for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
            "payment_system": PaymentSystem.objects.get(sys_code="WM")
        })
        return context


class YandexResult(FormView):
    template_name = "account/adding_funds/create/yandex/_result.html"
    form_class = AddingFundsCreateForm
    ps_sys_code = "YM"
    success_url = "/adding_funds/create/YM/"

    payment_system = get_obj_or_none(PaymentSystem, sys_code=ps_sys_code)

    # @method_decorator(login_required())
    # @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        if get_current_site_id() == 1:
            self.success_url = "/%s%s" % ("account", self.success_url)
        return super(YandexResult, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if request.GET.get("code", None):
            return HttpResponseRedirect(self.success_url + "?yandex_auth_code=%s" % request.GET.get("code"))
        return render(self.request, self.template_name, context)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        order = None
        purse = None
        try:
            order = get_obj_or_none(AddingFund, id=request.POST.get("order_id", 0))
            purse = Purse.objects.get(type="YAR", is_work=True, active=True)
            if order:
                order.status = "wait_secure"
                order.transaction = request.POST.get("LMI_SYS_TRANS_NO", 0)
                order.purse_from = request.POST.get("LMI_PAYER_PURSE", 0)
                order.save()
        except:
            messages.error(request, _("THE PURSE OR ORDER NOT REGISTERED IN SYSTEM OR NOT ACTIVATE"))
            return HttpResponseRedirect(self.success_url)
        # else:
        #     if float(order.ps_amount) != float(request.POST.get("LMI_PAYMENT_AMOUNT", 0)):
        #         return HttpResponse("ERR: AMOUNT OF PAYMENT WRONG")
        #
        #     if float(billing_information.get("payment_amount", 1)) != float(request.POST.get("payment_amount", 0)):
        #         messages.error(request, _("AMOUNT OF PAYMENT WRONG"))
        #         return HttpResponseRedirect("/adding_funds/YM/")



        form = AddingFundsCreateForm(ps_sys_code=self.ps_sys_code, initial={"amount": order.amount, "currency": order.currency, "ps_currency_type": purse.type})
        data_get_token = {"client_id": purse.password, "client_secret": purse.signature, "grant_type": "authorization_code",
                "redirect_uri": purse.result_url, "code": request.POST.get("code")}

        get_token = json.load(urllib.urlopen(YANDEX_URI.get("token", ""), urllib.urlencode(data_get_token)))
        access_token = get_token.get("access_token", "")
        save_data_to_file("yandex_access_token.txt", {"access_token": access_token})
        if access_token == "":
            order.status = "failure"
            order.save()
            messages.error(request, "get_token: %s" % get_token.get("error"))
            return HttpResponseRedirect(self.success_url)
        else:
            data_request_payment = {
                "pattern_id": "p2p",
                "to": purse.username,
                "amount_due": order.ps_amount,
                "comment": "Transfer system verifair",
                "message": "Transfer system verifair",
            }
            request_payment = json.load(urllib2.urlopen(urllib2.Request(YANDEX_URI.get("request_payment", ""), urllib.urlencode(data_request_payment), {"Authorization": "Bearer %s" % access_token, "Content-Type": "application/x-www-form-urlencoded"})))
            order.transaction = request_payment.get("request_id", "")
            order.save()
            save_data_to_file("yandex_request.txt", request_payment)
            if request_payment.get("error", None):
                order.status = "failure"
                order.save()
                messages.error(request, "request_payment: %s" % request_payment.get("error"))
                return HttpResponseRedirect(self.success_url)
            else:
                data_process_payment = {
                    "request_id": request_payment.get("request_id", ""),
                }
                process_payment = json.load(urllib2.urlopen(urllib2.Request(YANDEX_URI.get("process_payment", ""), urllib.urlencode(data_process_payment), {"Authorization": "Bearer %s" % access_token, "Content-Type": "application/x-www-form-urlencoded"})))
                order.purse_from = process_payment.get("payer", "")
                order.save()
                save_data_to_file("yandex_process.txt", process_payment)
                if process_payment.get("error", None):
                    order.status = "failure"
                    order.save()
                    messages.error(request, "process_payment: %s" % process_payment.get("error_description"))
                    return HttpResponseRedirect(self.success_url)
                else:
                    context.update({"id": process_payment.get("payee_uid", "")})
                    if order.paid == False:
                        order.paid = 1
                        order.purse_from = process_payment.get("payer", 0)
                        order.purse_to = purse.username
                        order.save()
                        context.update({"success": True})
                    else:
                        messages.error(request, _("Bill was paid earlier"))
                        return HttpResponseRedirect(self.success_url)

        context.update({"form": form})
        return render(self.request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super(YandexResult, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "form_url": YANDEX_URI.get("auth", ""),
            "form": AddingFundsCreateForm(ps_sys_code=self.ps_sys_code),
            "payment_system": self.payment_system,
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
        })
        return context


class LiqpayServer(FormView):
    template_name = "account/adding_funds/create/liqpay/_server.html"
    form_class = AddingFundsCreateForm
    ps_sys_code = "LPM"
    success_url = "/adding_funds/create/LPM/"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        if get_current_site_id() == 1:
            self.success_url = "/%s%s" % ("account", self.success_url)
        return super(LiqpayServer, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        order = None
        purse = None
        # save_data_to_file("LIQPAY_post%s.txt" % request.POST.get("transaction_id", ""), request.POST)
        try:
            order = get_obj_or_none(AddingFund, id=request.POST.get("order_id", 0))
            purse = Purse.objects.get(type="LPM", is_work=True, active=True)
            if order:
                order.status = request.POST.get("status", "wait_secure")
                order.transaction = request.POST.get("transaction_id", 0)
                order.purse_from = request.POST.get("sender_phone", 0)
                order.save()
        except:
            messages.error(request, _("THE PURSE OR ORDER NOT REGISTERED IN SYSTEM OR NOT ACTIVATE"))
            return HttpResponseRedirect(self.success_url)

        form = AddingFundsCreateForm(ps_sys_code=self.ps_sys_code, initial={"amount": order.amount, "currency": order.currency, "ps_currency_type": purse.type})

        context.update({"id": request.POST.get("transaction_id", "")})
        if request.POST.get("status") == "failure":
            order.status = "failure"
            order.save()
            messages.error(request, _("In making the payment is denied"))
        elif request.POST.get("status") == "waited":
            messages.error(request, _("Payment pending verification"))
        elif request.POST.get("status") == "success":
            if order.paid == False:
                order.paid = 1
                order.purse_from = request.POST.get("sender_phone", "")
                order.purse_to = purse.username
                order.save()
                context.update({"success": True})
            else:
                messages.error(request, _("Bill was paid earlier"))
                return HttpResponseRedirect(self.success_url)
        context.update({"form": form, "id": request.POST.get("id", 0)})
        return render(self.request, self.template_name, context)


class LiqpayResult(FormView):
    template_name = "account/adding_funds/create/liqpay/_result.html"
    form_class = AddingFundsCreateForm
    ps_sys_code = "LPM"
    payment_system = get_obj_or_none(PaymentSystem, sys_code=ps_sys_code)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LiqpayResult, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        # save_data_to_file("LIQPAY_RESULT_%s.txt" % request.POST.get("transaction_id", ""), request.POST)
        return render(self.request, self.template_name, context)


    def get_context_data(self, **kwargs):
        context = super(LiqpayResult, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "form_url": YANDEX_URI.get("auth", ""),
            "form": AddingFundsCreateForm(ps_sys_code=self.ps_sys_code),
            "payment_system": self.payment_system,
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
        })
        return context


class QiwiResult(FormView):
    template_name = "account/adding_funds/create/view.html"
    template_name_ajax = "account/adding_funds/create/_form_as_ps_form.html"

    form_class = AddingFundsQiwiForm
    ps_sys_code = "QM"
    success_url = "/adding_funds/create/QM/"

    payment_system = get_obj_or_none(PaymentSystem, sys_code=ps_sys_code)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    # @method_decorator(login_required())
    # @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        if get_current_site_id() == 1:
            self.success_url = "/%s%s" % ("account", self.success_url)
        return super(QiwiResult, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(self.request, self.get_template_names(), context)

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        try:
            order = get_obj_or_none(AddingFund, id=form.cleaned_data.get("order_id"))
            purse = Purse.objects.get(type=order.payment_system.sys_code, is_work=True, active=True)
        except:
            messages.error(self.request, _("THIS PURSE OR ORDER NOT REGISTERED IN SYSTEM OR NOT FOUND"))
            return HttpResponseRedirect(self.success_url)

        data_creating_invoice = {
            "user": "tel:%s" % form.cleaned_data.get("user").raw_input,
            "amount": form.cleaned_data.get("amount"),
            "ccy": form.cleaned_data.get("ccy", "RUB"),
            "comment": form.cleaned_data.get("comment"),
            "lifetime": DateTampering(form.cleaned_data.get("lifetime")).next_mounth().strftime("%Y-%m-%dT%H:%M:%S"),
            "pay_source": form.cleaned_data.get("pay_source"),
            "prv_name": form.cleaned_data.get("prv_name"),
        }

        c = pycurl.Curl()
        c.setopt(c.URL, "https://w.qiwi.com/api/v2/prv/%s/bills/%s" % (purse.username, order.id))
        c.setopt(c.SSL_VERIFYPEER, False)
        c.setopt(c.CUSTOMREQUEST, "PUT")
        c.setopt(c.POSTFIELDS, urllib.urlencode(data_creating_invoice))
        c.setopt(c.POST, 1)
        c.setopt(c.USERPWD, "%s:%s" % (purse.username, purse.password))
        c.setopt(c.HTTPHEADER, ["Accept: application/json"])
        b = StringIO.StringIO()
        c.setopt(pycurl.WRITEFUNCTION, b.write)
        c.perform()
        c.close()
        result = simplejson.loads(b.getvalue())

        if result.get("response", {}).get("result_code") != 0:
            messages.error(self.request, result.get("response", {}).get("description"), _("An unexpected error"))
        else:
            # messages.info(self.request, _("Invoice for payment has been created"))
            # save_data_to_file("qiwy_result.txt", result)
            redirect_url = "https://w.qiwi.com/order/external/main.action?shop=%(shop)s&transaction=%(order_id)s&success_url=%(success_url)s&fail_url=%(fail_url)s&qiwi_phone=%(phone)s" % ({
                "shop": purse.username,
                "order_id": order.id,
                "success_url": purse.success_url,
                "fail_url": purse.fail_url,
                "phone": form.cleaned_data.get("user").raw_input.replace('+', ''),
            })
            # webbrowser.open(redirect_url)
            context.update({"redirect_url": redirect_url})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(QiwiResult, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "form_url": "",
            "payment_system": self.payment_system,
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
        })
        return context


class QiwiSuccess(FormView):
    template_name = "account/adding_funds/create/qiwi/_success.html"
    form_class = AddingFundsCreateForm
    ps_sys_code = "QM"
    success_url = "/adding_funds/create/QM/"

    payment_system = get_obj_or_none(PaymentSystem, sys_code=ps_sys_code)


    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(QiwiSuccess, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        try:
            order = get_obj_or_none(AddingFund, id=request.GET.get("order", 0))
            purse = Purse.objects.get(type=order.payment_system.sys_code, is_work=True, active=True)
        except:
            messages.error(self.request, _("THE PURSE OR ORDER NOT REGISTERED IN SYSTEM OR NOT FOUND"))
            return HttpResponseRedirect(self.success_url)

        if order:
            c = pycurl.Curl()
            c.setopt(c.URL, "https://w.qiwi.com/api/v2/prv/%s/bills/%s" % (purse.username, order.id))
            c.setopt(c.SSL_VERIFYPEER, False)
            c.setopt(c.CUSTOMREQUEST, "GET")
            c.setopt(c.POSTFIELDS, urllib.urlencode({}))
            c.setopt(c.POST, 1)
            c.setopt(c.USERPWD, "%s:%s" % (purse.username, purse.password))
            c.setopt(c.HTTPHEADER, ["Accept: application/json"])
            b = StringIO.StringIO()
            c.setopt(pycurl.WRITEFUNCTION, b.write)
            c.perform()
            c.close()
            result = simplejson.loads(b.getvalue())
            if result.get("response", {}).get("result_code") != 0:
                messages.error(self.request, result.get("response", {}).get("description"), _("An unexpected error"))
            else:
                if result.get("response", {}).get("bill", {}):
                    bill = result.get("response", {}).get("bill", {})
                    context.update({"bill": bill, "order": order})
                    order.status = bill.get("status")
                    order.transaction = bill.get("bill_id")
                    order.purse_from = bill.get("user")
                    order.save()
                    if bill.get("status") == "paid" or bill.get("status") == "waiting":
                        if order.paid == False:
                            order.paid = 1
                            order.purse_from = bill.get("user")
                            order.purse_to = purse.username
                            order.save()
                            context.update({"success": True})
                            if self.request.session.has_key("shop_payment_form_data"):
                                request.session["shop_payment_form_data"].update({"ps_payment_status": "success"})
                                return HttpResponseRedirect("/shop/perform_payment/")
                        else:
                            messages.error(request, _("This bill has been paid previously"))
                            return HttpResponseRedirect(self.success_url)
                    elif bill.get("status") == "waiting":
                        pass
                    else:
                        messages.error(request, _("This bill hasn't been paid"))
                        return HttpResponseRedirect(self.success_url)
                else:
                    messages.error(request, _("Bill was not received"))
                    return HttpResponseRedirect(self.success_url)
        else:
            messages.error(request, _("This order or purse for add funds to system not found"))
        return render(self.request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super(QiwiSuccess, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "form_url": "",
            "form": AddingFundsCreateForm(ps_sys_code=self.ps_sys_code),
            "payment_system": self.payment_system,
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
        })
        return context


class QiwiFail(FormView):
    template_name = "account/adding_funds/create/qiwi/_fail.html"
    form_class = AddingFundsCreateForm
    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(QiwiFail, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if self.request.session.has_key("shop_payment_form_data"):
            request.session["shop_payment_form_data"].update({"ps_payment_status": "fail"})
            return HttpResponseRedirect("/shop/perform_payment/")
        return super(QiwiFail, self).get(request, *args, **kwargs)

    def post(self, request, **kwargs):
        if self.request.session.has_key("shop_payment_form_data"):
            request.session["shop_payment_form_data"].update({"ps_payment_status": "fail"})
            return HttpResponseRedirect("/shop/perform_payment/")
        context = self.get_context_data(**kwargs)
        form = AddingFundsCreateForm(initial={"amount": request.POST.get("amount", 0), "currency": get_obj_or_none(Currency, pk=request.POST.get("currency", 0)), "ps_currency": request.POST.get("wm_type", "")})
        context.update({"form": form})
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super(QiwiFail, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "menu_page": "adding_funds",
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
            "payment_system": PaymentSystem.objects.get(sys_code="QM")
        })
        return context