# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import transaction
from financial_system.helpers.system_functs import get_current_site_id

from financial_system.forms.account import *
from financial_system.forms.account.invoice import *
from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from main.tasks import send_inform_mail
from django.contrib import messages



class InvoiceCreate(FormView):
    form_class = InvoiceCreateForm
    template_name = "account/invoice/create/view.html"
    template_name_ajax = "account/invoice/create/_view.html"
    payer = None
    operation_mode = None
    success_url = "/invoice/list/my/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]


    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        self.operation_mode = get_operation_mode()
        return super(InvoiceCreate, self).dispatch(*args, **kwargs)


    def form_valid(self, form, **kwargs):
        try:
            escrow = self.request.user.profileaccountsettings.escrow
        except:
            escrow = None
        invoice = form.save(commit=False)
        invoice.owner = self.request.user
        if escrow:
            invoice.payment_coupon = "E"
        invoice.save()
        send_inform_mail.delay("account/invoice/notification/invoice_create_subject.txt",
                     "account/invoice/notification/invoice_create.html",
                     invoice.payer.email,
                     options={
                         "invoice": invoice,
                     })

        messages.info(self.request, _("Invoice was created"))
        return HttpResponseRedirect(self.success_url)

    def get(self, request, *args, **kwargs):
        if self.kwargs.get("payer_id"):
            self.payer = get_object_or_404(User, pk=self.kwargs.get("payer_id", 0))
        return self.render_to_response(self.get_context_data(form=InvoiceCreateForm(user=request.user)))

    def post(self, request, *args, **kwargs):
        form = InvoiceCreateForm(user=request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(InvoiceCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class InvoiceList(FormView):
    form_class = DateFilterForm
    model = Invoice
    template_name = "account/invoice/list/view.html"
    template_name_ajax = "account/invoice/list/_view.html"

    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": ["payer.username", "payer.first_name", "payer.last_name"], #"payer.get_username",
            "order_name": "payer",
            "title": _("Payer"),
            # "action_update": "invoice_view",
        },
        {
            "name": ["amount", "currency"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": "short_description",
            "order_name": "short_description",
            "title": _("Short description"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create"),
        },
        {
            "name": "get_status",
            "order_name": "status",
            "title": _("Status"),
        },
        {
            "name": "paid",
            "order_name": "paid",
            "title": _("Paid"),
        },
        {
            "name": "completed",
            "order_name": "completed",
            "title": _("Completed"),
        },
    ]

    object_options = [
        {
            "name": "invoice_pay",
            "title": _("Pay"),
            "rule": "not_paid && not_completed",
        },
        {
            "name": "invoice_view",
            "title": _("View"),
        },

    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        return super(InvoiceList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        if self.kwargs.get("filter", "") == "for_me":
            qs = self.model.objects.select_related("owner", "owner__username", "owner__first_name", "owner__last_name",
                                                   "payer", "payer__username", "payer__first_name", "payer__last_name",
                                                   "currency", "payment_currency", "shop").filter(
                payer=self.request.user)

        else:
            qs = self.model.objects.select_related("owner", "owner__username", "owner__first_name", "owner__last_name",
                                                   "payer", "payer__username", "payer__first_name", "payer__last_name",
                                                   "currency", "payment_currency", "shop").filter(owner=self.request.user)

        if len(orders) != 0:
                qs = qs.order_by(*orders)
        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Invoices"), "invoices")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(InvoiceList, self).get_context_data(**kwargs)

        if self.kwargs.get("filter", "") == "my":
            obj_options = self.object_options[1:]
        else:
            obj_options = self.object_options
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
            "object_options": obj_options,
            "operation_mode":  get_operation_mode()
        })
        return context


class InvoiceCompleteInFavorOwner(DetailView):
    model = Invoice

    success_url = "/invoice/list/for_me/"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(InvoiceCompleteInFavorOwner, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        invoice = self.get_object()
        if self.request.user == invoice.payer:
            system_owner = get_obj_or_none(Profile, system_owner=1)
            system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
            if not invoice.completed:
                if system_owner_balance and system_owner_balance.state >= invoice.amount:
                    invoice_complete_in_favor_owner(invoice, system_owner, system_owner_balance)
                    messages.info(self.request, _("Invoice complete in favor owner."))
                else:
                    messages.info(self.request, _("Sorry but system owner haven't enough money in this currency. Please try the operation later."))
            else:
                messages.info(self.request, _("The invoice completed."))
        else:
            messages.info(self.request, _("Invoice don't complete in favor payer, because you isn't owner the invoice"))
        return HttpResponseRedirect(self.success_url)


class InvoiceCompleteInFavorPayer(DetailView):
    model = Invoice

    success_url = "/invoice/list/my/"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(InvoiceCompleteInFavorPayer, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        invoice = self.get_object()
        if self.request.user == invoice.owner:
            system_owner = get_obj_or_none(Profile, system_owner=1)
            system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
            if not invoice.completed:
                if system_owner_balance and system_owner_balance.state >= invoice.amount:
                    invoice_complete_in_favor_payer(invoice, system_owner, system_owner_balance)
                    messages.info(self.request, _("Invoice complete in favor payer."))
                else:
                    messages.info(self.request, _("Sorry but system owner haven't enough money in this currency. Please try the operation later."))
            else:
                messages.info(self.request, _("The invoice completed."))
        else:
            messages.info(self.request, _("Invoice don't complete in favor payer, because you isn't payer for this invoice"))
        return HttpResponseRedirect(self.success_url)


class InvoiceView(DetailView):
    model = Invoice
    template_name = "account/invoice/view/view.html"
    template_name_ajax = "account/invoice/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().owner != self.request.user and self.get_object().payer != self.request.user:
            messages.info(self.request, _("You can't view this invoice"))
            return HttpResponseRedirect(get_success_url("/invoice/list/my/"))
        return super(InvoiceView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(InvoiceView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


""" Invoce pay view  """
class InvoicePay(UpdateView):
    form_class = InvoicePayForm
    model = Invoice
    template_name = "account/invoice/pay/view.html"
    template_name_ajax = "account/invoice/pay/_view.html"
    operation_mode = None
    success_url = "/invoice/list/for_me/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        if self.get_object().payer != self.request.user:
            messages.info(self.request, _("You can't pay this invoice"))
            return HttpResponseRedirect(get_success_url("/invoice/list/my/"))
        self.operation_mode = get_operation_mode()
        return super(InvoicePay, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if self.operation_mode and self.operation_mode.code == "PSC":
            currency = get_obj_or_none(Currency, iso_code=settings.SYSTEM_CURRENCY)
            self.object.payment_currency = currency
        elif self.operation_mode and self.operation_mode.code == "PUC":
            self.object.payment_currency = self.object.payer.profile.currency

        context.update({"form": InvoicePayForm(instance=self.object)})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = InvoicePayForm(data=request.POST, instance=self.get_object())
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form, **kwargs):
        getcontext().prec = 5
        invoice = form.save(commit=False)
        if self.request.user != invoice.payer:
            messages.info(self.request, _("You can't pay this invoice"))
            return HttpResponseRedirect(self.success_url)
        else:
            if form.cleaned_data.get("spec_action") == "pay":
                invoice.payment_amount = form.billing_information["amount"]
                invoice.payment_commission = form.billing_information["commission"]
                invoice.payment_amount_with_commission = form.billing_information["payment_amount_real"]
                invoice.payment_currency = form.cleaned_data["payment_currency"]
                invoice.status = "paid"
                invoice.paid = 1
                invoice.date_pay = datetime.today()
                invoice.save()

                if invoice.content_object:
                    invoice.content_object.status = "paid"
                    invoice.content_object.save()

                payer_invoice_balance = form.payer_balance
                payer_invoice_balance.state -= invoice.payment_amount_with_commission
                payer_invoice_balance.save()

                payer_invoice_ph = PaymentHistory(user=invoice.payer, profile=invoice.payer.profile,
                                    content_object=invoice, action_type="foot_invoice",
                                    shop=None, amount=invoice.payment_amount,
                                    commission=invoice.payment_commission,
                                    amount_with_commission=invoice.payment_amount_with_commission,
                                    currency=invoice.payment_currency,
                                    payment_system=form.ps,
                                    payment_kind="E")
                payer_invoice_ph.save()

                recipient_profile = invoice.owner.profile
                if invoice.payment_coupon == "E":
                    recipient_profile = system_owner = get_obj_or_none(Profile, system_owner=1)
                    try:
                        owner_invoice_balance = UserBalance.objects.get(user=system_owner.user, currency=invoice.currency, payment_coupon="E")
                    except:
                        ub = UserBalance(user=system_owner.user, profile=system_owner, state=invoice.amount, currency=invoice.currency, payment_coupon="E")
                        ub.save()
                    else:
                        owner_invoice_balance.state += form.billing_information.get("amount")
                        owner_invoice_balance.save()
                else:
                    try:
                        owner_invoice_balance = UserBalance.objects.get(user=invoice.owner, currency=invoice.currency, payment_coupon=None)
                    except:
                        ub = UserBalance(user=invoice.owner, profile=invoice.owner.profile, state=invoice.amount, currency=invoice.currency, payment_coupon=None)
                        ub.save()
                    else:
                        owner_invoice_balance.state += form.billing_information.get("amount")
                        owner_invoice_balance.save()

                owner_invoice_ph = PaymentHistory(user=recipient_profile.user, profile=recipient_profile,
                        content_object=invoice, action_type="foot_invoice",
                        shop=None, amount=invoice.amount,
                        commission=0,
                        amount_with_commission=invoice.amount,
                        currency=invoice.currency,
                        payment_system=form.ps,
                        payment_kind="I")
                owner_invoice_ph.save()


                send_inform_mail.delay("account/invoice/notification/invoice_paid_subject.txt",
                         "account/invoice/notification/invoice_paid.html",
                         invoice.owner.email,
                         options={
                             "invoice": invoice,
                         })

                messages.info(self.request, _("Invoice was successfully paid"))
                return HttpResponseRedirect(self.success_url)
            else:
                context = self.get_context_data(**kwargs)
                context.update({
                    "form": form,
                    "payment_amount": form.billing_information["amount"],
                    "payment_commission": form.billing_information["commission"],
                    "payment_amount_with_commission": form.billing_information["payment_amount_real"],
                    "payment_currency": form.cleaned_data["payment_currency"],
                })
                return render(self.request, self.get_template_names(), context)


    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(InvoicePay, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context



class InvoiceDiscussionView(DetailView, FormView):
    form_class = InvoiceDiscussionSendForm
    model = Invoice
    template_name = "account/invoice/discussion/view.html"
    template_name_ajax = "account/invoice/discussion/_view.html"

    success_url = "/invoice/list/my/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = self.get_success_url(self.success_url)
        if not self.request.user in [self.get_object().payer, self.get_object().owner]:
            messages.info(self.request, _("This isn't your invoice."))
            return HttpResponseRedirect(self.success_url)
        return super(InvoiceDiscussionView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = InvoiceDiscussionSendForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(form=InvoiceDiscussionSendForm()))

    def form_valid(self, form, **kwargs):
        InvoiceDiscussion.objects.create(user=self.request.user, invoice=self.get_object(), comment=form.cleaned_data["comment"])
        return self.render_to_response(self.get_context_data(form=InvoiceDiscussionSendForm()))

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(InvoiceDiscussionView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object": self.object,
        })
        return context


#rename invoice