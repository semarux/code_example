# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import transaction

from financial_system.forms.account import DateFilterForm
from financial_system.forms.account.transfer_funds import *

from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_list_or_404, get_object_or_404
from django.shortcuts import render
from financial_system.helpers.system_functs import get_current_site_id
from django.views.generic.detail import DetailView


class TransferFundsCreate(FormView):
    form_class = TrunsferFundsCreateForm
    template_name = "account/transfer_funds/create/view.html"
    template_name_ajax = "account/transfer_funds/create/_view.html"
    recipient = None
    operation_mode = None
    success_url = "/transfer_funds/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        self.operation_mode = get_operation_mode()
        return super(TransferFundsCreate, self).dispatch(*args, **kwargs)



    def form_valid(self, form, **kwargs):
        getcontext().prec = 5
        if self.operation_mode.code == "PUC":
            currency_to_obtain = form.cleaned_data["recipient"].profile.currency
        else:
            currency_to_obtain = form.cleaned_data["currency"]

        if form.cleaned_data.get("spec_action") == "transfer":
            transfer = form.save(commit=False)
            transfer.sender = self.request.user
            transfer.commission = form.billing_information.get("commission_real")
            transfer.amount_with_commission = form.billing_information.get("amount_with_commission")
            transfer.save()
            messages.info(self.request, _("Funds will be transfer"))

            sender_balance = form.sender_balance
            sender_balance.state -= form.billing_information.get("amount_with_commission")
            sender_balance.save()

            sender_ph = PaymentHistory(user=transfer.sender, profile=transfer.sender.profile,
                                content_object=transfer, action_type="transfer_funds",
                                shop=None, amount=form.cleaned_data["amount"],
                                commission=form.billing_information.get("commission_real"),
                                amount_with_commission=form.billing_information.get("amount_with_commission"),
                                currency=form.cleaned_data["currency"],
                                payment_system=form.ps,
                                payment_kind="E")
            sender_ph.save()

            try:
                recipient_balance = UserBalance.objects.get(user=transfer.recipient, currency=form.cleaned_data["currency"], payment_coupon=None)
            except:
                ub = UserBalance(user=transfer.recipient, profile=transfer.recipient.profile, state=form.billing_information.get("amount"), currency=form.cleaned_data["currency"], payment_coupon=None)
                ub.save()
            else:
                recipient_balance.state += form.billing_information.get("amount")
                recipient_balance.save()

            recipient_ph = PaymentHistory(user=transfer.recipient, profile=transfer.recipient.profile,
                    content_object=transfer, action_type="transfer_funds",
                    shop=None, amount=form.billing_information.get("amount"),
                    commission=0,
                    amount_with_commission=form.billing_information.get("amount"),
                    currency=currency_to_obtain,
                    payment_system=form.ps,
                    payment_kind="I")
            recipient_ph.save()

            return HttpResponseRedirect(self.success_url)

        else:
            context = self.get_context_data(**kwargs)
            context.update({
                "form": form,
                "amount": form.cleaned_data["amount"],
                "commission": form.billing_information.get("commission_real"),
                "amount_with_commission": form.billing_information.get("amount_with_commission"),
                "currency": form.cleaned_data["currency"],
                "amount_to_obtain": form.billing_information["amount"],
                "currency_to_obtain": currency_to_obtain

            })
            return render(self.request, self.get_template_names(), context)

    def get(self, request, *args, **kwargs):
        if self.kwargs.get("recipient_id"):
            self.recipient = get_object_or_404(User, pk=self.kwargs.get("recipient_id", 0))
        return self.render_to_response(self.get_context_data(form=TrunsferFundsCreateForm(user=request.user, recipient=self.recipient)))

    def post(self, request, *args, **kwargs):
        form = TrunsferFundsCreateForm(user=request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(TransferFundsCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class TransferFundsList(FormView):
    form_class = DateFilterForm
    model = TransferFund
    template_name = "account/transfer_funds/list/view.html"
    template_name_ajax = "account/transfer_funds/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": ["sender.username", "sender.first_name", "sender.last_name"],
            "order_name": "sender",
            "title": _("Sender"),
        },
        {
            "name": ["recipient.username", "recipient.first_name", "recipient.last_name"],
            "order_name": "recipient",
            "title": _("Recipient"),
        },

        {
            "name": ["amount", "currency"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": ["amount_with_commission", "currency"],
            "order_name": "amount_with_commission",
            "title": _("Amount with commission"),
        },

        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create transfer"),
        },
    ]

    object_options = [
        {
            "name": "transfer_funds_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(TransferFundsList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("sender", "recipient", "currency")
        if self.kwargs.get("filter", "") == "for_me":
            qs = qs.filter(recipient=self.request.user).order_by(*orders)
        else:
            qs = qs.filter(sender=self.request.user).order_by(*orders)


        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Transfer funds"), "transfer_funds")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(TransferFundsList, self).get_context_data(**kwargs)
        object_columns = self.object_columns[:]
        if self.kwargs.get("filter", "") == "my":
            del object_columns[1]
        else:
            del object_columns[2]

        context.update({
            "main_menu_page": "account",
            "object_columns": object_columns,
            "object_options": self.object_options,
        })
        return context


class TransferFundsView(DetailView):
    model = TransferFund
    template_name = "account/transfer_funds/view/view.html"
    template_name_ajax = "account/transfer_funds/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().sender != self.request.user and self.get_object().recipient != self.request.user:
            messages.info(self.request, "Transfer funds document doesn't belong to you")
            return HttpResponseRedirect(get_success_url("/transfer_funds/list/my/"))
        return super(TransferFundsView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(TransferFundsView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context