# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.db import transaction
from django.contrib import messages
from torgplace.settings import MAIN_PAYMENT_SYSTEM
from financial_system.helpers.system_functs import get_current_site_id
from financial_system.forms.account import DateFilterForm
from financial_system.forms.account.conversion_funds import *
from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.views.generic.detail import DetailView


class ConversionFundsCreate(FormView):
    form_class = ConversionFundsForm
    template_name = "account/conversion_funds/create/view.html"
    template_name_ajax = "account/conversion_funds/create/_view.html"
    success_url = "/conversion_funds/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(ConversionFundsCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        getcontext().prec = 5
        if form.cleaned_data.get("spec_action") == "convert":
            conversion_funds = form.save(commit=False)
            conversion_funds.user = self.request.user
            conversion_funds.exchange_rate = form.exchange_rate
            conversion_funds.value_exchange_rate = form.exchange_rate.rate
            conversion_funds.credit_commission = form.billing_information.get("commission_real")
            conversion_funds.credit_amount_with_commission = form.billing_information.get("amount_with_commission")
            conversion_funds.debit_amount = form.billing_information.get("amount")
            conversion_funds.save()

            system_balance_for_debit = get_obj_or_none(UserBalance, user=form.system_owner.user, currency=form.cleaned_data.get("credit_currency"), payment_coupon=None)
            system_balance_for_credit = get_obj_or_none(UserBalance, user=form.system_owner.user, currency=form.cleaned_data.get("debit_currency"), payment_coupon=None)
            user_balance_for_debit = get_obj_or_none(UserBalance, user=self.request.user, currency=form.cleaned_data.get("debit_currency"), payment_coupon=None)
            user_balance_for_credit = get_obj_or_none(UserBalance, user=self.request.user, currency=form.cleaned_data.get("credit_currency"), payment_coupon=None)

            #change system balance
            """ add funds to the system owner, taking into account with commission """
            if system_balance_for_debit:
               system_balance_for_debit.state += form.billing_information.get("amount_with_commission")
               system_balance_for_debit.save()
            else:
                system_balance_for_debit = UserBalance(user=form.system_owner.user, profile=form.system_owner.user.profile,
                                                       state=form.cleaned_data.get("amount", 0),
                                                       currency=form.cleaned_data["credit_currency"])
                system_balance_for_debit.save()


            system_balance_for_debit_ph = PaymentHistory(user=form.system_owner.user, profile=form.system_owner.user.profile,
                                                         content_object=conversion_funds,
                                                         action_type="exchange_funds",
                                                         shop=None, amount=form.billing_information.get("amount_with_commission"),
                                                         commission=0,
                                                         amount_with_commission=form.billing_information.get("amount_with_commission"),
                                                         currency=form.cleaned_data.get("credit_currency"),
                                                         payment_system=form.ps,
                                                         payment_kind="I")
            system_balance_for_debit_ph.save()


            """ withdrawal funds to the system owner"""
            system_balance_for_credit.state -= form.billing_information.get("amount")
            system_balance_for_credit.save()

            system_balance_for_credit_ph = PaymentHistory(user=form.system_owner.user, profile=form.system_owner.user.profile,
                                                          content_object=conversion_funds, action_type="exchange_funds",
                                                          shop=None, amount=form.billing_information.get("amount"),
                                                          commission=0, amount_with_commission=form.billing_information.get("amount"),
                                                          currency=form.cleaned_data.get("debit_currency"),
                                                          payment_system=form.ps,
                                                          payment_kind="E")
            system_balance_for_credit_ph.save()


            """ add funds to user"""
            if user_balance_for_debit:
                user_balance_for_debit.state += form.billing_information.get("amount")
                user_balance_for_debit.save()
            else:
                user_balance_for_debit = UserBalance(user=self.request.user, profile=self.request.user.profile,
                                                     state=form.billing_information.get("amount"),
                                                     currency=form.cleaned_data.get("debit_currency"),
                                                     payment_coupon=None)
                user_balance_for_debit.save()

            user_balance_for_debit_ph = PaymentHistory(user=self.request.user, profile=self.request.user.profile,
                                                       content_object=conversion_funds,
                                                       action_type="exchange_funds",
                                                       shop=None, amount=form.billing_information.get("amount"),
                                                       commission=0,
                                                       amount_with_commission=form.billing_information.get("amount"),
                                                       currency=form.cleaned_data.get("debit_currency"),
                                                       payment_system=form.ps,
                                                       payment_kind="I")
            user_balance_for_debit_ph.save()


            """  withdrawal funds to user"""
            user_balance_for_credit.state -= form.billing_information.get("amount_with_commission")
            user_balance_for_credit.save()

            user_balance_for_credit_ph = PaymentHistory(user=self.request.user, profile=self.request.user.profile,
                                                        content_object=conversion_funds,
                                                        action_type="exchange_funds",
                                                        shop=None, amount=form.billing_information.get("amount_real"),
                                                        commission=form.billing_information.get("commission_real"),
                                                        amount_with_commission=form.billing_information.get("amount_with_commission"),
                                                        currency=form.cleaned_data.get("credit_currency"),
                                                        payment_system=form.ps,
                                                        payment_kind="E")
            user_balance_for_credit_ph.save()
            messages.info(self.request, _("Conversion of funds has been successful"))
            return HttpResponseRedirect(self.success_url)
        else:
            context = self.get_context_data(**kwargs)
            context.update({
                "form": form,
                "credit_amount": form.cleaned_data["credit_amount"],
                "credit_commission": form.billing_information.get("commission_real"),
                "credit_amount_with_commission": form.billing_information.get("amount_with_commission"),
                "credit_currency": form.cleaned_data["credit_currency"],
                "debit_amount": form.billing_information.get("amount"),
                "debit_currency": form.cleaned_data["debit_currency"],
                "exchange_rate": form.exchange_rate,
            })
            return render(self.request, self.get_template_names(), context)


    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(form=ConversionFundsForm(user=request.user)))

    def post(self, request, *args, **kwargs):
        form = ConversionFundsForm(user=request.user, credit_currency_id=request.POST.get("credit_currency"), data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ConversionFundsCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class ConversionFundsList(FormView):
    form_class = DateFilterForm
    model = ConversionFund
    template_name = "account/conversion_funds/list/view.html"
    template_name_ajax = "account/conversion_funds/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": ["credit_amount", "credit_currency"],
            "order_name": "credit_amount",
            "title": _("Credit amount"),
        },
        {
            "name": ["credit_amount_with_commission", "credit_currency"],
            "order_name": "credit_amount_with_commission",
            "title": _("Credit amount with commission"),
        },
        {
            "name": ["debit_amount", "debit_currency"],
            "order_name": "debit_amount",
            "title": _("Debit amount"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create conversion"),
        },
    ]

    object_options = [
        {
            "name": "conversion_funds_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(ConversionFundsList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("user", "debit_currency", "credit_currency").filter(user=self.request.user)
        if len(orders) != 0:
            qs = qs.order_by(*orders)
        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Conversion funds"), "conversion_funds")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(ConversionFundsList, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
            "object_options": self.object_options,
        })
        return context


class ConversionFundsView(DetailView):
    model = ConversionFund
    template_name = "account/conversion_funds/view/view.html"
    template_name_ajax = "account/conversion_funds/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().user != self.request.user:
            messages.info(self.request, _("You can't view the document currency conversion"))
            return HttpResponseRedirect(get_success_url("/conversion_funds/list/"))
        return super(ConversionFundsView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(ConversionFundsView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context