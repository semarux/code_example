# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView
from django.shortcuts import render
from django.db import transaction
from django.contrib import messages
from financial_system.helpers.system_functs import get_current_site_id


from financial_system.forms.account import DateFilterForm
from financial_system.forms.account.regular_payment import *

from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.views.generic.detail import DetailView


class RegularPaymentList(FormView):
    form_class = DateFilterForm
    model = RegularPayment
    template_name = "account/regular_payment/list/view.html"
    template_name_ajax = "account/regular_payment/list/_view.html"

    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": ["recipient.username", "recipient.first_name", "recipient.last_name"],
            "order_name": "recipient",
            "title": _("Recipient"),
            "action_update": "regular_payment_update",
        },
        {
            "name": ["amount", "currency.iso_code"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": ["comment"],
            "order_name": "comment",
            "title": _("Comment"),
        },
        {
            "name": "date_last_payment",
            "order_name": "date_last_payment",
            "title": _("Date of last payment"),
        },
        {
            "name": "date_next_payment",
            "order_name": "date_next_payment",
            "title": _("Date of next payment")
        },
        {
            "name": "active",
            "order_name": "active",
            "title": _("Active")
        }
    ]

    multiple_action = [
        ("delete_records", _("Delete records")),
        ("activate_records", _("Activate record"))
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        return super(RegularPaymentList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self,):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("user", "user__username", "sender", "recipient", "currency").filter(sender=self.request.user)

        if len(orders) != 0:
            qs = qs.order_by(*orders)
        return qs

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def activate_records(self, items):
        items.update(active=True)
        messages.info(self.request, _("Activated %s rows" % items.count()))
        return HttpResponseRedirect(self.request.path)

    def delete_records(self, items):
        count_items = items.count()
        items.delete()
        messages.info(self.request, _("Deleted %s rows" % count_items))
        return HttpResponseRedirect(self.request.path)

    def post(self, request, *args, **kwargs):
        if request.POST.get("multiple_action"):
            obj_items = self.model.objects.filter(id__in=request.POST.getlist("check_item", []))
            return eval("self.%s(obj_items)" % request.POST.get("multiple_action"))

        else:
            self.object_list = self.get_queryset()
            return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(RegularPaymentList, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
            "multiple_action": self.multiple_action,
        })
        return context


class RegularPaymentOrderList(FormView):
    form_class = DateFilterForm
    model = OrderRegularPayment
    template_name = "account/regular_payment/order/list/view.html"
    template_name_ajax = "account/regular_payment/order/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": "get_sender_name",
            "order_name": "regular_payment__sender",
            "title": _("Sender"),
        },
        {
            "name": "get_recipient_name",
            "order_name": "regular_payment__recipient",
            "title": _("Recipient"),
        },
        {
            "name": ["amount", "currency"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": ["amount_with_commission", "currency"],
            "order_name": "amount_with_commission",
            "title": _("Amount with commission"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create transfer"),
        },
        {
            "name": "paid",
            "order_name": "paid",
            "title": _("Paid"),
        },
    ]

    object_options = [
        {
            "name": "regular_payment_order_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(RegularPaymentOrderList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related()
        if self.kwargs.get("filter", "") == "for_me":
            qs = qs.filter(regular_payment__recipient=self.request.user)
        else:
            qs = qs.filter(regular_payment__sender=self.request.user)

        qs = qs.order_by(*orders)
        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Orders of regular payments"), "order_regular_payment")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(RegularPaymentOrderList, self).get_context_data(**kwargs)
        object_columns = self.object_columns[:]
        if self.kwargs.get("filter", "") == "for_me":
            del object_columns[2]
        else:
            del object_columns[1]

        context.update({
            "main_menu_page": "account",
            "object_columns": object_columns,
            "object_options": self.object_options,
        })
        return context


class RegularPaymentCreate(FormView):
    form_class = CreateRegularPaymentForm
    template_name = "account/regular_payment/create/view.html"
    template_name_ajax = "account/regular_payment/create/_view.html"
    template_name_ajax_form = "account/regular_payment/create/_form.html"
    success_url = "/regular_payment/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            # if self.request.method == "POST":
            #     form = self.get_form(self.form_class)
            #     if not form.is_valid():
            #         return [self.template_name_ajax_form]
                # return [self.template_name_ajax_form]
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(RegularPaymentCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        reg_payment = form.save(commit=False)
        reg_payment.sender = self.request.user
        reg_payment.save()
        messages.info(self.request, _("Regular payment has been successfully created"))
        return HttpResponseRedirect(self.success_url)

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(form=CreateRegularPaymentForm(user=request.user)))

    def post(self, request, *args, **kwargs):
        form = CreateRegularPaymentForm(user=request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(RegularPaymentCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class RegularPaymentOrderView(DetailView):
    model = OrderRegularPayment
    template_name = "account/regular_payment/order/view/view.html"
    template_name_ajax = "account/regular_payment/order/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().regular_payment.sender != self.request.user and self.get_object().regular_payment.recipient != self.request.user:
            messages.info(self.request, "You can not view this regular payment")
            return HttpResponseRedirect(get_success_url("/regular_payment/orders/my/"))
        return super(RegularPaymentOrderView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(RegularPaymentOrderView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context



class RegularPaymentUpdate(UpdateView):
    form_class = UpdateRegularPaymentForm
    model = RegularPayment
    template_name = "account/regular_payment/update/view.html"
    template_name_ajax = "account/regular_payment/update/_view.html"
    template_name_ajax_form = "account/regular_payment/update/_form.html"
    success_url = "/regular_payment/list/"
    success_url = "/regular_payment/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        if self.get_object().sender != self.request.user:
            messages.info(self.request, _("You can't update this regular payment"))
            return HttpResponseRedirect(get_success_url("/regular_payment/list/"))
        self.form = UpdateRegularPaymentForm(user=self.request.user)
        return super(RegularPaymentUpdate, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({"form": UpdateRegularPaymentForm(user=request.user, instance=self.object)})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = UpdateRegularPaymentForm(user=request.user, data=request.POST, instance=self.get_object())
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        reg_payment = form.save(commit=False)
        reg_payment.sender = self.request.user
        reg_payment.save()
        messages.info(self.request, _("Regular payment has been successfully updated"))
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(RegularPaymentUpdate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context