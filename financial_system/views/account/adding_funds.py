# -*- coding: utf-8 -*-
import base64
import hashlib
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render
from webmoney.models import Invoice as WebmoneyInvoice
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from financial_system.forms.account import DateFilterForm
from financial_system.forms.account.adding_funds import *
from financial_system.forms.account.payment_system import *
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.helpers.amount import *

from financial_system.models import *
from torgplace.settings import YANDEX_URI, MAIN_PAYMENT_SYSTEM
from django.contrib import messages
from main.helpers.main import profile_required
from django.views.generic.detail import DetailView
from financial_system.helpers.system_functs import get_current_site_id

from cStringIO import StringIO
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase import ttfonts


# class AddingFunds(FormView):
#     ps_sys_code = "WM"#1
#     form_class = AddingFundsForm
#
#     template_name = "account/adding_funds/create/view.html"
#     template_name_ajax = "account/adding_funds/create/_view.html"
#
#     def get_payment_system(self):
#         try:
#             if self.kwargs.get("payment_sys_code", None):
#                 self.ps_sys_code = self.kwargs.get("payment_sys_code")
#                 ps = PaymentSystem.objects.get(sys_code=self.ps_sys_code)
#                 return ps
#             else:
#                 return PaymentSystem.objects.get(sys_code="WM")
#         except:
#             return False
#
#     def get_template_names(self):
#         if self.request.is_ajax():
#             return [self.template_name_ajax]
#         else:
#             return [self.template_name]
#
#     @method_decorator(login_required())
#     @method_decorator(profile_required(redirect_to="/"))
#     @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
#     @method_decorator(check_system(redirect_to="/"))
#     def dispatch(self, *args, **kwargs):
#         return super(AddingFunds, self).dispatch(*args, **kwargs)
#
#     def form_valid(self, form, **kwargs):
#         context = self.get_context_data(**kwargs)
#         # billing_information = get_billing_information(form.cleaned_data.get("amount"), form.cleaned_data.get("currency"), form.ps_currency, self.request.user, self.get_payment_system(), "adding_funds")
#         billing_information = get_billing_information(form.cleaned_data.get("amount"), form.exchange_rate, form.opposite_exchange_rate, self.request.user, self.get_payment_system(), "adding_funds")
#         context.update(
#             {
#                 "form": form,
#                 "order_form": AddingFundCreateOrderForm(ps_sys_code=self.ps_sys_code, user=self.request.user, initial=form.cleaned_data),
#                 "payment_amount": billing_information.get("payment_amount"),
#                 "payment_amount_real": billing_information.get("payment_amount_real"),
#                 "amount_real": billing_information.get("amount_real"),
#                 "currency": form.cleaned_data.get("currency"),
#                 "commission": billing_information.get("commission"),
#                 "ps_currency": form.cleaned_data.get("ps_currency_type"),
#             }
#         )
#         return render(self.request, self.get_template_names(), context)
#
#     def get(self, request, *args, **kwargs):
#         context = self.get_context_data(**kwargs)
#         purse = get_obj_or_none(Purse, payment_system=self.get_payment_system(), is_work=True, active=True)
#         if self.ps_sys_code == "YM" and purse and not request.GET.get("yandex_auth_code", None):
#             yandex_auth_form = YandexAuthForm(initial={
#                 "client_id": purse.password,
#                 "response_type": "code",
#                 "redirect_uri": purse.result_url,
#                 "scope": "account-info operation-details operation-history payment-p2p",
#             })
#             context.update({"yandex_auth_form": yandex_auth_form, "yandex_auth_uri": YANDEX_URI.get("auth")})
#         context.update(
#             {
#                 "form": AddingFundsForm(user=request.user, ps_sys_code=self.ps_sys_code),
#             }
#         )
#         return render(self.request, self.get_template_names(), context)
#
#     def post(self, request, *args, **kwargs):
#         form = AddingFundsForm(user=request.user, ps_sys_code=self.kwargs.get("payment_sys_code"), data=request.POST)
#         if form.is_valid():
#             return self.form_valid(form)
#         else:
#             return self.form_invalid(form)
#
#     def get_context_data(self, **kwargs):
#         context = super(AddingFunds, self).get_context_data(**kwargs)
#         if self.request.GET.get("yandex_auth_code", None):
#             context.update({"yandex_auth_code": self.request.GET.get("yandex_auth_code")})
#
#         context.update({
#             "main_menu_page": "account",
#             "payment_system": self.get_payment_system(),
#             "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM,),
#         })
#         return context


class AddingFundCreate(FormView):
    ps_sys_code = "WM"
    form_class = AddingFundsCreateForm

    template_name = "account/adding_funds/create/view.html"
    template_name_ajax = "account/adding_funds/create/_view.html"

    def get_payment_system(self):
        try:
            if self.kwargs.get("payment_sys_code", None):
                self.ps_sys_code = self.kwargs.get("payment_sys_code")
                ps = PaymentSystem.objects.get(sys_code=self.ps_sys_code)
                return ps
            else:
                return PaymentSystem.objects.get(sys_code="WM")
        except:
            return False

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        return super(AddingFundCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        ps_form = None
        purse = form.purse
        context.update(
            {
                "form": form,
                "payment_amount": form.billing_information.get("payment_amount"),
                "payment_amount_real": form.billing_information.get("payment_amount_real"),
                "amount_real": form.billing_information.get("amount_real"),
                "currency": form.cleaned_data.get("currency"),
                "commission": form.billing_information.get("commission"),
                "ps_currency": form.cleaned_data.get("ps_currency_type"),
           }
        )

        if purse:
            context.update({
                "ps_form_url": purse.url,
            })

        if self.ps_sys_code == "BT" and form.bank:
            context.update({
                "bank": form.bank,
            })

        if form.cleaned_data.get("spec_action") == "estimate":
            return render(self.request, self.get_template_names(), context)
        else:
            order = form.save(commit=False)
            order.user = self.request.user
            order.profile = self.request.user.profile
            order.amount = form.billing_information.get("amount_real", 0)
            order.commission = form.billing_information.get("commission_real", 0)
            order.amount_with_commission = form.billing_information.get("amount_with_commission", 0)
            order.payment_system = self.get_payment_system()
            if purse:
                order.purse_to = purse.username
            order.ps_amount = form.billing_information.get("payment_amount")
            order.ps_currency = form.ps_currency
            order.save()

            if self.ps_sys_code == "BT":
                transaction = hashlib.md5()
                transaction.update("%s" % order.id)
                order.transaction = transaction.hexdigest()
                order.bank = form.bank
                order.save()
                return HttpResponseRedirect(get_success_url("/adding_funds/list/"))

            if self.ps_sys_code == "WM":
                ps_form = AddingFundsWebmoneyForm(initial={
                    "LMI_PAYMENT_AMOUNT": form.billing_information.get("payment_amount"),
                    "LMI_PAYEE_PURSE": purse.username,
                    "LMI_PAYMENT_NO": WebmoneyInvoice.objects.create(user=self.request.user).payment_no,
                    "LMI_PAYMENT_DESC": purse.description,
                    "order_id": order.id,
                })
            elif self.ps_sys_code == "YM":
                ps_form = AddingFundsYandexForm(initial={
                    "code": self.request.GET.get("yandex_auth_code", ""),
                    "order_id": order.id,
                })

            elif self.ps_sys_code == "QM":
                ps_form = AddingFundsQiwiForm(initial={
                    "user": form.cleaned_data["phone"],
                    "amount": form.billing_information.get("payment_amount"),
                    "ccy":  QM_REAL_CURRENCY_CHOICE.get(form.cleaned_data.get("ps_currency_type", ""), "RUB"),
                    "comment": purse.description,
                    "lifetime": order.date_create,
                    "prv_name": purse.description,
                    "pay_source": form.cleaned_data["qm_pay_source"],
                    "order_id": order.id
                })
            elif self.ps_sys_code == "LPM":
                payment_type = "buy"
                description = purse.description
                signature = base64.encodestring(hashlib.sha1(
                        "%s%s%s%s%s%s%s%s%s" % (
                            purse.signature,
                            form.billing_information.get("payment_amount"),
                            LP_REAL_CURRENCY_CHOICE.get(form.cleaned_data.get("ps_currency_type", ""), "RUB"),
                            purse.username,
                            order.id,
                            payment_type,
                            description,
                            purse.result_url,
                            purse.server_url
                        )).digest().strip()).strip()

                ps_form = AddingFundsLiqpayForm(initial={
                    "public_key": purse.username,
                    "amount": form.billing_information.get("payment_amount"),
                    "currency": LP_REAL_CURRENCY_CHOICE.get("ps_currency_type", "RUB"),
                    "description": description,
                    "order_id": order.id,
                    "type": payment_type,
                    "signature": signature,
                    "result_url": purse.result_url,
                    "server_url": purse.server_url,
                })

            if ps_form:
                context.update({"ps_form": ps_form})
            return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = AddingFundsCreateForm(ps_sys_code=self.kwargs.get("payment_sys_code"), bank_id=self.request.POST.get("bank"), user=self.request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        purse = get_obj_or_none(Purse, payment_system=self.get_payment_system(), is_work=True, active=True)
        if self.ps_sys_code == "YM" and purse and not request.GET.get("yandex_auth_code", None):
            yandex_auth_form = YandexAuthForm(initial={
                "client_id": purse.password,
                "response_type": "code",
                "redirect_uri": purse.result_url,
                "scope": "account-info operation-details operation-history payment-p2p",
            })
            context.update({"yandex_auth_form": yandex_auth_form, "yandex_auth_uri": YANDEX_URI.get("auth")})

        context.update(
            {
                "form": AddingFundsCreateForm(user=request.user, ps_sys_code=self.ps_sys_code),
            }
        )
        return render(self.request, self.get_template_names(), context)

    def get_context_data(self, **kwargs):
        context = super(AddingFundCreate, self).get_context_data(**kwargs)
        if self.request.GET.get("yandex_auth_code", None):
            context.update({"yandex_auth_code": self.request.GET.get("yandex_auth_code")})
        context.update({
            "main_menu_page": "account",
            "form_url": "https://merchant.webmoney.ru/lmi/payment.asp",
            "payment_system": self.get_payment_system(),
            "payment_systems": PaymentSystem.objects.filter(active=1, for_adding_funds=1).exclude(sys_code=MAIN_PAYMENT_SYSTEM),
        })
        return context


class AddingFundsList(FormView):
    form_class = DateFilterForm
    model = AddingFund
    template_name = "account/adding_funds/list/view.html"
    template_name_ajax = "account/adding_funds/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": "transaction",
            "order_name": "transaction",
            "title": _("Transaction"),
        },
        {
            "name": ["amount", "currency"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": ["amount_with_commission", "currency"],
            "order_name": "amount_with_commission",
            "title": _("Amount with commission"),
        },
        {
            "name": "payment_system",
            "order_name": "payment_system",
            "title": _("Payment System"),
        },
        {
            "name": "purse_from",
            "order_name": "purse_from",
            "title": _("Purse"),
        },
        {
            "name": ["ps_amount", "ps_currency"],
            "order_name": "ps_amount",
            "title": _("Payment system amount"),
        },
        {
            "name": "get_status",
            "order_name": "status",
            "title": _("Status"),
        },
        {
            "name": "paid",
            "order_name": "paid",
            "title": _("Paid"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create transfer"),
        },
    ]

    object_options = [
        {
            "name": "adding_funds_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if get_current_site_id() == 1:
            self.success_url = "/%s%s" % ("account", self.success_url)
        return super(AddingFundsList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("user", "profile", "currency", "payment_system", "ps_currency").filter(user=self.request.user)
        if len(orders) != 0:
            qs = qs.order_by(*orders)

        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Adding funds"), "adding_funds")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(AddingFundsList, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
            "object_options": self.object_options,
        })
        return context


class AddingFundsView(DetailView):
    model = AddingFund
    template_name = "account/adding_funds/view/view.html"
    template_name_ajax = "account/adding_funds/view/_view.html"


    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    # @method_decorator(identity_doc(redirect_to="/adding_funds/list/", obj=object))
    def dispatch(self, *args, **kwargs):
        if self.get_object().user != self.request.user:
            messages.info(self.request, "Adding funds document doesn't belong to you")
            return HttpResponseRedirect(get_success_url("/adding_funds/list/"))
        return super(AddingFundsView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(AddingFundsView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class BankPaymentBlankView(DetailView):
    model = AddingFund


    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    # @method_decorator(identity_doc(redirect_to="/adding_funds/list/", obj=object))
    def dispatch(self, *args, **kwargs):
        if self.get_object().user != self.request.user:
            messages.info(self.request, "Adding funds document doesn't belong to you")
            return HttpResponseRedirect(get_success_url("/adding_funds/list/"))
        return super(BankPaymentBlankView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        response = HttpResponse(mimetype='application/pdf')
        obj = self.get_object()
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % obj.transaction
        temp = StringIO()
        p = canvas.Canvas(temp)
        font = ttfonts.TTFont('Arial', settings.MEDIA_ROOT+'/arial.ttf')
        pdfmetrics.registerFont(font)

        p.setFont('Arial', 14)
        rows = [
            {"x": 100, "y": 800, "data": _("Verifair.com")._proxy____text_cast()},
            {"x": 50, "y": 780, "data": "%s: %s" % (_("Bank name")._proxy____text_cast(), obj.bank.name)},
            {"x": 50, "y": 760, "data": "%s: %s" % (_("Bank address")._proxy____text_cast(), obj.bank.address)},
            {"x": 50, "y": 740, "data": "%s: %s" % (_("SWIFT")._proxy____text_cast(), obj.bank.swift)},
            {"x": 50, "y": 720, "data": "%s: %s" % (_("Account number")._proxy____text_cast(), obj.bank.account_number)},
            {"x": 50, "y": 700, "data": "%s: %s" % (_("Id transaction")._proxy____text_cast(), obj.transaction)},
            {"x": 50, "y": 680, "data": "%s: %s %s" % (_("Amount")._proxy____text_cast(), obj.ps_amount, obj.ps_currency)},
        ]
        for r in rows:
            p.drawString(r["x"], r["y"], r["data"])

        p.showPage()
        p.save()
        response.write(temp.getvalue())
        return response