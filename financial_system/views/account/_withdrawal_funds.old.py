# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import transaction

from financial_system.forms.account import DateFilterForm
from financial_system.forms.account.withdrawal_funds import *

from financial_system.helpers.system_functs import get_current_site_id
from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_list_or_404, get_object_or_404
from django.shortcuts import render


class WithdrawalFundsCreate(FormView):
    form_class = WithdrawalFundsForm
    payment_system = None
    template_name = "account/withdrawal_funds/create/view.html"
    template_name_ajax = "account/withdrawal_funds/create/_view.html"
    success_url = "/withdrawal_funds/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if get_current_site_id() == 1:
            self.success_url = "/%s%s" % ("account", self.success_url)
        return super(WithdrawalFundsCreate, self).dispatch(*args, **kwargs)

    def get_bank_requisite(self, form_data, ps_sys_code):
        if ps_sys_code != "BT":
            requisites = ""
        else:
            requisites = {}
            for requisite in BANK_REQUISITES:
                requisites.update({requisite[0]: form_data.get(requisite[0], "")})
        return requisites

    def form_valid(self, form, **kwargs):
        if form.cleaned_data.get("spec_action") == "withdrawal":
            withdrawal = form.save(commit=False)
            withdrawal.user = self.request.user
            withdrawal.profile = self.request.user.profile
            withdrawal.amount_to_withdrawal = form.cleaned_data["amount_to_withdrawal"] - form.billing_information_withdrawal["commission_real"]
            withdrawal.commission_to_withdrawal = form.billing_information_withdrawal["commission_real"]
            withdrawal.amount_with_commission_to_withdrawal = form.cleaned_data["amount_to_withdrawal"]
            withdrawal.bank_requisite = self.get_bank_requisite(form.cleaned_data, form.ps.sys_code)
            withdrawal.save()
            messages.info(self.request, _("Funds will be withdrawn after consideration of the application by the moderator"))
            return HttpResponseRedirect(self.success_url)
        else:
            context = self.get_context_data(**kwargs)
            form_initial = form.cleaned_data.copy()
            if form.cleaned_data["main_amount"] == "withdrawal":
                form_initial.update({"amount_to_obtain": form.billing_information_withdrawal["amount"] - form.billing_information_withdrawal["commission_real"]})
            elif form.cleaned_data["main_amount"] == "obtain":
                form_initial.update({"amount_to_withdrawal": form.billing_information_obtain["amount"]})

            if form.ps.sys_code == "BT":
                form_initial.update({"currency_to_obtain": form.cleaned_data["currency_to_obtain"].iso_code})

            amount_to_withdrawal = form_initial["amount_to_withdrawal"]
            commission = form.billing_information_withdrawal["commission_real"]
            currency_to_withdrawal = form.cleaned_data["currency_to_withdrawal"]
            amount_to_obtain = form_initial["amount_to_obtain"]
            currency_to_obtain = form.cleaned_data["currency_to_obtain"]
            amount_to_withdrawal_with_commission = form.billing_information_withdrawal["amount_with_commission"]
            form = WithdrawalFundsForm(user=self.request.user, payment_system=form.ps, initial=form_initial)
            context.update({
                "form": form,
                "amount_to_withdrawal": amount_to_withdrawal,
                "currency_to_withdrawal": currency_to_withdrawal,
                "amount_to_withdrawal_with_commission": amount_to_withdrawal_with_commission ,
                "commission": commission,
                "amount_to_obtain": amount_to_obtain,
                "currency_to_obtain": currency_to_obtain,
            })
            return render(self.request, self.get_template_names(), context)

    def get(self, request, *args, **kwargs):
        if self.kwargs.get("payment_system_id"):
            self.payment_system = get_object_or_404(PaymentSystem, pk=self.kwargs.get("payment_system_id", 0))
        return self.render_to_response(self.get_context_data(form=WithdrawalFundsForm(user=request.user, payment_system=self.payment_system)))

    def post(self, request, *args, **kwargs):
        form = WithdrawalFundsForm(user=request.user, data=request.POST, payment_system=get_obj_or_none(PaymentSystem, pk=request.POST.get("payment_system")))
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(WithdrawalFundsCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context


class WithdrawalFundsList(FormView):
    form_class = DateFilterForm
    model = Withdrawal
    template_name = "account/withdrawal_funds/list/view.html"
    template_name_ajax = "account/withdrawal_funds/list/_view.html"

    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": ["amount_with_commission_to_withdrawal", "currency_to_withdrawal.iso_code"],
            "order_name": "amount_with_commission_to_withdrawal",
            "title": _("Amount to withdrawal"),
        },
        {
            "name": ["amount_to_obtain", "currency_to_obtain.iso_code"],
            "order_name": "amount_to_obtain",
            "title": _("Amount to obtain"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create withdrawal"),
        },
        {
            "name": "date_pay",
            "order_name": "date_pay",
            "title": _("Date pay withdrawal"),
        },
        {
            "name": "status",
            "order_name": "status",
            "title": _("Status")
        },
        {
            "name": "paid",
            "order_name": "paid",
            "title": _("Paid")
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        return super(WithdrawalFundsList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("user", "profile", "withdrawal_manager", "currency_to_withdrawal",
                                               "currency_to_obtain", "payment_system").filter(user=self.request.user)
        if len(orders) != 0:
            qs = qs.order_by(*orders)

        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Withdrawal funds"), "withdrawal_funds")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(WithdrawalFundsList, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
        })
        return context