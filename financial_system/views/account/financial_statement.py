# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from financial_system.forms.account import DateFilterForm
from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _


class FinancialStatement(FormView):
    form_class = DateFilterForm
    template_name = "account/financial_statement/view.html"
    template_name_ajax = "account/financial_statement/_view.html"
    """ We need more information """
    object_columns = [
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date"),
        },
        {
            "name": "get_action_type",
            "order_name": "action_type",
            "title": _("Operation"),
        },
        {
            "name": "payment_system",
            "order_name": "payment_system",
            "title": _("Payment system"),
        },
        {
            "name": "get_payment_kind",
            "order_name": "payment_kind",
            "title": _("Payment kind"),
        },
        {
            "name": ["amount", "currency.iso_code"],
            "order_name": "amount",
            "title": _("Amount"),
        }
    ]


    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(profile_required(redirect_to="/"))
    def dispatch(self, *args, **kwargs):
        return super(FinancialStatement, self).dispatch(*args, **kwargs)

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = PaymentHistory.objects.select_related("currency", "payment_system", "user", "profile").filter(user=self.request.user)

        if len(orders) != 0:
            qs = qs.order_by(*orders)

        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Financial statement"), "financial_statement")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(FinancialStatement, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
        })
        return context
