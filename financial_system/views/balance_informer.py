# -*- coding: utf-8 -*-
import hashlib
import random

from django.views.generic.edit import FormView
from django.views.generic import View, TemplateView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.utils.translation import ugettext as _
from django.contrib import messages
from financial_system.templatetags.finance_tags import active
from main.helpers.main import profile_required

from main.tasks import send_inform_mail
from financial_system.forms.member import *
from main.models import get_user_code
from django.contrib.sites.models import get_current_site

from rest_framework.authtoken.models import Token
from financial_system.helpers.amount import get_success_url
from financial_system.helpers.system_functs import save_data_to_file
from django.views.decorators.csrf import csrf_exempt
from social_auth.db.django_models import UserSocialAuth
from social_auth.db.django_models import UserSocialAuthMixin
from social_auth.backends.pipeline.user import get_username

from django.utils.encoding import smart_str
from django.template.defaultfilters import slugify
import pytils
from django.contrib.auth import authenticate, login, logout
from django.test.utils import override_settings

class SocAuthView(View):
    template_name = "balance_informer/view.html"
    template_name_ajax = "balance_informer/_view.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]


    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SocAuthView, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        shop = get_obj_or_none(Shop, pk=kwargs.get("shop_id", 0))
        return render(request, self.get_template_names(), {"shop": shop, })

    @method_decorator(transaction.commit_on_success)
    def get_or_create_user_profile(self, request):
        user_social_auth = get_obj_or_none(UserSocialAuth, provider=request.POST.get("provider"), uid=request.POST.get("uid"))
        if user_social_auth:
            user = user_social_auth.user
        else:
            username = get_username(
                    details={'username': request.POST.get("username"), 'fullname': request.POST.get("username"),
                     'last_name': '', 'email': '', 'first_name': request.POST.get("username")}, user=None)

            user = get_obj_or_none(User, username=username['username'])
            if not user:
                user = UserSocialAuth.create_user(username=username['username'], email=request.POST.get("email"))
                if len(user.social_auth.all()) == 0:
                    try:
                        UserSocialAuth.create_social_auth(user=user, uid=request.POST.get("uid"), provider=request.POST.get("provider"))
                    except:
                        pass

            profile = get_obj_or_none(Profile, user=user)
            if not profile:
                profile = Profile(user=user, currency_id=request.session.get('selected_currency', 1),
                          company_location_id=request.session.get('selected_location', None),
                          location_id=request.session.get('selected_location', None),
                          activated=1,
                          user_code=get_user_code(user.id),
                          language=request.session.get('django_language', 'en'), company_name=user.username,
                          slug=smart_str(slugify(pytils.translit.translify(user.username))),
                )
                profile.special_id = int("%s%s" % (1000, profile.user.id))
                profile.save()

        user.backend = 'django.contrib.auth.backends.ModelBackend'
        return user

    def post(self, request, **kwargs):
        user = self.get_or_create_user_profile(request)
        shop = get_obj_or_none(Shop, pk=kwargs.get("shop_id", 0))
        login(self.request, user)
        request.session['django_language'] = request.user.profile.language
        user_balance = UserBalance.objects.filter(user=request.user, payment_coupon=None)
        return render(request, self.get_template_names(), {"shop": shop, "user_balance": user_balance})


class BalanceInformerView(View):
    template_name = "balance_informer/view.html"
    template_name_ajax = "balance_informer/_view.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]


    @method_decorator(transaction.commit_on_success)
    # @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(BalanceInformerView, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        shop = get_obj_or_none(Shop, pk=kwargs.get("shop_id"))
        if shop and not request.user.is_authenticated():
            request.session['django_language'] = shop.informer_language


        user_balance = None
        if request.user.is_authenticated():
            user_balance = UserBalance.objects.filter(user=request.user, payment_coupon=None)
        return render(request, self.get_template_names(), {"shop": shop, "user_balance": user_balance})


class Logout(View):
    def dispatch(self, request, *args, **kwargs):
        return super(Logout, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        logout(request)
        return HttpResponseRedirect('/balance_informer/%s/' % kwargs.get("shop_id", 0))


class Login(FormView):
    shop_id = None
    form_class = LoginForm
    template_name = "balance_informer/view.html"
    template_name_ajax = "balance_informer/_view.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def form_valid(self, form):
        log_user = form.get_user()
        login(self.request, log_user)
        self.request.session['django_language'] = self.request.user.profile.language

        return HttpResponseRedirect('/balance_informer/%s/' % self.shop_id)

    def form_invalid(self, form):
        shop = get_obj_or_none(Shop, pk=self.shop_id)

        return render(self.request, self.get_template_names(), {"shop": shop, "login_form": form})

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/balance_informer/%s/' % kwargs.get("shop_id", 0))

    def post(self, request, *args, **kwargs):
        form = LoginForm(data=request.POST)
        self.shop_id = kwargs.get("shop_id", 0)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

