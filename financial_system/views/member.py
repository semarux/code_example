# -*- coding: utf-8 -*-
import hashlib
import random

from django.views.generic.edit import FormView
from django.views.generic import View, TemplateView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.utils.translation import ugettext as _
from django.contrib import messages
from financial_system.templatetags.finance_tags import active
from main.helpers.main import profile_required

from main.tasks import send_inform_mail
from financial_system.forms.member import *
from main.models import get_user_code
from django.contrib.sites.models import get_current_site

from rest_framework.authtoken.models import Token
from financial_system.helpers.amount import get_success_url
from financial_system.helpers.system_functs import save_data_to_file
from django.views.decorators.csrf import csrf_exempt
from social_auth.db.django_models import UserSocialAuth

from django.utils.encoding import smart_str
from django.template.defaultfilters import slugify
import pytils
from django.contrib.auth import authenticate


class TokenView(View):
    success_url = "/account_settings/"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, request, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(TokenView, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        try:
            token = self.request.user.auth_token
        except:
            Token.objects.create(user=self.request.user)
        else:
            token.delete()
            Token.objects.create(user=self.request.user)
        return HttpResponseRedirect(self.success_url)


class Registration(View):
    template_name = "member/registration/registration_view.html"

    @method_decorator(transaction.commit_on_success)
    def dispatch(self, request, *args, **kwargs):
        return super(Registration, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        user_form = RegistrationUserForm()
        profile_form = RegistrationProfileForm()
        return render(request, self.template_name, {'user_form': user_form, 'profile_form': profile_form})

    def post(self, request, **kwargs):
        user_form = RegistrationUserForm(data=request.POST)
        profile_form = RegistrationProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()
            user.username = 100000 + user.id
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.user_code = get_user_code(user.id)
            profile.activation_key = hashlib.sha1(hashlib.sha1(str(random.random())).hexdigest()[:5]+str(user.id)).hexdigest()
            profile.activated = 1
            profile.access_level = 0
            profile.special_id = "%s%s" % (1000, profile.user.id)
            profile.save()
            #todo: enter params
            send_inform_mail.delay('member/registration/registration_email_subject.txt', 'member/registration/registration_email.html', user.email, options={'user': user, 'password': password})
            return HttpResponseRedirect('/registration/complete/')
        else:
            return render(request, self.template_name, {'user_form': user_form, 'profile_form': profile_form})


class SocialUserLogin(View):
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(SocialUserLogin, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if self.request.session.has_key("shop_payment_form_data"):
            return HttpResponseRedirect("/shop/perform_payment/")
        return HttpResponseRedirect("/profile/")


class Login(FormView):
    form_class = LoginForm
    template_name = "member/login/login_view.html"
    success_url = '/profile/'


    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(Login, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        if self.request.GET.get("next"):
            return HttpResponseRedirect(self.request.GET["next"])
        return HttpResponseRedirect(self.success_url)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(self.success_url)
        else:
            return super(Login, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ProfileDetail(View):
    model = Profile
    template_name = "member/profile/profile_view.html"
    template_name_ajax = "member/profile/_profile_view.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to='/'))
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetail, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.get_template_names(), {"profile": request.user.profile, "main_menu_page": "profile"})


class ProfileSettings(FormView):
    template_name = "member/settings/view.html"
    template_name_ajax = "member/settings/_view.html"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to='/'))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        return super(ProfileSettings, self).dispatch(*args, **kwargs)

    def get_account_settings(self):
        return get_obj_or_none(ProfileAccountSettings, user=self.request.user)

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        account_settings = self.get_account_settings()
        profile_settings_form = ProfileSettingsForm(instance=request.user.profile)
        account_settings_form = AccountSettingsForm
        if account_settings:
            account_settings_form = AccountSettingsForm(instance=account_settings)
        context.update({"profile_settings_form": profile_settings_form, "account_settings_form": account_settings_form})
        return render(request, self.get_template_names(), context)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        account_settings = self.get_account_settings()
        profile_settings_form = ProfileSettingsForm(data=request.POST, instance=request.user.profile)
        account_settings_form = AccountSettingsForm(data=request.POST, instance=request.user)
        if account_settings:
            account_settings_form = AccountSettingsForm(data=request.POST, instance=account_settings)

        if account_settings_form.is_valid() and profile_settings_form.is_valid():
            account_settings = account_settings_form.save(commit=False)
            account_settings.user = request.user
            account_settings.profile = request.user.profile
            account_settings.save()
            profile_settings_form.save()
            messages.info(request, _("Account settings saved"))
        context.update({"profile_settings_form": profile_settings_form, "account_settings_form": account_settings_form})
        return render(request, self.get_template_names(), context)

    def get_context_data(self, **kwargs):
        context = super(ProfileSettings, self).get_context_data(**kwargs)

        try:
            api_token = self.request.user.auth_token
        except:
            api_token = None

        context.update({
            "main_menu_page": "account_settings",
            "api_token": api_token,
        })
        return context