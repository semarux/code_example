# -*- coding: utf-8 -*-
from amqp.transport import _AbstractTransport
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.views.generic.edit import UpdateView
from financial_system.forms.account import DateFilterForm, payment_system
from financial_system.forms.shop import *

from financial_system.helpers.amount import *
from financial_system.models import *
from main.helpers.main import profile_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_list_or_404, get_object_or_404, render_to_response
from django.shortcuts import render
from financial_system.helpers.system_functs import get_current_site_id
from django.views.generic.detail import DetailView
import hashlib
from django.views.decorators.csrf import csrf_exempt
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.helpers.shop import get_secret_key
from django.utils import simplejson, timezone
from urlparse import urlparse
import httplib, urllib, urllib2
import requests
from django.template import RequestContext
from financial_system.forms.account.adding_funds import AddingFundsCreateForm
from financial_system.forms.account.payment_system import AddingFundsWebmoneyForm, AddingFundsQiwiForm, AddingFundsYandexForm, AddingFundsLiqpayForm
from webmoney.models import Invoice as WebmoneyInvoice
from financial_system.helpers.system_functs import save_data_to_file

class ShopCreate(FormView):
    form_class = ShopCreateForm
    template_name = "shop/create/view.html"
    template_name_ajax = "shop/create/_view.html"
    success_url = "/shop/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(ShopCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        shop = form.save(commit=False)
        shop.user = self.request.user
        shop.profile = self.request.user.profile
        secret_key = hashlib.md5()
        secret_key.update("%s%s" % (shop.id, shop.password))
        shop.secret_key = secret_key.hexdigest()
        shop.save()
        messages.info(self.request, _("Shop has been successfully created"))
        return HttpResponseRedirect(self.success_url)

    def post(self, request, *args, **kwargs):
        form = ShopCreateForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ShopCreate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "shop",
        })
        return context


class ShopList(FormView):
    model = Shop
    template_name = "shop/list/view.html"
    template_name_ajax = "shop/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": "name",
            "order_name": "name",
            "title": _("Name"),
            "action_update": "shop_update",
        },
        {
            "name": "shop_url",
            "order_name": "shop_url",
            "title": _("Url"),
        },
        {
            "name": "get_status",
            "order_name": "status",
            "title": _("Status"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create shop"),
        },
    ]

    object_options = [
        {
            "name": "shop_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(ShopList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.filter(user=self.request.user).order_by(*orders)
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(ShopList, self).get_context_data(**kwargs)
        object_columns = self.object_columns[:]
        context.update({
            "main_menu_page": "shop",
            "object_columns": object_columns,
            "object_options": self.object_options,
        })
        return context


class ShopView(DetailView):
    model = Shop
    template_name = "shop/view/view.html"
    template_name_ajax = "shop/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().user != self.request.user:
            messages.info(self.request, "Shop doesn't belong to you")
            return HttpResponseRedirect(get_success_url("/shop/list/"))
        return super(ShopView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(ShopView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "shop",
        })
        return context


class ShopUpdate(UpdateView):
    form_class = ShopUpdateForm
    model = Shop
    template_name = "shop/update/view.html"
    template_name_ajax = "shop/update/_view.html"
    template_name_ajax_form = "shop/update/_form.html"
    success_url = "/shop/list/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(check_operation_mode_for_profile_currency(redirect_to="/account_settings/"))
    @method_decorator(check_system(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        if self.get_object().user != self.request.user:
            messages.info(self.request, _("You can't update this shop"))
            return HttpResponseRedirect(get_success_url("/regular_payment/list/"))
        self.form = ShopUpdateForm()
        return super(ShopUpdate, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({"form": ShopUpdateForm(instance=self.object)})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = ShopUpdateForm(data=request.POST, instance=self.get_object())
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        shop = form.save(commit=False)
        secret_key = hashlib.md5()
        secret_key.update("%s%s" % (shop.id, shop.password))
        shop.secret_key = secret_key.hexdigest()
        shop.save()
        messages.info(self.request, _("Shop has been successfully updated"))
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(ShopUpdate, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "shop",
        })
        return context

class GetShopPerformPayment(FormView):
    form_class = GetShopPaymentForm
    template_name = "shop/get_perform_payment/view.html"
    template_name_ajax = "shop/get_perform_payment/_view.html"

    def get_signature(self, shop_id, order_id, password):
        secret_key = hashlib.md5()
        secret_key.update("%s-%s%-s" % (shop_id, order_id, password))
        return secret_key.hexdigest()

    def get(self, request, *args, **kwargs):

        context = self.get_context_data(**kwargs)
        shop_id = 9
        shop_order = 2
        shop_password = "your_password"
        payment_signature = self.get_signature(shop_id, shop_order, shop_password)
        form_initial = {
            "shop_id": shop_id,
            "shop_password": shop_password,
            "shop_order_id": shop_order,
            "payment_signature": payment_signature,
            "shop_amount": "2.01",
            "shop_currency": "EUR",
            "payment_system": "FSM",
        }
        context.update({"form": ShopPaymentForm(initial=form_initial)})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = ShopPaymentForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(GetShopPerformPayment, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "shop",
        })
        return context


class ShopPerformPayment(FormView):
    form_class = ShopPaymentForm
    ps = None
    currency = None
    sender = None
    shop = None
    model = Shop
    template_name = "shop/perform_payment/view.html"
    template_name_ajax = "shop/perform_payment/_view.html"
    template_name_fail = "shop/perform_payment/fail.html"
    template_name_success = "shop/perform_payment/success.html"
    template_payment_system = "shop/perform_payment/payment_system.html"

    success_url = "/shop/perform_payment/"

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    @method_decorator(transaction.commit_on_success)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        self.request.session["django_language"] = "en"
        return super(ShopPerformPayment, self).dispatch(*args, **kwargs)

    def get_signature(self, shop_id, order_id, password):
        secret_key = hashlib.md5()
        secret_key.update("%s-%s%-s" % (shop_id, order_id, password))
        return secret_key.hexdigest()

    def check_shop(self, id, order_id, password, signature):
        shop = get_obj_or_none(Shop, pk=id)
        if not shop:
            return {"errors": {"shop": ["This shop does not exist", ]}}
        else:
            if shop.status == "na":
                return {"errors": {"shop": ["Shop is not active", ]}}
            elif signature != self.get_signature(shop.id, order_id, shop.password):
                return {"errors": {"shop": ["Payment signature data not valid", ]}}
        return shop

    def get_error_page(self, request, shop, errors):
        params = urllib.urlencode({'errors': errors})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        conn = httplib.HTTPConnection(urlparse(shop.shop_url).hostname, 80)
        conn.request("POST", urlparse(shop.fail_url).path, params, headers)
        response = conn.getresponse()
        data = response.read()
        conn.close()
        return data

    def get_error_page2(self, shop, errors):
        return requests.post(shop.fail_url, data={'errors': errors})

    def get_user_balance(self, billing_information, amount):
        user_balance = get_obj_or_none(UserBalance, user=self.request.user, currency=self.currency, payment_coupon=None)
        if not user_balance:
            return {"user_state": ["You haven't balance in this currency.", ]}
        if user_balance.state < amount:
            return {"user_state": ["You haven't enough money in this currency.", ]}
        elif user_balance.state < billing_information["amount_with_commission"]:
            return {"user_state": [_("You haven't enough money in this currency. You must pay %s and %s" % (amount, billing_information["commission_real"]))]}
        return user_balance

    def complete_payment(self, user_balance, shop_payment, billing_information):
        getcontext().prec = 5
        if self.ps.sys_code == "FSM":
            user_balance.state -= billing_information.get("amount_with_commission")
            user_balance.save()
            sender_ph = PaymentHistory(user=shop_payment.sender, profile=shop_payment.sender.profile,
                                content_object=shop_payment, action_type="shop_payment",
                                shop=shop_payment.shop, amount=shop_payment.amount,
                                commission=0,
                                amount_with_commission=shop_payment.amount,
                                currency=shop_payment.currency,
                                payment_system=shop_payment.payment_system,
                                payment_kind="E")
            sender_ph.save()

        try:
            recipient_balance = UserBalance.objects.get(user=shop_payment.recipient, currency=self.currency, payment_coupon=None)
        except:
            ub = UserBalance(user=shop_payment.recipient, profile=shop_payment.recipient.profile, state=shop_payment.amount, currency=self.currency, payment_coupon=None)
            ub.save()
        else:
            recipient_balance.state += shop_payment.amount
            recipient_balance.save()

        recipient_ph = PaymentHistory(user=shop_payment.recipient, profile=shop_payment.recipient.profile,
                content_object=shop_payment, action_type="shop_payment",
                shop=None, amount=shop_payment.amount,
                commission=shop_payment.commission,
                amount_with_commission=shop_payment.amount_with_commission,
                currency=shop_payment.currency,
                payment_system=shop_payment.payment_system,
                payment_kind="I")
        recipient_ph.save()
        return True

    def billing_inf(self, form_data):
        currency_from = form_data.get("shop_currency")
        currency_to = form_data.get("shop_currency")

        if self.ps.sys_code == "FSM":
            self.sender = self.request.user
        #     currency_to = form_data.get("shop_currency")
        # else:
        #     currency_to = form_data.get("ps_currency_type", currency_from)

        exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency_from, currency_to))
        opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency_to, currency_from))
        billing_information = get_billing_information(form_data.get("shop_amount", Decimal(0)), exchange_rate, opposite_exchange_rate, self.sender, self.ps, "shop_payment")
        return billing_information

    def perform_payment(self, form_data):
        billing_information = self.billing_inf(form_data)
        if self.ps.sys_code == "FSM":
            user_balance = self.get_user_balance(billing_information, form_data.get("shop_amount", Decimal(0)))
            try:
                user_balance.state
            except:
                return user_balance
        else:
            user_balance = None

        shop_payment_status = "t"
        if self.shop.status == "t":
            shop_payment_status = "t"
        else:
            shop_payment_status = "r"

        shop_payment = ShopPayment(
            recipient=self.shop.user,
            sender=self.sender,
            amount=form_data.get("shop_amount", Decimal(0)),
            commission=billing_information["commission_real"],
            amount_with_commission=billing_information["amount_with_commission"],
            currency=self.currency,
            shop=self.shop,
            shop_order=form_data.get("shop_order_id"),
            payment_signature=form_data.get("payment_signature"),
            payment_system=self.ps,
            phone=form_data.get("phone"),
            status=shop_payment_status
        )
        shop_payment.save()

        if self.shop.status == "a":
            self.complete_payment(user_balance, shop_payment, billing_information)
        else:
            pass

        if self.request.session.has_key("shop_payment_form_data"):
            del self.request.session["shop_payment_form_data"]

        return shop_payment

    def check_shop_information(self, form_data):
        shop = self.check_shop(id=form_data.get("shop_id"), order_id=form_data.get("shop_order_id"),
                               password=form_data.get("shop_password"),
                               signature=form_data.get("payment_signature"))
        try:
            shop.secret_key
        except:
            shop_errors = shop
            return HttpResponse(simplejson.dumps(shop_errors), content_type="text/plain")
        else:
            self.shop = shop

        currency = get_obj_or_none(Currency, iso_code=form_data["shop_currency"])
        if not currency:
            return render_to_response(self.template_name_fail, {"form": self.get_fail_form({"currency": ["Currency does not exist", ]}), "shop": shop}, RequestContext(self.request))
        else:
            self.currency = currency
        payment_system = get_obj_or_none(PaymentSystem, sys_code=form_data.get("payment_system"))

        if not payment_system:
            return render_to_response(self.template_name_fail,
                                      {"form": self.get_fail_form({"payment_system": ["Payment system does not exist", ]}), "shop": shop},
                                      RequestContext(self.request))
        else:
            self.ps = payment_system
        return True

    def get_fail_form(self, errors):
        return ShopPaymentFailForm(initial={"errors": dict(errors)})

    def get_success_form(self, data):
        return ShopPaymentSuccessForm(initial=dict(data))

    def save_adding_statement(self, form):
        order = form.save(commit=False)
        order.user = self.shop.user
        order.profile = self.shop.user.profile
        order.amount = form.billing_information.get("amount_real", 0)
        order.commission = 0
        order.amount_with_commission = form.billing_information.get("amount_real", 0) # we don't need to use commission in this statement
        order.payment_system = self.ps
        if form.purse:
            order.purse_to = form.purse.username
        order.ps_amount = form.billing_information.get("amount")
        order.ps_currency = form.ps_currency
        order.shop = self.shop
        order.save()
        return order

    def payment_response(self, payment):
        try:
            payment.status
        except:
            return render_to_response(self.template_name_fail, {"form": self.get_fail_form(payment), "shop": self.shop}, RequestContext(self.request))
        else:
            return render_to_response(self.template_name_success, {"form": self.get_success_form({"payment": payment.id}), "shop": self.shop}, RequestContext(self.request))

    def form_valid(self, form):
        shop_information_not_error = self.check_shop_information(form.cleaned_data)
        if shop_information_not_error != True:
            return shop_information_not_error

        if self.request.session.has_key("shop_payment_form_data"):
            del self.request.session["shop_payment_form_data"]

        self.request.session["shop_payment_form_data"] = form.cleaned_data

        if self.ps.sys_code == "FSM":
            if self.request.user.is_anonymous():
                return HttpResponseRedirect("/login/?next=/shop/perform_payment/")
            else:
                payment = self.perform_payment(form.cleaned_data)
                return self.payment_response(payment)
        else:
            """
                we need to have commission before organize adding funds statement
                and adding funds statement not need commission
            """
            billing_information = self.billing_inf(form.cleaned_data)

            if self.shop.status == "t":
                payment = self.perform_payment(form.cleaned_data)
                return self.payment_response(payment)

            adding_data_form = {
                    "currency": self.currency.id,
                    "amount": billing_information.get("amount_with_commission"),
                    "ps_currency_type": form.cleaned_data.get("ps_currency_type"),
                    "phone": form.cleaned_data.get("phone").raw_input,
                    "spec_action": "adding",
                }

            if self.ps.sys_code == "QM":
                adding_data_form.update({"qm_pay_source": "qw"})


            adding_funds_form = AddingFundsCreateForm(
                ps_sys_code=form.cleaned_data.get("payment_system"),
                bank_id=None, user=self.shop.user,
                data=adding_data_form
            )
            if adding_funds_form.is_valid():
                adding_funds_statement = self.save_adding_statement(adding_funds_form)
                if self.ps.sys_code == "WM":
                    ps_form = AddingFundsWebmoneyForm(initial={
                        "LMI_PAYMENT_AMOUNT": adding_funds_statement.ps_amount,
                        "LMI_PAYEE_PURSE": adding_funds_form.purse.username,
                        "LMI_PAYMENT_NO": WebmoneyInvoice.objects.create(user=self.shop.user).payment_no,
                        "LMI_PAYMENT_DESC": adding_funds_form.purse.description,
                        "order_id": adding_funds_statement.id,
                    })
                elif self.ps.sys_code == "QM":
                    ps_form = AddingFundsQiwiForm(initial={
                        "user": form.cleaned_data.get("phone"),
                        "amount": adding_funds_statement.ps_amount,
                        "ccy":  QM_REAL_CURRENCY_CHOICE.get(form.cleaned_data.get("ps_currency_type", ""), "RUB"),
                        "comment": adding_funds_form.purse.description,
                        "lifetime": adding_funds_statement.date_create,
                        "prv_name": adding_funds_form.purse.description,
                        "pay_source": "qw",
                        "order_id": adding_funds_statement.id
                    })

                else:
                    return render_to_response(self.template_name_fail,
                                      {"form": self.get_fail_form({"payment_system": ["not developed", ]}), "shop": self.shop},
                                      RequestContext(self.request))
                # elif self.ps.sys_code == "YM":
                #     ps_form = AddingFundsYandexForm(initial={
                #         "code": self.request.GET.get("yandex_auth_code", ""),
                #         "order_id": adding_funds_statement.id,
                #     })


                return render_to_response(self.template_payment_system, {"ps_form": ps_form, "ps_form_url": adding_funds_form.purse.url}, RequestContext(self.request))
            else:
                return render_to_response(self.template_name_fail, {"form": self.get_fail_form(adding_funds_form.errors), "shop": self.shop}, RequestContext(self.request))
            #totdo сделать пополнение всех платежных систем
        # messages.info(self.request, _("Transfer was successful"))
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        return HttpResponse(simplejson.dumps({"errors": form.errors}), content_type="text/plain")

    def get(self, request, *args, **kwargs):
        if self.request.session.has_key("shop_payment_form_data"):
            shop_information_not_error = self.check_shop_information(self.request.session["shop_payment_form_data"])
            if self.request.session["shop_payment_form_data"].get("ps_payment_status") == "fail":
                return render_to_response(self.template_name_fail,
                                      {"form": self.get_fail_form({"payment_system": ["Payment is fail", ]}), "shop": self.shop},
                                      RequestContext(self.request))

            if shop_information_not_error != True:
                return shop_information_not_error

            payment = self.perform_payment(self.request.session["shop_payment_form_data"])
            return self.payment_response(payment)
        else:
            pass

        context = self.get_context_data(**kwargs)
        form_initial = {
            "shop_id": "9",
            "shop_password": "coolstore.com",
            "shop_order_id": "2",
            "shop_amount": "2.01",
            "shop_currency": "EUR",
            "payment_system": "FSM",
        }
        context.update({"form": ShopPaymentForm(initial=form_initial)})
        return render(self.request, self.get_template_names(), context)

    def post(self, request, *args, **kwargs):
        form = ShopPaymentForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ShopPerformPayment, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "shop",
        })
        return context


class ShopPaymentList(FormView):
    form_class = DateFilterForm
    model = ShopPayment
    template_name = "shop/payment/list/view.html"
    template_name_ajax = "shop/payment/list/_view.html"
    object_columns = [
        {
            "name": "id",
            "order_name": "id",
            "title": _("N"),
        },
        {
            "name": "shop",
            "order": "shop",
            "title": _("Shop"),
        },
        {
            "name": ["amount", "currency"],
            "order_name": "amount",
            "title": _("Amount"),
        },
        {
            "name": ["amount_with_commission", "currency"],
            "order_name": "amount_with_commission",
            "title": _("Amount with commission"),
        },
        {
            "name": "payment_system",
            "order_name": "payment_system",
            "title": _("Payment system"),
        },
        {
            "name": "get_status",
            "order_name": "status",
            "title": _("Status"),
        },
        {
            "name": "date_create",
            "order_name": "date_create",
            "title": _("Date create transfer"),
        },
    ]

    object_options = [
        {
            "name": "shop_payment_view",
            "title": _("View"),
        }
    ]

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        self.success_url = get_success_url(self.success_url)
        return super(ShopPaymentList, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_queryset(self, form=None):
        orders = get_orders(self.request, self.object_columns)
        qs = self.model.objects.select_related("sender", "recipient", "currency")
        qs = qs.filter(recipient=self.request.user).order_by(*orders)
        if form:
            if form.cleaned_data.get("date_from", None) != None and form.cleaned_data.get("date_to", None) != None:
                qs = qs.filter(date_create__range=[form.cleaned_data["date_from"].isoformat(), form.cleaned_data["date_to"].isoformat()])
        return qs

    def form_valid(self, form):
        self.object_list = self.get_queryset(form)
        if form.cleaned_data.get("spec_action", None) == "save_to_xls":
            return prepare_xls_for_download(self.object_list, self.object_columns, _("Shop payments"), "shop_payments")
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(object_list=self.object_list, form=self.form_class))

    def get_context_data(self, **kwargs):
        context = super(ShopPaymentList, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
            "object_columns": self.object_columns,
            "object_options": self.object_options,
        })
        return context


class ShopPaymentView(DetailView):
    model = ShopPayment
    template_name = "shop/payment/view/view.html"
    template_name_ajax = "shop/payment/view/_view.html"

    @method_decorator(login_required())
    @method_decorator(profile_required(redirect_to="/"))
    @method_decorator(transaction.commit_on_success)
    def dispatch(self, *args, **kwargs):
        if self.get_object().sender != self.request.user and self.get_object().recipient != self.request.user:
            messages.info(self.request, "Shop payment document doesn't belong to you")
            return HttpResponseRedirect(get_success_url("/shop/list/"))
        return super(ShopPaymentView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(ShopPaymentView, self).get_context_data(**kwargs)
        context.update({
            "main_menu_page": "account",
        })
        return context
