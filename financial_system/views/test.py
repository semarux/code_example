# -*- coding: utf-8 -*-
from django.template import loader, RequestContext
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from webmoney.forms import *
from webmoney.models import Invoice, Purse
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from torgplace import settings
from financial_system.models import *
import datetime
from torgplace.settings import MAIN_PAYMENT_SYSTEM
import django_settings



@login_required
def simple_payment(request):
    initial = {
        'LMI_PAYEE_PURSE': Purse.objects.all()[0],
        'LMI_PAYMENT_NO': Invoice.objects.create(user=request.user).payment_no,
        'LMI_PAYMENT_DESC': loader.render_to_string('test/webmoney/simple_payment_desc.txt',
                                                    RequestContext(request)).strip()[:255],
    }
    form = PaymentRequestForm(initial=initial)
    return render_to_response("test/webmoney/simple_payment.html", locals(), RequestContext(request))


@login_required
def yandex_payment(request):
    return render_to_response("test/yandex/page.html", locals(), RequestContext(request))


@login_required
def run_regular_payment(request):
    from django.contrib.sites.models import Site
    from financial_system.helpers.system_functs import get_obj_or_none
    from financial_system.models import PaymentSystem, RegularPayment, UserBalance
    from financial_system.helpers.amount import get_billing_information

    from financial_system.tasks import send_event_message, perform_regular_payment

    payment_system = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)
    regular_payments = RegularPayment.objects.filter(active=1, date_next_payment=datetime.datetime.today())
    current_site = Site.objects.get_current()
    operation_mode = get_operation_mode()
    for regular_payment in regular_payments:
        if not operation_mode:
            send_event_message.delay(
                user=regular_payment.sender,
                action_type="regular_payment",
                message_type="not_funds_by_this_currency",
                subject=loader.render_to_string("account/regular_payment/event_message/not_operation_mode_subject.txt", {}),
                content=loader.render_to_string("account/regular_payment/event_message/not_operation_mode.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
            )
        else:
            user_balance = get_obj_or_none(UserBalance, user=regular_payment.sender, currency=regular_payment.currency, payment_coupon=None)
            if not user_balance:
                send_event_message.delay(
                    user=regular_payment.sender,
                    action_type="regular_payment",
                    message_type="not_funds_by_this_currency",
                    subject=loader.render_to_string("account/regular_payment/event_message/not_funds_by_this_currency_subject.txt", {}),
                    content=loader.render_to_string("account/regular_payment/event_message/not_funds_by_this_currency_content.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
                )
            else:
                exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (regular_payment.currency.iso_code, regular_payment.currency.iso_code))
                billing_information = get_billing_information(regular_payment.amount, exchange_rate, exchange_rate, regular_payment.sender, payment_system, "regular_payment")
                if user_balance.state < billing_information.get("amount_with_commission", 0):
                    send_event_message.delay(
                        user=regular_payment.sender,
                        action_type="regular_payment",
                        message_type="insufficient_funds",
                        subject=loader.render_to_string("account/regular_payment/event_message/insufficient_funds_subject.txt", {}),
                        content=loader.render_to_string("account/regular_payment/event_message/insufficient_funds_content.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
                    )
                else:
                    perform_regular_payment.delay(regular_payment, user_balance, billing_information, operation_mode, payment_system)
                regular_payment.date_last_payment = datetime.datetime.today()
                regular_payment.save()
        pass
    return HttpResponse(True)

import urllib2
import json
from financial_system.helpers.amount import *
from django.db import transaction
@login_required
@transaction.commit_on_success
def set_exchange_rate(request):
    """
    Maybe we have to rewrite this code,
    it depends on the service receipt currency pairs
    """
    currency_list = Currency.objects.all()

    # exchange rate USD to other currencies
    usd_curr = Currency.objects.get(iso_code="USD")

    for currency in currency_list:

        exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % ("USD", currency.iso_code))
        if exchange_rate:
            exchange_rate.rate = currency.rate_with_positive_spred
            exchange_rate.save()
        else:
            exchange_rate = ExchangeRate(
                iso_code="%s%s" % ("USD", currency.iso_code),
                currency_from=usd_curr,
                currency_to=currency,
                rate=currency.rate_with_positive_spred
                )
            exchange_rate.save()

    #exchange rate currencies to USD
    for currency in currency_list:
        rate = Decimal(Decimal(1)/currency.rate_with_negative_spred)._rescale(-2, "ROUND_DOWN")
        exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency.iso_code, "USD"))
        if exchange_rate:
            exchange_rate.rate = rate
            exchange_rate.save()
        else:
            exchange_rate = ExchangeRate(
                iso_code="%s%s" % (currency.iso_code, "USD"),
                currency_from=currency,
                currency_to=usd_curr,
                rate=rate
                )
            exchange_rate.save()

    # exchange other currency pairs

    currency_list = currency_list.exclude(iso_code="USD")
    for currency_from in currency_list:
        for currency_to in currency_list:
            exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency_from.iso_code, currency_to.iso_code))
            if currency_to == currency_from:
                rate = 1
            else:
                rate = Decimal(currency_to.rate_with_positive_spred/currency_from.rate_with_negative_spred)._rescale(-2, "ROUND_DOWN")
            if exchange_rate:
                exchange_rate.rate = rate
                exchange_rate.save()
            else:
                exchange_rate = ExchangeRate(
                    iso_code="%s%s" % (currency_from.iso_code, currency_to.iso_code),
                    currency_from=currency_from,
                    currency_to=currency_to,
                    rate=rate
                    )
                exchange_rate.save()
    return HttpResponse(True)


def liqpay_payment(request):
    return render_to_response("test/liqpay/index.html", locals(), RequestContext(request))


@login_required
def test_save_post(request):
    initial = {
        'LMI_PAYEE_PURSE': Purse.objects.all()[0],
        'LMI_PAYMENT_NO': Invoice.objects.create(user=request.user).payment_no,
        'LMI_PAYMENT_DESC': loader.render_to_string('test/webmoney/simple_payment_desc.txt',
                                                    RequestContext(request)).strip()[:255],
    }
    form = PaymentRequestForm(initial=initial)
    if request.method == "POST":
        f = open(settings.MEDIA_ROOT+'/test_post.txt', "w+")
        for key in request.POST:
            f.write("%s = %s \n" % (key, request.POST[key]))
        f.close()
    return render_to_response("test/webmoney/test_save_post.html", locals(), RequestContext(request))


@csrf_exempt
def result(request):
    if request.method == "POST":
        f = open(settings.MEDIA_ROOT+'/test_post.txt', "w+")
        for key in request.POST:
            f.write("%s = %s \n" % (key, request.POST[key]))
        f.close()
        if int(request.POST.get('LMI_PREREQUEST', 0)) == 1:
            #3) Проверяем, не произошла ли подмена кошелька.
            #// Cравниваем наш настоящий кошелек с тем кошельком, который передан нам Мерчантом.
            #// Если кошельки не совпадают, то выводим ошибку и прерываем работу скрипта.
            purse = Purse.objects.all()[0]
            if request.POST.get('LMI_PAYEE_PURSE', "").strip() != purse.purse:
                return HttpResponse("ERR: НЕВЕРНЫЙ КОШЕЛЕК ПОЛУЧАТЕЛЯ " + request.POST.get('LMI_PAYEE_PURSE', ""))
            #Проверяем, указал ли пользователь свой email.
            #if not request.POST.get('email',"").strip() or request.POST.get('email').strip() == "":
            #    print "ERR: НЕ УКАЗАН EMAIL"
            #    exit()
            # Если ошибок не возникло и мы дошли до этого места, то выводим YES
            return HttpResponse("YES")
        else:
            return HttpResponse("ERR: Это не форма предвадительного запроса")
    else:
        return HttpResponse("ERR: Данные пришли не методом POST")


@csrf_exempt
def success(request):
    if request.method == 'POST':
        form = SettledPaymentForm(request.POST)
        if form.is_valid():
            id = form.cleaned_data['LMI_PAYMENT_NO']
            sys_invs_no = form.cleaned_data['LMI_SYS_INVS_NO']
            sys_trans_no = form.cleaned_data['LMI_SYS_TRANS_NO']
            date = form.cleaned_data['LMI_SYS_TRANS_DATE']
    return render_to_response("test/webmoney/success.html", locals(), RequestContext(request))

@csrf_exempt
def fail(request):
    if request.method == 'POST':
        form = UnSettledPaymentForm(request.POST)
        if form.is_valid():
            id = form.cleaned_data['LMI_PAYMENT_NO']
            sys_invs_no = form.cleaned_data['LMI_SYS_INVS_NO']
            sys_trans_no = form.cleaned_data['LMI_SYS_TRANS_NO']
            date = form.cleaned_data['LMI_SYS_TRANS_DATE']
    return render_to_response("test/webmoney/fail.html", locals(), RequestContext(request))


def check_post(request):
    pass


def complete_invoice(request):
    import datetime
    from financial_system.helpers.amount import invoice_complete_in_favor_owner
    if not django_settings.exists('INVOICE_TIME_LIMIT'):
        django_settings.set('Integer', 'INVOICE_TIME_LIMIT', 14)
    invoice_time_limit = django_settings.get('INVOICE_TIME_LIMIT')
    system_owner = get_obj_or_none(Profile, system_owner=1)

    invoices = Invoice.objects.filter(completed=False, date_pay__lte=datetime.datetime.today() - datetime.timedelta(days=1))
    for invoice in invoices:
        system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
        if system_owner_balance and system_owner_balance.state >= invoice.amount:
            invoice_complete_in_favor_owner(invoice, system_owner, system_owner_balance)
    return HttpResponse("Done")