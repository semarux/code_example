from selectable.base import ModelLookup
from selectable.registry import registry
from main.models import *


class RecipientsLookup(ModelLookup):
    model = User
    search_fields = ('profile__special_id__icontains', 'first_name__icontains', 'last_name__icontains', 'email__icontains', 'profile__company_name__icontains')

    def get_query(self, request, term):
        qs = super(RecipientsLookup, self).get_query(request, term).exclude(user=request.user).exclude(
            email=None).exclude(is_superuser=True).exclude(id=request.user.id)
        return qs

    def get_custom_item(self, item, company_name=None):
        if item.first_name or item.last_name or company_name:
            if company_name:
                return "%s - %s %s (%s)" % (item.profile.special_id, item.first_name, item.last_name, item.profile.company_name)
            else:
                return "%s - %s %s" % (item.profile.special_id, item.first_name, item.last_name)
        else:
            if item.email == "":
                return "%s" % item.profile.special_id
            else:
                return "%s - %s" % (item.profile.special_id, item.email)


    def get_item_text(self, item):
        try:
            item.profile.company_name
        except:
            return self.get_custom_item(item)
        else:
            return self.get_custom_item(item, item.profile.company_name)


    def get_item_value(self, item):
        return self.get_item_text(item)

    def get_item_label(self, item):
        return self.get_item_text(item)

registry.register(RecipientsLookup)