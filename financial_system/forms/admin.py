# --coding: utf-8
from django import forms
from django.forms.models import BaseInlineFormSet
from django.forms.util import ErrorList
from selectable.forms import *

from financial_system.models import *
from financial_system.helpers.system_functs import get_obj_or_none
from django.utils.translation import ugettext_lazy as _


class CommissionSettingAdminForm(forms.ModelForm):
    class Meta:
        model = CommissionSetting

    def clean_currency_commission(self):
        if self.cleaned_data.get("type_commission") == "M" and not self.cleaned_data.get("currency_commission", None):
            self._errors["currency_commission"] = ErrorList([_("This field is required")])
        return self.cleaned_data.get("currency_commission")


class CommissionSettingAddAdminForm(CommissionSettingAdminForm):
    def clean(self):
        try:
            CommissionSetting.objects.get(action_type=self.cleaned_data.get("action_type"), payment_coupon_kind=self.cleaned_data.get("payment_coupon_kind"), payment_system=self.cleaned_data.get("payment_system"))
        except:
            pass
        else:
            raise forms.ValidationError(_("Similar setting commissions already exists"))
        return self.cleaned_data


class OperatingModeAdminForm(forms.ModelForm):
    class Meta:
        model = OperatingMode

    def clean_active(self):
        if int(self.cleaned_data["active"]) == 1:
            operation_modes = OperatingMode.objects.filter(active=1)
            if operation_modes.count() == 1:
                if self.instance != operation_modes[0]:
                    raise forms.ValidationError(_("The system should have a single active mode"))
            else:
                if operation_modes.count() >= 1:
                    raise forms.ValidationError(_("The system should have a single active mode"))
        return self.cleaned_data["active"]


class CommissionRangeInlineFormSet(BaseInlineFormSet):
    """
    Generates an inline formset that is required
    """
    def _construct_form(self, i, **kwargs):
        """
        Override the method to change the form attribute empty_permitted
        """
        form = super(CommissionRangeInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form

    def check_commission(self, prev_max_amount, min_amount):
        if prev_max_amount and min_amount != prev_max_amount:
                return prev_max_amount
        return False

    def clean(self):
        super(CommissionRangeInlineFormSet, self).clean()
        prev_max_amount = None
        for i, form in enumerate(self.forms):
            if i == 0:
                if form.cleaned_data.get("min_amount") != 0.00:
                    form._errors["min_amount"] = ErrorList([_("Must been 0")])
                elif form.cleaned_data.get("max_amount") <= form.cleaned_data.get("min_amount"):
                    form._errors["max_amount"] = ErrorList(["%s < %s" % (form.base_fields.get("min_amount").label.encode(), form.base_fields.get("max_amount").label.encode())])
            else:
                if form.cleaned_data.get("max_amount") <= form.cleaned_data.get("min_amount"):
                    form._errors["max_amount"] = ErrorList(["%s <= %s" % (form.base_fields.get("min_amount").label.encode(), form.base_fields.get("max_amount").label.encode())])
                elif self.check_commission(prev_max_amount, form.cleaned_data.get("min_amount")):
                    form._errors["min_amount"] = ErrorList([_("%s must contain %s") % (form.base_fields.get("min_amount").label.encode(), self.check_commission(prev_max_amount, form.cleaned_data.get("min_amount")))])
            prev_max_amount = form.cleaned_data.get("max_amount")
        # return self.cleaned_data


class SystemOwnerBalanceAdminForm(forms.ModelForm):
    system_owner = get_obj_or_none(Profile, system_owner=1)
    currency = forms.ModelChoiceField(required=True, label=_("Currency"), empty_label=_("Choose currency"),
                                      queryset=Currency.objects.all().exclude(ps_special=1),
                                      widget=forms.Select())

    class Meta:
        model = UserBalance
        widgets = {
            "user": forms.widgets.HiddenInput(),
            "profile": forms.widgets.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(SystemOwnerBalanceAdminForm, self).__init__(*args, **kwargs)
        if self.system_owner:
            self.fields["user"].initial = self.system_owner.user.id
            self.fields["profile"].initial = self.system_owner.id


class WithdrawalAdminForm(forms.ModelForm):
    class Meta:
        model = Withdrawal

    def clean(self):
        user_balance = get_obj_or_none(UserBalance, user=self.instance.user, currency=self.instance.currency_to_withdrawal, payment_coupon=None)
        if user_balance:
            if user_balance.state < self.instance.amount_with_commission_to_withdrawal:
                self.errors["paid"] = ErrorList([_("You can not save the data because the user haven't enough money")])
        else:
            self.errors["paid"] = ErrorList([_("You can not save the data because the user does not balance")])

        if self.cleaned_data.get("purse_from", None) == "":
            self.errors["purse_from"] = ErrorList([_("This field must be required")])

        if self.cleaned_data.get("paid", "") == False and self.cleaned_data.get("status", "") == "paid":
            self.errors["status"] = ErrorList([_("Please, change status")])
        return self.cleaned_data

#
# class AddingFundsAdminForm(forms.ModelForm):
#     class Meta:
#         model = AddingFund
#
#
#     def clean(self):
#         self
#         # user_balance = get_obj_or_none(UserBalance, user=self.instance.user, currency=self.instance.currency_to_withdrawal, payment_coupon=None)
#         # if user_balance:
#         #     if user_balance.state < self.instance.amount_with_commission_to_withdrawal:
#         #         self.errors["paid"] = ErrorList([_("You can not save the data because the user haven't enough money")])
#         # else:
#         #     self.errors["paid"] = ErrorList([_("You can not save the data because the user does not balance")])
#         #
#         # if self.cleaned_data.get("purse_from", None) == "":
#         #     self.errors["purse_from"] = ErrorList([_("This field must be required")])
#         #
#         # if self.cleaned_data.get("paid", "") == False and self.cleaned_data.get("status", "") == "paid":
#         #     self.errors["status"] = ErrorList([_("Please, change status")])
#         return self.cleaned_data




class InvoiceDiscussionSendAdminForm(forms.ModelForm):
    comment = forms.CharField(required=True, widget=forms.Textarea(attrs={"placeholder": _("Your comment")}))
    invoice = None
    user = None

    def __init__(self, invoice=None, user=None, *args, **kwargs):
        super(InvoiceDiscussionSendAdminForm, self).__init__(*args, **kwargs)
        self.invoice = invoice
        self.user = user

    class Meta:
        model = InvoiceDiscussion
        fields = ["comment", ]


    def clean(self):
        if not self.invoice.moderator:
             self.errors["comment"] = ErrorList([_("Didn't appointed moderator")])
        else:
            if self.user != self.invoice.moderator:
                self.errors["comment"] = ErrorList([_("You aren't a moderator of discussion")])
        return self.cleaned_data