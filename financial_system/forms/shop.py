# --coding: utf-8
from financial_system.fields.account import *
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField


class ShopForm(forms.ModelForm):

    class Meta:
        model = Shop
        exclude = ["user", "profile", "secret_key"]
        widgets = {
            "status": forms.widgets.Select(attrs={"class": "custom-sel", "width": "295", "placeholder": _("Shop status")}),
            "shop_informer": forms.CheckboxInput(attrs={"class": "mic_check"}),
            "informer_language": forms.widgets.Select(
                attrs={"class": "custom-sel", "width": "295", "placeholder": _("Informer default language")}),
        }



class ShopCreateForm(ShopForm):
    pass

class ShopUpdateForm(ShopForm):
    pass


class ShopPaymentForm(forms.Form):
    """reorganize form"""
    shop_id = forms.IntegerField(required=True, widget=forms.TextInput(attrs={},))
    payment_signature = forms.CharField(required=True, widget=forms.TextInput(attrs={}))
    shop_order_id = forms.IntegerField(required=True, widget=forms.TextInput(attrs={}))
    shop_amount = forms.DecimalField(max_digits=9, decimal_places=2, label=_(u"Amount"),
                                validators=[MinValueValidator(Decimal("0.01"))])
    shop_currency = forms.ChoiceField(required=True, label=_("Currency"),
                                      choices=[(c.iso_code, c.name) for c in Currency.objects.filter().exclude(ps_special=1)],
                                      # widget=forms.Select(attrs={"class": "custom-sel all_width"})
    )
    payment_system = forms.ChoiceField(required=True, label=_("Payment system"),
                                       choices=[(ps.sys_code, ps.name) for ps in
                                               PaymentSystem.objects.filter(for_shop_payment=1, active=1)],
                                       # widget=forms.Select(attrs={"class": "custom-sel all_width"})
    )
    phone = PhoneNumberField(required=True, label=_("Phone"))

    ps_currency_type = forms.ChoiceField(required=False, label=_("Payment system currency type"), choices=[("YAR", "YAR", ), ] + WM_TYPE_PURSE_CHOICE + QM_TYPE_PURSE_CHOICE + LP_TYPE_CURRENCY_CHOICE)


class GetShopPaymentForm(ShopPaymentForm):
    pass




class ShopPaymentFailForm(forms.Form):
    errors = forms.CharField(widget=forms.Textarea())


class ShopPaymentSuccessForm(forms.Form):
    payment = forms.CharField(widget=forms.Textarea())



