# --coding: utf-8
from django import forms

from webmoney import PURSE_RE
from phonenumber_field.formfields import PhoneNumberField
from django.utils.translation import ugettext_lazy as _

class AddingFundsWebmoneyForm(forms.Form):
    LMI_PAYMENT_AMOUNT = forms.DecimalField(required=True, max_digits=7, decimal_places=2, label=_(u"Amount"), widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    LMI_PAYMENT_DESC = forms.CharField(label=_(u"Description"), widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    LMI_PAYMENT_NO = forms.IntegerField(label=_(u"Payment Number"), widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    LMI_PAYEE_PURSE = forms.RegexField(required=True, regex=PURSE_RE, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    LMI_SIM_MODE = forms.IntegerField(required=True, initial="0", widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    order_id = forms.IntegerField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))


class AddingFundsYandexForm(forms.Form):
    code = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    order_id = forms.IntegerField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))


class YandexAuthForm(forms.Form):
    client_id = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    response_type = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    redirect_uri = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    scope = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))


class AddingFundsLiqpayForm(forms.Form):
    public_key = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    amount = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    currency = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    description = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    order_id = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    type = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    signature = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    result_url = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    server_url = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))


class AddingFundsQiwiForm(forms.Form):
    user = PhoneNumberField(required=True, label=_("Phone"))
    amount = forms.DecimalField(required=True, max_digits=11, decimal_places=2, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    ccy = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    comment = forms.CharField(widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    lifetime = forms.DateTimeField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    pay_source = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("mobile", _("Mobile")), ("qw", _("QIWI"))], initial="qw")
    prv_name = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))
    order_id = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}))