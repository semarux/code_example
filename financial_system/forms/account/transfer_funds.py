# --coding: utf-8
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _

from financial_system.lookups import RecipientsLookup
from financial_system.fields.account import *
from financial_system.helpers.amount import get_billing_information, get_operation_mode
from torgplace.settings import MAIN_PAYMENT_SYSTEM


class TrunsferFundsCreateForm(forms.ModelForm):
    recipient = AutoCompleteSelectField(required=True, lookup_class=RecipientsLookup,
                                        widget=AutoComboboxSelectWidget(lookup_class=RecipientsLookup,
                                        attrs={"placeholder": _("Choose Profile"), "class": "dj-select-section recipient"}), label=_("Recipient"))
    amount_recipient = forms.DecimalField(max_digits=9, decimal_places=2, label=_(u"Amount"), validators=[MinValueValidator(Decimal("0.01"))])

    # "hidden": "hidden"
    # main_amount = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={}),
    #                                 choices=[("currency_own", "In your own currency"),
    #                                          ("currency_recipient", "In currency recipient")],
    #                                 initial="currency_own")
    spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}),
                                    choices=[("estimate", "Estimate"), ("transfer", "Transfer")], initial="estimate")
    user = None
    sender_balance = None
    exchange_rate = None
    opposite_exchange_rate = None
    billing_information = None
    ps = None

    class Meta:
        model = TransferFund
        exclude = ["sender", "commission", "amount_with_commission"]

    def __init__(self, user=None, recipient=None, *args, **kwargs):
        super(TrunsferFundsCreateForm, self).__init__(*args, **kwargs)
        self.user = user
        self.fields["currency"] = OperationModeCurrencyField(user=user, use_user_currency=True)
        self.fields.keyOrder = ["recipient", "amount", "currency", "comment", "spec_action"]
        self.ps = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)

        if recipient:
            self.fields["recipient"].initial = recipient

    def clean_currency(self):
        if not get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("currency"), payment_coupon=None):
            raise forms.ValidationError(_("You have no money in the currency"))
        return self.cleaned_data.get("currency")

    def clean(self):
        if not self.ps:
            self._errors["comment"] = ErrorList([_("The payment system is not ready for use")])

        if self.cleaned_data.get("currency"):
            self.opposite_exchange_rate = self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["currency"].iso_code, self.cleaned_data["currency"].iso_code))
            if get_operation_mode() and get_operation_mode().code == "PUC":
                if self.cleaned_data.get("recipient"):
                    self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["currency"].iso_code, self.cleaned_data.get("recipient").profile.currency.iso_code))
                    self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data.get("recipient").profile.currency.iso_code, self.cleaned_data["currency"].iso_code))


            if not self.exchange_rate or not self.opposite_exchange_rate:
                self._errors["currency"] = ErrorList([_("This exchange rate not working in system")])

            self.sender_balance = get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("currency"), payment_coupon=None)
            if self.sender_balance:
                self.billing_information = get_billing_information(self.cleaned_data.get("amount", Decimal(0)), self.exchange_rate, self.opposite_exchange_rate, self.user, self.ps, "transfer_funds")
                if self.sender_balance.state < self.cleaned_data.get("amount", 0):
                    self._errors["amount"] = ErrorList([_("You haven't enough money in this currency.")])
                elif self.sender_balance.state < self.billing_information["amount_with_commission"]:
                    self._errors["amount"] = ErrorList([_("You haven't enough money in this currency. You must pay %s and %s" % (self.cleaned_data.get("amount"), self.billing_information["commission_real"]))])
            else:
                self._errors["amount"] = ErrorList([_("You haven't enough money in this currency.")])
        return self.cleaned_data
        #     try:
        #         self.fields["currency_recipient"] = forms.ModelChoiceField(required=True, label=_("Currency recipient"), empty_label=_("Choose currency"),
        #                                   queryset=Currency.objects.all().exclude(ps_special=1),
        #                                   initial=recipient.profile.currency,)
        #         self.fields.keyOrder.insert(4, "currency_recipient")
        #
        #     except:
        #         pass

            # self.fields.keyOrder.extend(["currency_recipient", ])
        # self.fields.keyOrder.extend([])