# --coding: utf-8
from decimal import *
from datetime import datetime
from django import forms
from captcha.fields import CaptchaField
from django.forms.util import ErrorList
from django.core.validators import MinValueValidator
from phonenumber_field.formfields import PhoneNumberField

from financial_system.helpers.amount import get_convert_amount
from financial_system.lookups import RecipientsLookup
from financial_system.fields.account import *
from django.utils.translation import ugettext_lazy as _
from financial_system.helpers.amount import get_billing_information, get_exchange_rate, get_operation_mode
from torgplace.settings import MAIN_PAYMENT_SYSTEM


class DateFilterForm(forms.Form):
    date_from = forms.DateField(required=False, label=_("Date from"), input_formats=["%Y-%m-%d"], widget=forms.TextInput(attrs={"class": "calendar"}))
    date_to = forms.DateField(required=False, label=_("Date to"), input_formats=["%Y-%m-%d"], widget=forms.TextInput(attrs={"class": "calendar"}))
    spec_action = forms.ChoiceField(required=False, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("filter", "Filter"), ("save_to_xls", "Save to xls")])

    def clean_date_to(self):
        date_from = self.cleaned_data.get("date_from")
        date_to = self.cleaned_data.get("date_to")
        if date_from and date_to and date_to < date_from:
            raise forms.ValidationError(_("Enter the date more or equal than Start date"))
        return date_to


class ReplenishAccountForm(forms.Form):
    captcha = CaptchaField(label=_("Captcha"), help_text=_("Enter the text from the image"))