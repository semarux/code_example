# --coding: utf-8
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _
from financial_system.helpers.amount import get_convert_amount
from financial_system.fields.account import *
from financial_system.helpers.amount import get_billing_information, get_exchange_rate, get_operation_mode
from phonenumber_field.formfields import PhoneNumberField
#
# class AddingFundCreateForm(forms.ModelForm):
#     spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}),
#                                     choices=[("estimate", "Estimate"), ("transfer", "Transfer")], initial="estimate")
#     class Meta:
#         model = AddingFund
#         exclude = ("user", "profile", "transaction", "commission", "amount_with_commission", "payment_system", "purse_from", "purse_to", "ps_amount", "ps_currency", "status", "paid", "date_create")
#         widgets = {
#             "amount": forms.widgets.HiddenInput(attrs={"id": "id_order_amount", "hidden": "hidden"}),
#             "currency": forms.widgets.HiddenInput(attrs={"id": "id_order_currency", "hidden": "hidden"}),
#         }
#     purse = None
#     ps_currency = None
#     exchange_rate = None
#     opposite_exchange_rate = None
#     payment_system_sys_code = "WM"
#
#
#     def __init__(self, user=None, ps_sys_code="WM", *args, **kwargs):
#         super(AddingFundCreateForm, self).__init__(*args, **kwargs)
#         self.payment_system_sys_code = ps_sys_code
#         self.fields["ps_currency_type"] = PsCurrencyField(ps_sys_code=ps_sys_code, widget=forms.widgets.HiddenInput(attrs={"id": "id_order_ps_currency_type", "hidden": "hidden"}))
#         if ps_sys_code == "QM":
#             self.fields["phone"] = PhoneNumberField(required=True, label=_("Your phone number"))
#             self.fields["qm_pay_source"] = forms.CharField(required=True, widget=forms.HiddenInput(attrs={"hidden": "hidden"}), initial="qw")
#             if user:
#                 self.fields["phone"].initial = user.profile.phone
#
#     def clean(self):
#         super(AddingFundCreateForm, self).clean()
#         self.ps_currency = self.fields["ps_currency_type"].ps_currency
#         self.purse = self.fields["ps_currency_type"].purse
#         if self.cleaned_data["currency"] and self.ps_currency:
#             self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["currency"].iso_code, self.ps_currency.iso_code))
#             self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.ps_currency.iso_code, self.cleaned_data["currency"].iso_code))
#             if not self.exchange_rate or not self.opposite_exchange_rate:
#                 self._errors["ps_currency_type"] = ErrorList([_("This exchange rate not working in system")])
#         return self.cleaned_data





class AddingFundsCreateForm(forms.ModelForm):
    amount = forms.DecimalField(max_digits=9, decimal_places=2, label=_(u"Amount"),
                                validators=[MinValueValidator(Decimal("0.01"))])
    spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}),
                                    choices=[("estimate", "Estimate"), ("adding", "Adding")], initial="estimate")

    payment_system_sys_code = "WM"
    bank = None
    ps = None
    purse = None
    ps_currency = None
    exchange_rate = None
    opposite_exchange_rate = None
    billing_information = None
    user = None

    class Meta:
        model = AddingFund
        exclude = (
        "user", "profile", "transaction", "commission", "amount_with_commission", "payment_system", "purse_from",
        "purse_to", "ps_amount", "ps_currency", "status", "shop", "paid", "date_create", "date_pay", "bank")
        widgets = {
            "amount": forms.widgets.HiddenInput(attrs={"id": "id_order_amount", "hidden": "hidden"}),
            "currency": forms.widgets.HiddenInput(attrs={"id": "id_order_currency", "hidden": "hidden"}),
        }

    def __init__(self, user=None, bank_id=None, ps_sys_code="WM", *args, **kwargs):
        super(AddingFundsCreateForm, self).__init__(*args, **kwargs)
        self.user = user
        self.payment_system_sys_code = ps_sys_code
        self.ps = get_obj_or_none(PaymentSystem, sys_code=ps_sys_code, active=True)
        self.bank = get_obj_or_none(Bank, id=bank_id)
        self.fields["currency"] = OperationModeCurrencyField(user=user)
        if ps_sys_code == "BT":
            self.fields["bank"] = forms.ChoiceField(required=True, choices=[("", _("Choice bank"))] + [(c.id, c.name) for c in Bank.objects.filter(active=1)],
                                                    widget=forms.Select(attrs={"class": "custom-sel all_width"}),
                                                    label=_("Bank"))
            if bank_id:
                self.fields["ps_currency_type"] = BankCurrencyField(bank=self.bank)
            else:
                pass
                # self.fields["ps_currency_type"] = PsCurrencyField(ps_sys_code=ps_sys_code)
        else:
            self.fields["ps_currency_type"] = PsCurrencyField(ps_sys_code=ps_sys_code)
        if ps_sys_code == "QM":
            self.fields["phone"] = PhoneNumberField(required=True, label=_("Your phone number"))
            self.fields["qm_pay_source"] = forms.CharField(required=True,
                                                           widget=forms.HiddenInput(attrs={"hidden": "hidden"}),
                                                           initial="qw")
            if user:
                self.fields["phone"].initial = user.profile.phone


    def clean(self):
        super(AddingFundsCreateForm, self).clean()
        getcontext().prec = 2

        self.ps_currency = self.fields["ps_currency_type"].ps_currency
        if self.ps and self.ps.sys_code != "BT":

            self.purse = self.fields["ps_currency_type"].purse

        if not self.purse and self.ps.sys_code != "BT":
            self._errors["ps_currency_type"] = ErrorList([_("This purse do not work in system")])
        else:
            if self.cleaned_data.get("currency"):
                if self.cleaned_data["currency"] and self.ps_currency:
                    self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (
                    self.cleaned_data["currency"].iso_code, self.ps_currency.iso_code))
                    self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (
                    self.ps_currency.iso_code, self.cleaned_data["currency"].iso_code))
                    if not self.exchange_rate or not self.opposite_exchange_rate:
                        self._errors["ps_currency_type"] = ErrorList([_("This exchange rate not working in system")])

                    if not self.ps:
                        self._errors["ps_currency_type"] = ErrorList([_("This payment system is not available")])
                    else:
                        self.billing_information = get_billing_information(self.cleaned_data.get("amount"),
                                                                           self.exchange_rate,
                                                                           self.opposite_exchange_rate, self.user,
                                                                           self.ps, "adding_funds")
                        try:
                            min_amount = get_convert_amount(self.ps.adding_funds_min, self.exchange_rate)
                            max_amount = get_convert_amount(self.ps.adding_funds_max, self.exchange_rate)
                            if float(self.cleaned_data.get("amount")._rescale(-2, "ROUND_UP")) < float(
                                    min_amount._rescale(-2, "ROUND_UP")):
                                self._errors["amount"] = ErrorList([_(
                                    "For this payment system too low amount. The minimum value for this payment system is %s" % round(
                                        min_amount._rescale(-2, "ROUND_UP"), 2))])
                            elif float(self.cleaned_data.get("amount")._rescale(-2, "ROUND_UP")) > float(
                                    max_amount._rescale(-2, "ROUND_UP")):
                                self._errors["amount"] = ErrorList([_(
                                    "For this payment system too low amount. The max value for this payment system is %s" % round(
                                        max_amount._rescale(-2, "ROUND_UP"), 2))])
                        except:
                            pass
                else:
                    self._errors["ps_currency_type"] = ErrorList([_("Select a currency")])
            else:
                pass
        return self.cleaned_data