# --coding: utf-8
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _

from financial_system.helpers.amount import get_convert_amount
from financial_system.fields.account import *
from financial_system.helpers.amount import get_billing_information, get_exchange_rate


class WithdrawalFundsForm(forms.ModelForm):
    payment_system = forms.ModelChoiceField(required=True, label=_("Payment system"), empty_label=_("Choose a payment system"), queryset=PaymentSystem.objects.filter(for_withdrawal_funds=1, active=1),
                                      widget=forms.Select(attrs={"class": "custom-sel all_width"}))

    main_amount = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("obtain", "Obtain"), ("withdrawal", "Withdrawal")], initial="obtain")
    spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("estimate", "Estimate"), ("withdrawal", "Withdrawal")], initial="estimate")
    user = None

    exchange_rate = None
    opposite_exchange_rate = None
    billing_information_withdrawal = None
    billing_information_obtain = None

    ps = None


    class Meta:
        model = Withdrawal
        # fields = ("payment_system", "purse", "comment", "amount_to_withdrawal", "currency_to_withdrawal", "amount_to_obtain", "currency_to_obtain")
        exclude = ("user", "profile")

    def __init__(self, user=None, payment_system=None, *args, **kwargs):
        super(WithdrawalFundsForm, self).__init__(*args, **kwargs)
        self.ps = payment_system
        self.user = user
        self.fields["amount_to_withdrawal"].required = False
        self.fields["amount_to_obtain"].required = False
        self.fields["currency_to_withdrawal"] = OperationModeCurrencyField(user=user)
        if payment_system:
            self.fields["payment_system"].initial = payment_system
            self.fields["currency_to_obtain"] = PsCurrencyField(user=user, ps_sys_code=payment_system.sys_code)
        else:
            self.fields["currency_to_obtain"] = PsCurrencyField(user=user)
        self.fields.keyOrder = ["payment_system", "purse", "comment", "amount_to_withdrawal", "currency_to_withdrawal", "currency_to_obtain", "amount_to_obtain"]

        if payment_system and payment_system.sys_code == "BT":
            for bank_field in BANK_REQUISITES:
                self.fields[bank_field[0]] = forms.CharField(required=True, label=bank_field[1], widget=forms.TextInput())

        self.fields.keyOrder.extend(["main_amount", "spec_action"])

    def clean_currency_to_withdrawal(self):
        if not get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("currency_to_withdrawal"), payment_coupon=None):
            raise forms.ValidationError(_("You have no money in the currency"))
        return self.cleaned_data.get("currency_to_withdrawal")

    def clean_payment_system(self):
        payment_system = self.cleaned_data.get("payment_system")
        self.fields["currency_to_obtain"] = PsCurrencyField(user=self.user, ps_sys_code=payment_system.sys_code)
        return self.cleaned_data.get("payment_system")

    def clean(self):
        """
            we nead control user enough money in clean_function
            becouse clean_amount_to_withdrawal validate before currency_to_withdrawal
            end if we validate amount_to withdrawal we not get currency_to_withdrawal
        """
        """ check one's required fields"""
        if self.cleaned_data.get("amount_to_withdrawal", "") == "" and self.cleaned_data.get("amount_to_withdrawal", "") == "":
            self._errors["amount_to_withdrawal"] = ErrorList([_("This field must be required")])

        user_balance = get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("currency_to_withdrawal"), payment_coupon=None)


        """ check user balanse for amount_to_withdrawal"""
        if user_balance:
            if user_balance.state < self.cleaned_data.get("amount_to_withdrawal"):
                self._errors["amount_to_withdrawal"] = ErrorList([_("You haven't enough money for the operation")])

        """ check currency_to_withdrawal and currency_to_obtain"""
        if self.cleaned_data.get("currency_to_withdrawal") and self.cleaned_data.get("currency_to_obtain"):

            if self.cleaned_data.get("currency_to_withdrawal"):
                self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["currency_to_withdrawal"].iso_code, self.cleaned_data["currency_to_obtain"].iso_code))

            if self.cleaned_data.get("currency_to_obtain"):
                self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["currency_to_obtain"].iso_code, self.cleaned_data["currency_to_withdrawal"].iso_code))

            """ check exchange rate for currency to obtain """
            if not self.exchange_rate or not self.opposite_exchange_rate:
                self._errors["currency_to_obtain"] = ErrorList([_("This exchange rate not working in system")])
            else:
                if self.cleaned_data.get("amount_to_withdrawal") and self.cleaned_data.get("currency_to_withdrawal"):
                    self.billing_information_withdrawal = get_billing_information(self.cleaned_data.get("amount_to_withdrawal"), self.exchange_rate, self.opposite_exchange_rate, self.user, self.cleaned_data["payment_system"], "withdrawal")
                    ps_exchange_rate_to_witdrawal = get_exchange_rate(self.cleaned_data["currency_to_withdrawal"], self.ps.withdrawals_currency)

                    """ check exchange rate for currency to withdrawal """
                    if not ps_exchange_rate_to_witdrawal:
                        self._errors["currency_to_withdrawal"] = ErrorList([_("This exchange rate not working in system (%s%s)" % (self.cleaned_data["currency_to_withdrawal"].iso_code, self.ps.withdrawals_currency.iso_code))])
                    else:
                        """ check min and max amount to withdrawal """
                        min_amount = get_convert_amount(self.ps.withdrawals_min, ps_exchange_rate_to_witdrawal)
                        max_amount = get_convert_amount(self.ps.withdrawals_max, ps_exchange_rate_to_witdrawal)
                        if float(self.cleaned_data.get("amount_to_withdrawal")._rescale(-2, "ROUND_UP")) < float(min_amount._rescale(-2, "ROUND_UP")):
                            self._errors["amount_to_withdrawal"] = ErrorList([_("For this payment system too low amount. The minimum value for this payment system is %s" % round(min_amount._rescale(-2, "ROUND_UP"), 2))])
                        elif float(self.cleaned_data.get("amount_to_withdrawal")._rescale(-2, "ROUND_UP")) > float(max_amount._rescale(-2, "ROUND_UP")):
                            self._errors["amount_to_withdrawal"] = ErrorList([_("For this payment system too low amount. The max value for this payment system is %s" % round(max_amount._rescale(-2, "ROUND_UP"), 2))])

                if self.cleaned_data.get("amount_to_obtain") and self.cleaned_data.get("currency_to_obtain"):
                    # and self.cleaned_data.get("main_amount") == "obtain"
                    billing_information_obtain_prepare = get_billing_information(self.cleaned_data.get("amount_to_obtain"), self.opposite_exchange_rate, self.exchange_rate, self.user, self.cleaned_data["payment_system"], "withdrawal")

                    self.billing_information_obtain = get_billing_information(self.cleaned_data.get("amount_to_obtain") + billing_information_obtain_prepare['commission'], self.opposite_exchange_rate, self.exchange_rate, self.user, self.cleaned_data["payment_system"], "withdrawal")

                    if user_balance.state < self.billing_information_obtain["payment_amount_real"]:
                        self._errors["amount_to_obtain"] = ErrorList([_("You haven't enough money for the operation")])
                    ps_exchange_rate_to_obtain = get_exchange_rate(self.cleaned_data["currency_to_obtain"], self.ps.withdrawals_currency)
                    """ check exchange rate for currency to obtain """
                    if not ps_exchange_rate_to_obtain:
                        self._errors["currency_to_obtain"] = ErrorList([_("This exchange rate not working in system (%s%s)" % (self.cleaned_data["currency_to_obtain"].iso_code, self.ps.withdrawals_currency.iso_code))])
                    else:
                        """ check min and max amount to obtain """
                        min_amount = get_convert_amount(self.ps.withdrawals_min, ps_exchange_rate_to_obtain)
                        max_amount = get_convert_amount(self.ps.withdrawals_max, ps_exchange_rate_to_obtain)
                        if float(self.cleaned_data.get("amount_to_obtain")._rescale(-2, "ROUND_UP")) < float(min_amount._rescale(-2, "ROUND_UP")):
                            self._errors["amount_to_obtain"] = ErrorList([_("For this payment system too low amount. The minimum value for this payment system is %s" % round(min_amount._rescale(-2, "ROUND_UP"), 2))])
                        elif float(self.cleaned_data.get("amount_to_obtain")._rescale(-2, "ROUND_UP")) > float(max_amount._rescale(-2, "ROUND_UP")):
                            self._errors["amount_to_obtain"] = ErrorList([_("For this payment system too low amount. The max value for this payment system is %s" % round(max_amount._rescale(-2, "ROUND_UP"), 2))])

                if self.cleaned_data.get("spec_action") == "withdrawal":
                    if self.cleaned_data.get("main_amount") == "withdrawal":
                        if (self.billing_information_withdrawal["amount"] - self.billing_information_withdrawal["commission_real"]) != self.cleaned_data["amount_to_obtain"]:
                            self._errors["amount_to_withdrawal"] = ErrorList([_("Withdrawal amount and the obtain amount is not consistent")])
                    elif self.cleaned_data.get("main_amount") == "obtain":
                        if self.billing_information_obtain["amount"] != self.cleaned_data["amount_to_withdrawal"]:
                            self._errors["amount_to_obtain"] = ErrorList([_("Withdrawal amount and the obtain amount is not consistent")])

        return self.cleaned_data

