# --coding: utf-8
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _
from torgplace.settings import MAIN_PAYMENT_SYSTEM
from financial_system.fields.account import *
from financial_system.models import ConversionFund
from financial_system.helpers.amount import get_billing_information, get_operation_mode


class ConversionFundsForm(forms.ModelForm):

    credit_currency = forms.ModelChoiceField(required=True, label=_("Currency from"), empty_label=_("Select currency"),
                                            queryset=Currency.objects.all().exclude(ps_special=1),
                                            widget=forms.Select(attrs={"class": "custom-sel all_width"}))

    debit_currency = forms.ModelChoiceField(required=True, label=_("Currency to"), empty_label=_("Select currency"),
                                            queryset=Currency.objects.all().exclude(ps_special=1),
                                            widget=forms.Select(attrs={"class": "custom-sel all_width"}))


    credit_amount = forms.DecimalField(required=True, max_digits=9, decimal_places=2, label=_(u"Amount"), validators=[MinValueValidator(Decimal("0.01"))])
    spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("estimate", "Estimate"), ("convert", "Convert")], initial="estimate")
    billing_information = None
    user = None
    ps = None
    exchange_rate = None
    opposite_exchange_rate = None
    system_owner = None


    class Meta:
        model = ConversionFund
        exclude = ["user", "credit_commission", "credit_amount_with_commission", "debit_amount", "exchange_rate", "value_exchange_rate"]

    def __init__(self, user=None, credit_currency_id="", *args, **kwargs):
        self.user = user
        self.ps = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)
        super(ConversionFundsForm, self).__init__(*args, **kwargs)
        if user:
            self.fields["credit_currency"].queryset = Currency.objects.filter(id__in=[ub.currency.id for ub in user.userbalance_set.all() if ub.state > 0])

        if credit_currency_id != "":
            self.fields["debit_currency"].queryset = Currency.objects.all().exclude(ps_special=1).exclude(id=credit_currency_id)
        self.fields.keyOrder = ["credit_currency", "debit_currency", "credit_amount", "spec_action"]


    # def clean_debit_currency(self):
    #     if not get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("debit_currency"), payment_coupon=None):
    #         raise forms.ValidationError(_("You have no money in the currency"))
    #     return self.cleaned_data.get("debit_currency")
    #
    # def clean_amount(self):
    #     user_balance = get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("debit_currency"), payment_coupon=None)
    #
    #     if user_balance:
    #         if user_balance.state < self.cleaned_data.get("amount"):
    #             raise forms.ValidationError(_("You  haven't enough money for the operation."))
    #     return self.cleaned_data.get("amount")
    #

    def clean(self):
        self.system_owner = get_obj_or_none(Profile, system_owner=1)
        if self.cleaned_data.get("credit_currency") and self.cleaned_data.get("debit_currency") and self.cleaned_data.get("credit_amount"):
            self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["credit_currency"].iso_code, self.cleaned_data["debit_currency"].iso_code))
            self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["debit_currency"].iso_code, self.cleaned_data["credit_currency"].iso_code))

            if not self.exchange_rate or not self.opposite_exchange_rate:
                self._errors["debit_currency"] = ErrorList([_("This exchange rate not working in system")])
            else:
                self.billing_information = get_billing_information(self.cleaned_data.get("credit_amount", Decimal(0)), self.exchange_rate, self.opposite_exchange_rate, self.user, self.ps, "exchange_funds")
                """ check the availability of money in user """
                user_balance = get_obj_or_none(UserBalance, user=self.user, currency=self.cleaned_data.get("credit_currency"), payment_coupon=None)
                if user_balance:
                    if user_balance.state < self.billing_information.get("amount_with_commission"):
                        self._errors["credit_amount"] = ErrorList([_(
                            "You haven't enough money for the operation.You must pay %(credit_amount)s %(credit_currency)s and commission %(commission_real)s %(credit_currency)s " % {
                                "credit_amount": self.cleaned_data.get("credit_amount"),
                                "commission_real": self.billing_information["commission_real"],
                                "credit_currency": self.cleaned_data.get("credit_currency"),})])
                else:
                    self._errors["credit_currency"] = ErrorList([_("You have no money in the currency")])

                """ check the availability of money in the system """

                if not self.system_owner:
                    self._errors["credit_amount"] = ErrorList([_("The owner of the system has not been created.")])
                else:
                    owner_balance = get_obj_or_none(UserBalance, user=self.system_owner.user, currency=self.cleaned_data.get("debit_currency"), payment_coupon=None)
                    if owner_balance:
                        if owner_balance.state < self.billing_information.get("amount"):
                            self._errors["credit_amount"] = ErrorList([_("The system has not enough money in this currency.")])
                    else:
                        self._errors["debit_currency"] = ErrorList([_("The system has not enough money in this currency.")])
        return self.cleaned_data