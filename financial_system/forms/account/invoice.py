# --coding: utf-8
from decimal import *
from datetime import datetime
from django import forms
from captcha.fields import CaptchaField
from django.forms.util import ErrorList
from django.core.validators import MinValueValidator
from phonenumber_field.formfields import PhoneNumberField


from financial_system.helpers.amount import get_convert_amount
from financial_system.lookups import RecipientsLookup
from financial_system.fields.account import *
from django.utils.translation import ugettext_lazy as _
from financial_system.helpers.amount import get_billing_information, get_exchange_rate, get_operation_mode
from financial_system.helpers.system_functs import get_obj_or_none
from torgplace.settings import MAIN_PAYMENT_SYSTEM

class InvoiceCreateForm(forms.ModelForm):
    payer = AutoCompleteSelectField(required=True, lookup_class=RecipientsLookup,
                                        widget=AutoComboboxSelectWidget(lookup_class=RecipientsLookup,
                                        attrs={"placeholder": _("Choose Profile"), "class": "dj-select-section recipient"}), label=_("Payer"))
    amount = forms.DecimalField(max_digits=9, decimal_places=2, label=_(u"Amount"), validators=[MinValueValidator(Decimal("0.01"))])
    
    user = None

    class Meta:
        model = Invoice
        fields = ["payer", "amount", "currency", "short_description", "comment"]

    def __init__(self, user=None, *args, **kwargs):
        super(InvoiceCreateForm, self).__init__(*args, **kwargs)
        self.user = user
        self.fields["currency"] = OperationModeCurrencyField(user=user)
        self.fields.keyOrder = ["payer", "amount", "currency", "short_description", "comment"]


class InvoicePayForm(forms.ModelForm):
    spec_action = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={"hidden": "hidden"}), choices=[("estimate", _("Estimate")), ("pay", _("Pay"))], initial="estimate")
    ps = None
    payer_balance = None
    billing_information = None

    class Meta:
        model = Invoice
        fields = ["spec_action",]

    def __init__(self, *args, **kwargs):
        super(InvoicePayForm, self).__init__(*args, **kwargs)
        self.ps = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)
        if self.instance.content_object:
            self.fields["payment_currency"] = forms.ModelChoiceField(required=True, initial=self.instance.content_object.currency.id,
                                                                     queryset=Currency.objects.all().exclude(
                                                                         ps_special=1),
                                                                     widget=forms.HiddenInput(attrs={}),
                                                                     help_text=self.instance.content_object.currency
            )
        else:
            self.fields["payment_currency"] = OperationModeCurrencyField(user=self.instance.payer, use_user_currency=True, label=_("Payment currency"))

    def clean_payment_currency(self):
        if not get_obj_or_none(UserBalance, user=self.instance.payer, currency=self.cleaned_data.get("payment_currency"), payment_coupon=None):
            raise forms.ValidationError(_("You have no money in the currency"))
        return self.cleaned_data.get("payment_currency")

    def clean(self):
        if self.instance.completed:
            self._errors["payment_currency"] = ErrorList([_("You can't pay the invoice, because it is completed")])

        if not self.ps:
            self._errors["payment_currency"] = ErrorList([_("The payment system is not ready for use")])

        if self.cleaned_data.get("payment_currency"):
            self.exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.instance.currency.iso_code, self.cleaned_data["payment_currency"].iso_code))
            self.opposite_exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (self.cleaned_data["payment_currency"].iso_code, self.instance.currency.iso_code))
            if not self.exchange_rate or not self.opposite_exchange_rate:
                self._errors["payment_currency"] = ErrorList([_("This exchange rate not working in system")])
            self.payer_balance = get_obj_or_none(UserBalance, user=self.instance.payer, currency=self.cleaned_data.get("payment_currency"), payment_coupon=None)
            if self.payer_balance:
                self.billing_information = get_billing_information(self.instance.amount, self.exchange_rate, self.opposite_exchange_rate, self.instance.payer, self.ps, "foot_invoice")
                if self.payer_balance.state < self.billing_information["payment_amount_real"]:
                    self._errors["payment_currency"] = ErrorList([_("You haven't enough money in this currency. You must pay %s" % self.billing_information["payment_amount_real"])])
            else:
                self._errors["payment_currency"] = ErrorList([_("You haven't enough money in this currency.")])
        return self.cleaned_data


class InvoiceDiscussionSendForm(forms.ModelForm):
    comment = forms.CharField(required=True, widget=forms.Textarea(attrs={"placeholder": _("Your comment")}))

    class Meta:
        model = InvoiceDiscussion
        fields = ["comment", ]


