# --coding: utf-8
from datetime import datetime

from django.utils.translation import ugettext_lazy as _

from financial_system.lookups import RecipientsLookup
from financial_system.fields.account import *


class RegularPaymentForm(forms.ModelForm):
    type_periodicity = forms.ChoiceField(widget=forms.Select(attrs={"class": "custom-sel all_width"},), choices=TYPE_PERIODICITY_CHOICE)
    currency = forms.ModelChoiceField(required=True, label=_("Currency"), empty_label=_("Choose currency"),
                                      queryset=Currency.objects.all().exclude(ps_special=1),
                                      widget=forms.Select(attrs={"class": "custom-sel all_width", }),
    )
    # "%Y-%m-%d %H:%M:%S"
    date_start_payment = forms.DateField(required=True, label=_("Date from"), input_formats=["%Y-%m-%d"], widget=forms.TextInput(attrs={"class": "calendar"}))
    recipient = AutoCompleteSelectField(required=True, lookup_class=RecipientsLookup,
                                        widget=AutoComboboxSelectWidget(lookup_class=RecipientsLookup,
                                        attrs={"placeholder": _("Choose Profile"), "class": "dj-select-section recipient"}), label=_("Recipient"))

    class Meta:
        model = RegularPayment

    def clean_date_start_payment(self):
        if self.cleaned_data["date_start_payment"].strftime("%Y-%m-%d") < datetime.now().strftime("%Y-%m-%d"):
            raise forms.ValidationError(_("Enter a time later than the current time"))
        return self.cleaned_data["date_start_payment"]


class CreateRegularPaymentForm(RegularPaymentForm):
    def __init__(self, user=None, *args, **kwargs):
        super(CreateRegularPaymentForm, self).__init__(*args, **kwargs)
        self.fields["currency"] = OperationModeCurrencyField(user=user, use_user_currency=True)

    class Meta:
        model = RegularPayment
        fields = ("recipient", "amount", "currency", "date_start_payment", "type_periodicity", "comment")


class UpdateRegularPaymentForm(RegularPaymentForm):
    def __init__(self, user=None, *args, **kwargs):
        super(UpdateRegularPaymentForm, self).__init__(*args, **kwargs)
        self.fields["currency"] = OperationModeCurrencyField(user=user, use_user_currency=True)

    class Meta:
        model = RegularPayment
        widgets = {
            "active":  forms.CheckboxInput(attrs={"class": "mic_check", "mic_check": "mic_check"}),
        }
        fields = ("recipient", "amount", "currency", "date_start_payment", "type_periodicity", "comment", "active")