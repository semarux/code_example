# --coding: utf-8
from django import forms
from django.contrib.auth.models import User
from main.models import Profile, BusinessType
from financial_system.models import *
from django.db.models import Q
from django.forms import ModelForm
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from captcha.fields import CaptchaField
from django.contrib.auth import authenticate
from yacaptcha.fields import YaCaptchaField


class ProfileAdminForm(ModelForm):
    class Meta:
        model = Profile

    def clean_system_owner(self):
        if int(self.cleaned_data["system_owner"]) == 1:
            if Profile.objects.filter(system_owner=1).count() >= 1:
                raise forms.ValidationError(_("The system has to be one master"))
        return self.cleaned_data["system_owner"]


class RegistrationProfileForm(ModelForm):
    captcha = YaCaptchaField(label=_('Captcha'), help_text=_('Enter the text from the image'))

    class Meta:
        model = Profile
        fields = ("company_name", "company_business_type")
        widgets = {
            "company_name": forms.widgets.TextInput(attrs={"class": "", "placeholder": _("Your company name")}),
            "company_business_type": forms.widgets.Select(attrs={"class": "custom-sel", "width": "295", "placeholder": _("Your company name")}),
        }


class RegistrationUserForm(ModelForm):

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")
        widgets = {
            "first_name": forms.widgets.TextInput(attrs={"class": "", "placeholder": _("Your first name")}),
            "last_name": forms.widgets.TextInput(attrs={"class": "", "placeholder": _("Your last name")}),
            "email": forms.widgets.TextInput(attrs={"class": "", "placeholder": _("Your email")}),
        }

    def __init__(self, *args, **kwargs):
        super(RegistrationUserForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].required = True

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(_("This Email is already in use. Please enter a different email"))


class LoginForm(forms.Form):
    username = forms.CharField(required=True, label=_("Login"), widget=forms.TextInput(attrs={"class": "aleft", "placeholder": _("login or email")}))
    password = forms.CharField(required=True, label=_("Password"), widget=forms.PasswordInput(render_value=False, attrs={"class": "aright", "placeholder": _("password")}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        if self.cleaned_data.get("username") and self.cleaned_data.get("password"):
            try:
                user = User.objects.get(Q(email=self.cleaned_data["username"]) | Q(username=self.cleaned_data["username"]))
            except ObjectDoesNotExist:
                self._errors["username"] = ErrorList([_("User with the given username and password does not exist")])
            else:
                cleaned_data["username"] = user.username
                if not self.get_user():
                    self._errors["password"] = ErrorList([_("The password is wrong")])
                try:
                    profile = user.profile
                except ObjectDoesNotExist:
                    self._errors["username"] = ErrorList([_("The user has no profile")])
                else:
                    if profile.activated != 1:
                        self._errors["username"] = ErrorList([_("Your profile is not activated")])

            if not self.get_user():
                self._errors["password"] = ErrorList([_("Login or password is wrong")])

        return cleaned_data

    def get_user(self):
        cleaned_data = super(LoginForm, self).clean()
        return authenticate(username=self.cleaned_data["username"], password=self.cleaned_data["password"])


class AccountSettingsForm(ModelForm):
    class Meta:
        model = ProfileAccountSettings
        fields = ("escrow",)
        widgets = {
            "escrow": forms.CheckboxInput(attrs={"class": "mic_check"}),
        }


class ProfileSettingsForm(ModelForm):
    currency = forms.ModelChoiceField(required=False, empty_label=None, label=_('Currency'),
                                      queryset=Currency.objects.all().exclude(ps_special=1).order_by('order'),
                                      widget=forms.Select(attrs={'class': 'custom-sel all_width', }))

    class Meta:
        model = Profile
        fields = ["currency", "language", "phone"]
        widgets = {
            "language": forms.widgets.Select(attrs={"class": "custom-sel all_width"}),
        }