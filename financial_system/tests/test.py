# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.core.management import setup_environ
from torgplace import settings
setup_environ(settings)
from django_webtest import WebTest



def plus(int1, int2):
    return int1 + int2

class SimpleTest(WebTest):
    def test_plus(self):
        a = plus(1, 2)
        self.assertEqual(a, 3)

    def test_plus1(self):
        a = plus(1, 2)
        self.assertEqual(a, 31)

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
