# -*- coding: utf-8 -*-
from financial_system.models import *
from modeltranslation.translator import translator, TranslationOptions


class InformationBlockTranslationOptions(TranslationOptions):
    fields = ("name", "title", "short_description", "description")


class PaymentSystemTranslationOptions(TranslationOptions):
    fields = ("name", "description")


class OperatingModeTranslationOptions(TranslationOptions):
    fields = ("name", "description")

# class ShopTranslationOptions(TranslationOptions):
#     fields = ("name", )


class UserEventMessageOptions(TranslationOptions):
    fields = ("subject", "content")


class BankTranslationOptions(TranslationOptions):
    fields = ("name", "description", "address")


translator.register(InformationBlock, InformationBlockTranslationOptions)
translator.register(PaymentSystem, PaymentSystemTranslationOptions)
# translator.register(Shop, ShopTranslationOptions)
translator.register(OperatingMode, OperatingModeTranslationOptions)
translator.register(UserEventMessage, UserEventMessageOptions)
translator.register(Bank, BankTranslationOptions)
