var History = window.History;
    History.Adapter.bind(window, 'statechange', function() {
        updateContent(History.getState());
    });
    var updateContent = function(State) {
        var container = State.data.container;

        if ($(container).length == 0 && container != '.single-trade'){
            window.location = State.url;
            return false;
        }
        else if (State.data.url == undefined){
            window.location = State.url;
            return false;
        }

        if (State.data.content==undefined){
            if (State.data.is_ajax == true){
                if (State.data.type=="POST"){
                    $('.loader').show();
                    $.ajax({
                        url: State.url,
                        data:State.data.data,
                        type: 'POST',
                        success: function(data){
                            $('.loader').hide();
                            document.title = State.title;
                            $(container).show();
                            $(container).html(data);
                            initialPage();
                            return false;
                        }
                    });


                }else{
                    if (State.data.showProd != undefined){
                        if ($(container).length==0){
                            window.location = State.url;
                            return false;
                        }
                    }
                    $('.loader').show();
                    $.ajax({
                        type: "GET",
                        url: State.data.url,
                        dataType: "HTML",
                        success: function(data){
                            $('.loader').hide();
                            document.title = State.title;
                            $(container).show();
                            $(container).html(data);
                            initialPage();
                            return false;
                        }
                    });

                }

            }

        }else{
            $(container).html(State.data.content);
            $(window).resize();

        }

        if (State.data.display_container != "" ){
            $('.popover.history').hide();
            $(State.data.display_container).show();
        }

        return false;
    };