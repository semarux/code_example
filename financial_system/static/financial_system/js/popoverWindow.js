(function ($) {
    $.extend($.fn, {
        popoverWindow: function (options){
            var defaults = {
                winid : "",
                arrow : {
                    left : "50%",
                    position : "top"
                },
                bgcolor: "#15191c",
                position: "absolute",
                content: "",
                parent: "#header",
                url: "",
                h_url: "",
                history:false
            };
            var options = $.extend(defaults, options);
            var elem = $(this);
            elem.addClass("cur");
            $(".open-pop").each(function(){
                if (!$(this).hasClass("cur")) {
                    $(this).removeClass("open-pop");
                    hidePop();
                }
            });
            elem.removeClass("cur");
            elem.toggleClass("open-pop");
            var isClear;
            if (elem.hasClass("open-pop")) {
                if (!$("div").hasClass(options.winid)){
                    isClear = true;
                    $("body").append(
                        '<div class="popover ajax-pop '+options.winid+'" id="'+options.winid+'" style="display: none;">' +
                            '<div class="arrow-pop"></div>' +
                            '<div class="inner-pop"></div>' +
                        '</div>'
                    );
                } else isClear = false;
                var pop=$("."+options.winid);
                var arrow = pop.find(".arrow-pop");
                var inner = pop.find(".inner-pop");
                if (isClear==true) {
                    if (options.url!="") {inner.load(options.url,function(data){
//                        $('[placeholder]').placeholder();
                        var height = inner.height();
//                        checkSelect(options.winid);
                        var response = $(data);
                        var title = response.filter('.page_title').text();
                        var container = "#" + inner.closest('.popover ').attr('id') + ' .' + inner.attr('class');
                        if (options.history == true){
                            if (options.h_url != ""){
                                History.pushState({ url: options.url, is_ajax:false, display_container: "#" + inner.closest('.popover ').attr('id'), title:title, container: container, content: data}, title, options.h_url);
                            }
                            else{
                                History.pushState({ url: options.url, is_ajax:false, display_container: "#" + inner.closest('.popover ').attr('id'), title:title, container: container, content: data}, title, options.url);
                            }

                        }

                        mediaJS();
                        showPop(elem,pop,options,inner);
                    });} else {
                        inner.html(options.content);
//                        $('[placeholder]').placeholder();
//                        checkSelect(options.winid);
                        if (options.arrow.position=="right") $(".ui-multiselect-menu, .ui-autocomplete").css("position","fixed");
                        showPop(elem,pop,options,inner);
                    }

                }
                else{
                    showPop(elem,pop,options,inner);
                }

            } else hidePop();

        }
    });

    function showPop(elem,pop,options,inner){
        pop.css("position", options.position);
        if (options.arrow.position=="top" && $(options.parent).length){
            if ((elem.offset().left + elem.width()/2 + pop.width()/2) < $('#header .inner').offset().left+$("#header .inner").width() ) {
                popleft = elem.offset().left + elem.width()/2 - pop.width()/2;
            }
            else {
                popleft = $('#header .inner').offset().left+$("#header .inner").width() - pop.width()+1;
            }
            pop.css({
                //"left" : $('#header .inner').offset().left+$("#header .inner").width() - pop.width()+1,
                "left" : popleft,
                "top" : $(options.parent).offset().top + $(options.parent).height()
                /*"right" : $('#header .inner').offset().left+$("#nav-bar").width()+5*/
            });

            pop.addClass("top");
            pop.find(".arrow-pop").css({
                "border-bottom-color" : options.bgcolor,
                "left" : options.arrow.left
            });
        }
        if (options.arrow.position=="top" && !$(options.parent).length){
            pop.css({
                "left" : elem.offset().left + elem.width()/2 - pop.width()/2,
                "top" : elem.offset().top + $(options.parent).height() + 20
                /*"right" : $('#header .inner').offset().left+$("#nav-bar").width()+5*/
            });
            pop.addClass("top");
            pop.find(".arrow-pop").css({
                "border-bottom-color" : options.bgcolor,
                "left" : options.arrow.left
            });
        }
        if (options.arrow.position=="right"){
            pop.css({
                "position" : "fixed",
                "right" : $("#nav-bar").width(),
                "top" : elem.position().top - (elem.height()/2)
            });
            pop.addClass("right");
            pop.find(".arrow-pop").css({
                "border-left-color" : options.bgcolor
            });
        }
        inner.css("background",options.bgcolor);
        if (options.addScroll){
        pop.show(function(){
            $(".cart_popup_box").mCustomScrollbar();
//            refresh_cusel();
            checkSelect(options.winid);

        },"fast");
        }
        else {
            pop.show(10,function(){
                checkSelect(options.winid);
            });
        }

    }

    function hidePop(){
        $(".ajax-pop").hide();
        $(".ui-multiselect-menu, .ui-autocomplete").css("");
    }

    function checkSelect(cl){
        if ($("."+cl+" select").hasClass("custom-sel")){
            $("."+cl+" select.custom-sel").each(function(){
                if ($(this).attr("multiple")) {
                    $(this).multiselect({
                        header : false,
                        noneSelectedText : $(this).attr("data-placeholder"),
                        selectedText: "# выбрано",
                        position: {
                            my: 'left bottom',
                            at: 'left top'
                        }
                    });
                }
                else /*$(this).sb();*/{

                    var params = {
                        changedEl: $(this),
                        visRows: 5,
                        scrollArrows: true
                    }
                    cuSel(params);
                }
            });
        }
    }
    
    $(window).resize(function(){
        if ($(".ajax-pop").hasClass("top") && $(window).width()>730) {
            $(".ajax-pop.top").css("left", $('#header .inner').offset().left+$("#header .inner").width() - $(".ajax-pop.top").width()+1);
        } 
    });

})(jQuery);