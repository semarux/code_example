;(function ($) {
    // language utility
    jQuery.fn.message = function (mess) {
        if (typeof (mess) === 'string') {
            if (typeof jQuery.fn.message[mess] == 'undefined')
            return jQuery.fn.message[mess];
        }
        else {
            for (var i in mess) {
                jQuery.fn.message[i] = mess[i];
            }
            return true;
        }
    };
})(jQuery)
