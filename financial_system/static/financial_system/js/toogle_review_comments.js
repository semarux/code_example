/**
 * Created with PyCharm.
 * User: kortes
 * Date: 16.04.13
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
function toggleReply(elm, id)
{

    $("#reply"+id).toggleClass("open");
    if ($("#reply"+id).hasClass("open")) $("#reply"+id).slideDown(400)
    else $("#reply"+id).slideUp(400);
};


function toggleComments(elm, id)
{

    $("#comments"+id).toggleClass("open");
    if ($("#comments"+id).hasClass("open")) $("#comments"+id).slideDown(400)
    else $("#comments"+id).slideUp(400);
};

function toggleCommentForm(elm, id)
{

    $("#commentForm"+id).toggleClass("open");
    if ($("#commentForm"+id).hasClass("open")) $("#commentForm"+id).slideDown(400)
    else $("#commentForm"+id).slideUp(400);
}