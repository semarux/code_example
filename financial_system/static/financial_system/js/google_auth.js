var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
        var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
        var SCOPE       =   'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
        var CLIENTID    =   '1074417973478-6loqra359nl8jbbcllc5vcam409unisc.apps.googleusercontent.com';
        var REDIRECT    =   'http://finance.ti.dn.ua/balance_informer/soc_auth/';
        var LOGOUT      =   'http://accounts.google.com/Logout';
        var TYPE        =   'token';
        var _url  = url =   OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
        var acToken;
        var tokenType;
        var expiresIn;
        var user;
        var email;
        var loggedIn    =   false;

        function Glogin() {
            var win         =   window.open(_url, "windowname1", 'width=800, height=600');
            var pollTimer   =   window.setInterval(function() {
                try {
                    console.log(win.document.URL);
                    if (win.document.URL.indexOf(REDIRECT) != -1) {
                        window.clearInterval(pollTimer);
                        var url =   win.document.URL;
                        acToken =   gup(url, 'access_token');
                        tokenType = gup(url, 'token_type');
                        expiresIn = gup(url, 'expires_in');
                        win.close();
                        GvalidateToken(acToken);
                    }
                } catch(e) {
                    console.log(e);
                }
            }, 500);
        }



        function GvalidateToken(token) {
            $.ajax({
                url: VALIDURL + token,
                data: null,
                success: function(responseText){
                    email = responseText.email;
                    GgetUserInfo();
                },
                dataType: "jsonp"
            });
        }



        function GgetUserInfo() {
            $.ajax({
                url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + acToken,
                data: null,
                async: false,
                success: function(resp) {
                    user = resp;
                    userdata = {"uid": user.id, "email": email, "username": user.name, "first_name":user.given_name, "last_name": user.family_name};
                    socialLogin(userdata, "google");
                },
                dataType: "jsonp"
            });
        }


        function gup(url, name) {
            name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS = "[\\#&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var results = regex.exec( url );
            if( results == null )
                return "";
            else
                return results[1];
        }