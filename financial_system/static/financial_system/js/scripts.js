var ieVersion;


function openPop(buttonClass) {
    $(buttonClass).each(function(){
        var element = $(this);
        if($(this).hasClass("open")) {
            $($(this).attr("opPop")).show();
        }
        else {
            $($(this).attr("opPop")).hide();
        }
     });

    $(buttonClass).on("click",function(event){
        var action;

        if($(this).hasClass("open")) {
            action = false;
        }
        else {
            action = true;
        }
        $(buttonClass).each(function(){
                $(this).removeClass("open");
                $($(this).attr("opPop")).fadeOut();
        });
        if (action) {
            $(this).addClass("open");
            $($(this).attr("opPop")).fadeIn();
            console.log("ad open");
        }
    });
}



function micCheckbox (inputClass) {
    inputClass.each(function(){
        if(!$(this).siblings("span.new_check").length){
            $("<span class='new_check'></span>").prependTo($(this).closest("label"));
            if ($(this).hasClass("left")){
                $(this).siblings("span.new_check").css("float", "left");
            }
            else if ($(this).hasClass("right")){
                $(this).siblings("span.new_check").css("float", "right");
            }

            $(this).live("change",function(){
                inputClass.each(function(){
                    if ($(this).is(":checked")) {
                        $(this).siblings("span.new_check").addClass("checked");
                    }
                    else {
                        $(this).siblings("span.new_check").removeClass("checked");
                    }
                });
            });
        }
        if ($(this).is(":checked")) {
            $(this).siblings("span.new_check").addClass("checked");
        }
        else {
            $(this).siblings("span.new_check").removeClass("checked");
        }
    });
}


function showHideOtrizi(elem,shadow,refelText){
	if (elem.hasClass("short")){
		shadow.hide();
		refelText.text("свернуть");

		elem.removeClass("short");

	}
	else {
		shadow.show();
		refelText.text("подробнее");
		elem.addClass("short");
		refelText.show();

	}
}
//
function init_cusel(){
    var params = {
        changedEl: "select.custom-sel",
        visRows: 5,
        scrollArrows: true
    }
    cuSel(params);
}

function refresh_cusel(){
    cuSelRefresh({
        refreshEl: ".custom-sel"
    });
}


function getScrol(){
    if ($("body").find("div").is(".catalog-wrap")){
        $(".catalog-wrap").each(function(){
            var cWidth = 0;
            var maxH = 0;
            var maxD = 0;
            var maxC = 0;
            if (!$(this).find("div").hasClass("others-list")) {
                if ($(this).hasClass("compare-wrap")){
                    $(this).find(".item").each(function(){
                        cWidth += $(this).width()+10;
                        if ($(this).find(".info").height()>maxH) maxH = $(this).find(".info").height();
                    });
                    $(this).find(".info").css("height",maxH);
                    for (var i=0;i<$(this).find(".item .feature-item").length;i++){
                        $(this).find(".item").each(function(){
                            if ($(this).find(".feature-item").eq(i).height()>maxD) maxD = $(this).find(".feature-item").eq(i).height();
                        });
                        $(this).find(".item").each(function(){
                            $(this).find(".feature-item").eq(i).height(maxD);
                        });
                        maxD = 0;
                    }
                    $(this).find(".catalog-list").css({
                        "width" : cWidth-10
                    });
                } else{
                    $(this).find(".cat-item").each(function(){
                        cWidth += $(this).width()+11;
                    });

                    $(this).find(".catalog-list").css({
                        "width" : cWidth
                    });

                }
            } else {
                $(this).find(".cat-item").each(function(){
                    cWidth += $(this).width()+11;
                    if ($(this).height()>maxH) maxH = $(this).height();
                    if ($(this).find(".offerer").height()>maxD) maxD = $(this).find(".offerer").height();
                    if ($(this).find(".info").height()>maxC) maxC = $(this).find(".info").height();
                });

                $(this).find(".cat-item").find(".offerer").css("height",maxD);
                $(this).find(".cat-item").find(".info").css("height",maxC);
                $(this).find(".catalog-list").css({
                    "width" : cWidth,
                    "height": maxH+27
                });
            }
            $(this).mCustomScrollbar({horizontalScroll:true, scrollInertia: 200});
            if ($(this).hasClass("compare-wrap")) {
                $(".mCSB_scrollTools").css("top",$(window).height()-$("#header").height()-$(".a-header").height()-57);
            }

        });
    }
}




function initSlider(){
    if ($("body").find("div").is("#slider-home")){
        $("#slider-home .item").css("width",$(".promo.main-slider").width());
        $("#slider-home").carouFredSel({
            responsive  : true,
            items: {
                visible : 1,
                width: "1070"
            },
            height: "auto",
            prev: ".s-nav.prev",
            next: ".s-nav.next",
            auto: {
                easing : "swing",
                duration : 700,
                timeoutDuration : 7000,
                pauseOnHover : false
            }
        });
    }
}

function scrollView(){
    if (scroll_top>=50){
        $(".s-arrow").removeClass("to-bottom");
        $(".s-arrow").addClass("to-top");
        $(".s-arrow").attr("title","Вверх");
    } else {
        $(".s-arrow").removeClass("to-top");
        $(".s-arrow").addClass("to-bottom");
        $(".s-arrow").attr("title","Вниз");
    }
}

function mediaJS(){
    $("#nav-bar").height("");
    if ($(window).height()<$(document).height() && $(window).width()<980){
        $("#nav-bar").css("height",$("#page").height());
    }
    if ($(document).height()<=$(window).height()){
        if ($("section").hasClass("pagination-wrap")) {
            var curTop = $(window).height()-($(".pagination-wrap").outerHeight()+$("#footer").outerHeight()+1);
            if ($(".pagination-wrap").offset().top<curTop) {
                $(".pagination-wrap").css({
                    "margin-top" : curTop - $(".pagination-wrap").offset().top
                })
            }
        }
    }

    if ($("div").hasClass("catalog-grid-wrap")){
        var row = Math.ceil($(".catalog-grid-wrap").width()/$(".catalog-grid .cat-item").outerWidth());


        var mR = ($(".catalog-grid-wrap").width() - row*$(".catalog-grid .cat-item").width())/(row-1);

        if (mR>=9) {
            $(".catalog-grid").css("width",Math.ceil(row*$(".catalog-grid .cat-item").outerWidth()+row*mR));
            $(".catalog-grid .cat-item.top-level").css("margin-right",mR);
        } else {
            $(".catalog-grid").removeAttr("style");
            $(".catalog-grid .cat-item.top-level").removeAttr("style");
        }
    }

    if ($("div").hasClass("single-trade")) {
        $(".single-trade").css("width",$(".catalog-grid-wrap").width());
    }

    /*if ($("div").hasClass("users-sort")) {
        $(".users-sort").css("width",$(".wrapper .inner").width());
        $(".users-sort .arrow-pop").css("left",$(".wrapper .inner").innerWidth()-$(".sort").width());
    }*/

    if ($("div").hasClass("search-container")){
        $(".search-container").removeAttr("style");
        if ($(window).width()<=980) {
            $(".pop-search.ajax-pop").css("left", $(document).width()-$(".pop-search.ajax-pop").width()-80);
        }
    }

    /*if (document.getElementById("private")) {
        if ($(".private-tabs").height()>$(".private-content").height()) $(".private-content").height($(".private-tabs").height());
    }*/

    //var ua = navigator.userAgent.toLowerCase();
    //alert(ua);
    var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
    //var isMobile = ua.indexOf("mobile")>-1;
    if(jQuery.browser.mobile) {
        $("#nav-bar").height($(document).height());
        $("#page, #footer").width($(window).width()-70);
        /*if ($("div").hasClass("search-container")) $(".search-container").width($(window).width()-90);*/
    }
    if (isiPad) {
        $$i({
            create:'link',
            attribute: {
                'rel':'stylesheet',
                'type':'text/css',
                'href':'css/tablet.css'
            },
            insert:$$s.getelbytag('footer')[0]//подключаю стиль после тега head
        });
    }
}

function getInternetExplorerVersion()
{
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    return rv;
}
function checkIEVersion()
{
    ieVersion = getInternetExplorerVersion();
}

function initSingleFancy(){
    $('.fancybox-gal').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        prevEffect : 'none',
        nextEffect : 'none',
        showTitle : false
    });
}

/*function customCheck(){
    $('input[type="checkbox"].custom, input[type="radio"].custom').each(function(){
        var id = $(this).attr("id");
        $(this).addClass("hidden");
        $('label[for="'+id+'"]').addClass("custom-checkbox");
        $('label[for="'+id+'"]').prepend('<i class="icon-checkbox"></i>');
        if ($(this).attr("checked")) $('label[for="'+id+'"]').addClass("checked");
    });
}*/

function singlePos(url){
    var elem = $(".catalog-grid .cat-item.active");
    var row = Math.floor($(".catalog-grid").width()/($(".catalog-grid .cat-item").width()+parseInt($(".catalog-grid .cat-item").css("margin-right"))));
    var i = elem.index()+1;
    var curRow = Math.ceil(i/row);
    var length = $(".catalog-grid .cat-item").length;
        curRow = curRow * row;
    var newItem = $('<div class="single-trade"></div>');

    title = '';

    if (curRow<length) {
        $(".catalog-grid .cat-item.top-level").eq(curRow-1).after(newItem);
    } else $(".catalog-grid .cat-item.top-level").eq(length-1).after(newItem);
    newItem.css("width",$(".catalog-grid-wrap").width());


    try{
        History.pushState({
            url:url,
            is_ajax:true,
            arrow: '.single-trade .arrow',
            showProd: true,
            arrow_left: elem.offset().left-$(".catalog-grid").offset().left+elem.width()/2,
            container: '.single-trade'},
            title,
            url);
    }catch (e){
        window.location = url;
    }


    return false;
}

var alphaWidth;

function alphaWidthCalc(minH){
    $("ul.alphabet").css("display","block");
    var kol = $("ul.alphabet li").length;
    var row = Math.ceil(kol/minH);
    alphaWidth = (row * ($("ul.alphabet li").width()+parseInt($("ul.alphabet li").css("margin-right"))))+140;
    $("ul.alphabet").css("display","none");
    return false;
}

function createAlphabet(){/*
    var minH = 3;
    alphaWidthCalc(minH);
    while (alphaWidth>($("#alphabet").width())){
        minH++;
        alphaWidthCalc(minH);
    }
    var kol = $(".alphabet li").length;
    var ar = new Array();
    var k = 0;
    var j = 0;
    ar[k] = new Array();
    var clon = $("ul.alphabet").clone().addClass("clone");
    for (var i=1;i<=kol;i++){
        ar[k][j] = clon.find("li").eq(i-1);
        j++;
        if (i % minH ==0) {
            k++;
            ar[ar.length]=new Array();
            j=0;
        }
    }
    $("ul.alphabet").css("display","none");
    $("div.alphabet").remove();
    if (!$("div").hasClass("alphabet")){
        var obj = $("<div class='alphabet'></div>");
        $("#alphabet").append(obj);
    } else {
        var obj = $("div.alphabet");
    }
    obj.html("");
    for (var n=0;n<=k;n++){
        var ul = $("<ul></ul>");
        for (var m=0;m<ar[n].length;m++){
            ul.append(ar[n][m]);
        }
        obj.append(ul);
    }
    $(".alphabet.clone").remove();
    $(".alphabet ul").each(function(){
       if ($(this).html()=="") $(this).remove();
    });*/

}

/*function detectActive(){
    $(".private-arrow").css({
        "left" : $(".private-tabs li").width()-7,
        "top" : $(".private-tabs .ui-state-active").position().top-5+$(".private-tabs li").height()/2
    });
}*/

function tiCheck(){
    $(".import-container .custom").tiCheckbox({margin: "12"});
    $(".users-sort .custom").tiCheckbox({margin: "12", position: "left"});
    $(".filter-pop .custom").tiCheckbox({margin: "12", position: "right"});
    $(".profile_type .custom").tiCheckbox({margin: "12", position: "left"});
}

$(document).ready(function(){

    getScrol();

    initSlider();

    checkIEVersion();

    mediaJS();

    tiCheck();

    micCheckbox($(".mic_check"));

    openPop(".tab_lang");
    openPop(".tab_lang_short");


    $(".fancy_page").fancybox({
        'type' : 'iframe',

    })

    //customCheck();
    $('.sb-custom-sel').sb();


    $(".feedback_container .pop_feedback").live("click",function(event){
        event.preventDefault();
        $(this).siblings(".feedback_hidden").slideToggle();
        $(this).toggleClass("active")
    });



    $(".categories_list a.sect").live("dblclick",function(event){
        event.preventDefault();
        location.href = $(this).attr("href");
        return false;
    });


    $(".categories_list a.sect").live("click",function(event){
        event.preventDefault();
        if ($(this).closest("li").children("ul").length){
            if ($(this).closest("li").hasClass("open")){
                $(this).closest("li").removeClass("open");
                $(this).siblings("ul").slideUp("slow");
                $(this).siblings(".icon_bg").removeClass("open");
            }
            else {
                $(this).closest("li").addClass("open");
                $(this).siblings("ul").slideDown("slow");
                $(this).siblings(".icon_bg").addClass("open");
            }
        }

    });






//    if($(".feedback").length) {
//        var top_marg = $(".feedback").offset().top - 25;
//        $(".feedback_wind").offset({top:top_marg, right:0})
//    }


//    $(".set.manufacturer").live('click',function(){
//
//        if ($(this).hasClass('open-pop')){
//            $(this).removeClass('open-pop');
//        }else{
//            $(this).addClass('open-pop');
//        }
//
//        $('.properties_list').hide();
//        $('.manufacturer_list').slideToggle(250);
//
//        setTimeout(function(){
//            oHeight = $(".manufacturer_list").outerHeight();
//
//            if (oHeight < 556  ) {
//                textHeight = "-" + (oHeight-22) + "px";
//                $(".manufacturer_list").css("margin-top",textHeight);
//            }
//
//        },270);
//
//    });
//
//
//    $(".set.properties").live('click',function(){
//
//        if ($(this).hasClass('open-pop')){
//            $(this).removeClass('open-pop');
//        }else{
//            $(this).addClass('open-pop');
//        }
//        $('.manufacturer_list').hide();
//        $('.properties_list').slideToggle(250);
//
//        setTimeout(function(){
//            oHeight = $(".properties_list").outerHeight();
//
//            if (oHeight < 556  ) {
//                textHeight = "-" + (oHeight-22) + "px";
//                $(".properties_list").css("margin-top",textHeight);
//            }
//
//        },270);
//
//    });




    $(".filter_settings .set").live('click',function(){
        if($(this).hasClass("open-pop")) {
            var open = true;
        }

        $(".filter_settings .popover").slideUp(250);

        $(".filter_settings .set").removeClass("open-pop");

        if(!open) {
            $(this).closest(".filter_settings").find(".popover").slideDown(250, function(){
                var pageHeight = $("#main").height() - $(".a-header").outerHeight() - $(".pagination-wrap").outerHeight()- $(".footer-container").outerHeight() - $(this).closest(".filter_settings").find(".popover").outerHeight() + $(this).closest(".filter_settings").find(".popover").height() -25;
                $(".filter_settings .popover").css("max-height", pageHeight);
                if (!$(this).closest(".filter_settings").find(".popover").hasClass("initScroll")) {
                    $(this).closest(".filter_settings").find(".popover").addClass("initScroll").mCustomScrollbar();
                }

            });
            $(this).addClass("open-pop");
        }
    });


    if ($("div").hasClass("preview")) initSingleFancy();

    if (ieVersion<=8) setTimeout("initSlider()",400);

    if ($("select").hasClass("custom-sel")){
        /*$(".custom-sel").sb();*/
        var params = {
            changedEl: ".custom-sel",
            visRows: 5,
            scrollArrows: true
        }
        cuSel(params);
    }

    if ($("div").hasClass("tabs")) $(".tabs").tabs();

//    if (document.getElementById("private")) {
//        $("#private").tabs({
//            active: $('#spike1').val(),
//            activate: function( event, ui ) {
//                $(".private-arrow").css("display","none");
//                ui.newTab.find(".private-arrow").css("display","block");
//                //if ($(".private-tabs").height()>ui.newPanel.height()) ui.newPanel.height($(".private-tabs").height());
//               /* $(".private-arrow").css({
//                    "left" : $(".private-tabs li").width()-6,
//                    "top" : ui.newTab.position().top+ui.newTab.height()/2
//                });*/
//            }
//        });
//        $(".private-tabs .ui-state-active").find(".private-arrow").css("display","block");
//    }


//    $("#private" ).tabs({
//        active:$.cookie("privateTab"),
//        beforeLoad: function(event, ui) {
//            $('.loader').show();
//            var selectedTab = $(this).tabs('option', 'active');
//            $.cookie("privateTab", selectedTab);
//            ui.jqXHR.error(function() {
//            ui.panel.html(
//                "Couldn't load this tab. We'll try to fix this as soon as possible. " +
//                "If this wouldn't be a demo." );
//            });
//        },
//        load: function(event, ui){
//            var cont_class = $(this).find(".ui-tabs-active .hidden").text();
//            var anchor = ui.tab.find(".ui-tabs-anchor");
//            var url = anchor.attr('href');
//            var content = ui.panel.context;
//            var title = $(content).find('.page_title').text();
//            ui.panel.eq(0).addClass(cont_class);
//            $('.loader').hide();
////            console.log($(content).html());
////            $(content).html()
////            History.pushState({ url: url, title:title, container: '#private', content: 'sssssss'}, title, url);
//        }
//    });

//    private-tabs


//    $(".private-tabs li").each(function(){
//        if ($(this).hasClass('active')){
//            $(this).find('.private-arrow').show();
//        }
//    });


//    $("#private").tabs();

    //$('#sale .tabs').tabs({active:$($('#spike2').val()).index()-1});
    //$('#purchase .tabs').tabs({active:$($('#spike2').val()).index()-1});


    $(".orders-tab .item .title, .import-container .item h3").click(function(){
        if ($(this).parents(".item").hasClass("open")) {
            $(this).parents(".item").find(".content").slideUp(300);
            $(this).parents(".item").removeClass("open");
        } else {
            $(this).parents(".item").siblings(".open").find(".content").slideUp(300);
            $(this).parents(".item").siblings(".open").removeClass("open");
            $(this).parents(".item").find(".content").slideDown(300);
            $(this).parents(".item").addClass("open");
        }
    });

    $(".orders-tab .item .toggle-btn").click(function(){
        $(this).parents(".item").find(".content").slideUp(300);
        $(this).parents(".item").removeClass("open");
        return false;
    });

    $(".file-btn").click(function(){
        $(this).siblings(".custom-file").trigger("click");
        return false;
    });

    //$(".private-content select").cusel();
    /*var params = {
        changedEl: ".private-content select",
        visRows: 5,
        scrollArrows: true
    }
    cuSel(params);*/


$(".new-trade .re-file").live("click", function(){
        $(this).siblings("input[type='file']").trigger("click");
    });

    $(".new-trade input[type='file']").live("change", function(){
        $(this).siblings(".re-file").val($(this).val());
    });


    $(".custom-file").change(function(){
        $(this).siblings(".file-url").text($(this).val());
    });

    // этот код не нужен
    // Используйте formset

//    $(".new-trade .add-row").live("click", function(){
//        var parent = $(this).parents(".row");
//        var newRow = parent.clone().insertAfter(parent);
//        newRow.find('.col.title').css('visibility', 'hidden');
//        if (newRow.find(".del-row").length==0) $('<div class="del-row"></div>').insertBefore(newRow.find(".add-row"));
//        var idAr = newRow.attr("id").split("-");
//        nRid = idAr[0]+'-'+(parseInt(idAr[1])+1);
//        newRow.attr("id", nRid);
//        //if (idAr[0]=='photo') newRow.find(".title").html("&nbsp;");
//        newRow.find(".cusel").each(function(){
//            //console.log("htf");
//            $(this).find('input[type="hidden"]').attr("id",nRid);
//            var idAr = $(this).attr("id").split("-");
//            $(this).attr("id",idAr[0]+'-'+idAr[1]+'-'+(parseInt(idAr[2])+1));
//        });
//        parent.find(".add-row").remove();
//        if (parent.find(".del-row").length==0) parent.append('<div class="del-row"></div>');
//    });
//
//    $(".new-trade .del-row").live("click",function(){
//        var parent = $(this).parents(".row");
//        if (parent.find(".add-row").length!=0) {
//            /*var idAr = parent.prev().attr("id").split("-");
//            if (idAr[1]==1){parent.prev().find(".del-row").remove()}*/
//            parent.prev().append('<div class="add-row"></div>');
//        }
//        console.log("");
//        parent.remove();
//        if (($(".new-trade .row.group").length==1)) $(".row.group").find(".del-row").remove();
//        else if (($(".new-trade .row.photo").length==1)) $(".row.photo").find(".del-row").remove();
//    });

    $(".s-arrow").click(function(e){
       var scroll_top = $(window).scrollTop();
       if($(this).hasClass("to-bottom")) {
           $.scrollTo("#footer",500);
           scrollView();
       } else {
           $.scrollTo("#header",500);
           scrollView();
       }
        e.preventDefault();
    });

    /*$(".custom-checkbox").click(function(){
        var id = $(this).attr("for");
        if ($("#"+id).attr("type")=="radio") {
            $('input[name="'+$("#"+id).attr("name")+'"]').each(function(){
                $('label[for="'+$(this).attr("id")+'"]').removeClass("checked");
            });
            $(this).addClass("checked");
        } else $(this).toggleClass("checked");
        if ($(this).parents("div.filter-pop").attr("id")=="sort"){
            $(".sort").find(".kind").text($("#"+id).val()+":");
        }
        if ($(this).parents("div.filter-pop").attr("id")=="compare"){
            $(".compare").find(".kind").text($("#"+id).val());
        }
    });*/



    $(".alphabet li").live("click", function(){
        if ($(this).hasClass("active")) $(".alphabet li").removeClass("active");
        else {
            $(".alphabet li").removeClass("active");
            $(this).addClass("active");
        }
    });

    $(".switcher").live("click", function(){
        $(".sort-tab").removeClass("open");
        $(this).find(".switch").toggleClass("open");
        if ($(this).find(".switch").hasClass("open")) {
            $(this).find('input').val(1);
            $(".sort-tab#alphabet").addClass("open");
            $(this).parents(".sort-content").find(".search").hide();
        } else {
            $(this).find('input').val(0);
            $(".sort-tab#filter_container").addClass("open");
            $(this).parents(".sort-content").find(".search").show(100,function(){refresh_cusel();});
        }


    });

    $(".switch-group label").live('click',function(){
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        $(this).closest('.switch-group').find('input[type="radio"]').removeAttr("checked");
        $(this).find('input[type="radio"]').attr("checked","checked");
        $(this).find('input[type="radio"]').trigger('change');
    });

    $(".auth-lnk").click(function(){
        $(this).popoverWindow({
            winid : "pop-auth",
            arrow : {
                left : "252px",
                position : "top"
            },
            bgcolor : "#15191c",
            history : true,
            url : '/login/?timestamp='+now_timestamp(),
            h_url: '/login/'
        });
        return false;
    });

    $(".settings-lnk").live("click", function(){
        $(this).popoverWindow({
            winid : "pop-settings",
            arrow : {
//                left : "29px",
                position : "top"
            },
            bgcolor : "#15191c",
            url : $(this).attr("href") + "?timestamp="+now_timestamp()
        });
        return false;
    });

    $("#menu .search").click(function(){
//        var content = $("#search").clone();
//        $("#search").remove();
        $(this).popoverWindow({
            winid : "pop-search",
            arrow : {
                position : "right"
            },
            bgcolor : "#ff8400",
            position: "absolute",
//            content:content
            url : "/products/view_search_form/"
        });
        return false;
    });

    $("#menu .cart").click(function(){
        $(this).popoverWindow({
            winid : "pop-cart",
            arrow : {
                position : "right"
            },
            bgcolor : "#a868c7",
            position: "fixed",
            url : "/basket_mini/",
            addScroll : true
        });

        //$(".cart_popup_box").mCustomScrollbar();
        return false;
    });

    $("body").click(function(e){
        var clicked = $(e.target);
        var clickedClass = clicked.prop("class");
        var cur = clicked.hasClass('cur')||clicked.parents('.sort').hasClass('cur');

        if (!clicked.closest(".popover")){
            $.cookie("filter_active", 0);
            $.cookie("sort_active", 0);
            $.cookie("manufacturer_active", 0);
            $.cookie("properties_active", 0);
        }


        if (clicked.hasClass('profile_filter')){
            if (clicked.hasClass('profile_filter')&&clicked.hasClass('open-pop')){
                $.cookie("profile_filter_active", 0);
            }else{
                $.cookie("profile_filter_active", 1);
            }
        }



        if (clicked.hasClass('filters')){
            if (clicked.hasClass('filters')&&clicked.hasClass('open-pop')){
                $.cookie("filter_active", 0);
            }else{
                $.cookie("filter_active", 1);
                $.cookie("manufacturer_active", 0);
                $.cookie("properties_active", 0);
                $.cookie("sort_active", 0);
            }
        }

        if (clicked.hasClass('manufacturer')){
            if (clicked.hasClass('manufacturer')&&clicked.hasClass('open-pop')){
                $.cookie("manufacturer_active", 0);
            }else{
                $.cookie("manufacturer_active", 1);
                $.cookie("sort_active", 0);
            }
        }

         if (clicked.hasClass('properties')){
            if (clicked.hasClass('propertiesr')&&clicked.hasClass('open-pop')){
                $.cookie("properties_active", 0);
            }else{
                $.cookie("properties_active", 1);
                $.cookie("sort_active", 0);
            }
        }


        if (clicked.closest('.sort').length > 0){
            if (clicked.closest('.sort').hasClass('open-pop')){
                $.cookie("sort_active", 0);
            }else{
                $.cookie("sort_active", 1);
                $.cookie("filter_active", 0);
                $.cookie("manufacturer_active", 0);
                $.cookie("properties_active", 0);
            }
        }






        if(clicked.parents("div").hasClass("ajax-pop")||clicked.parents("div").hasClass("popover")|| clicked.parents("div").hasClass("ui-widget")||clicked.parents("ul").hasClass("ui-widget")){
        }else{

            $(".open-pop").removeClass("open-pop");
            $(".cur").removeClass("cur");
            $(".ajax-pop,.popover").hide();
            if(cur){
                $(".open-pop").removeClass("open-pop");
                return false;
            }
        }

    });

//     $(".filters-wrap a").live('click',function(){
//
////        $($(this).siblings(".open").attr("href")).slideUp(300);
////        $(this).siblings(".open").removeClass("open");
////
////        $(this).toggleClass("open");
////        if (($(this).position().left+$($(this).attr("href")).width())>$(".a-header .wrapper").innerWidth()) {
////            $($(this).attr("href")).css("right", $("#header .wrapper").css("padding-left"));
////        } else {
////            $($(this).attr("href")).css("left", $(this).position().left+parseInt($(this).css("margin-left")));
////        }
//        if (!$(this).hasClass("open")) {
//            $(this).addClass('open');
//            $(this).addClass('cur');
//            $($(this).attr("href")).slideDown(300);
//            $("#left-range").css("left",-($("#left-range").width() - $(".ui-slider-handle").width())/2);
//            $("#right-range").css("left",-($("#right-range").width() - $(".ui-slider-handle").width())/2);
//        }
//
//        return false;
//    });

    $(".catalog-grid .cat-item").live('click', function(event){
        event.preventDefault();

        if ($(this).hasClass('active') && $(this).hasClass('last_act')){
            $(this).removeClass('active');
            $('.single-trade').hide();
        }else{
            if($(this).hasClass('last_act')){
                $(this).addClass('active')
                $('.single-trade').show();
            }else{
                $('.catalog-grid .cat-item').removeClass('active');
                $('.catalog-grid .cat-item').removeClass('last_act');

                $(this).addClass('last_act');
                $(this).addClass('active');
                $('.loader').show();
                $('.single-trade').remove();
                singlePos($(this).find('a.product-item').attr('href'));


            }
        }



//        if ($(this).hasClass('active')){
//            $('.single-trade').hide();
//            $(this).removeClass("active");
//            $(this).addClass("last_active");
//        }else
//        {
//            if ($(this).hasClass("lact_active")){
//                $('.single-trade').show();
//            }
//            else {
//                $(this).addClass("cur");
//                $('.loader').show();
//                var length = 0;
//                $(".catalog-grid .cat-item").each(function(){
//                    if (!$(this).hasClass("cur")) {
//                        $(this).removeClass("active");
//                        $(".catalog-grid").find(".single-trade").remove();
//
//                    }
//                    length++;
//                });
//                $(this).removeClass("cur");
//                $(this).toggleClass("active");
//                if ($(this).hasClass("active")) {
//                    $(this).append('<div class="substrate"></div>');
//                    singlePos($(this).find('a.product-item').attr('href'));
//                    //return false;
//                } else {
//                    $('.loader').hide();
//                    $(this).find(".substrate").remove();
//                    $(".catalog-grid").find(".single-trade").remove();
//                    var ua = navigator.userAgent.toLowerCase();
//                    var isMobile = ua.indexOf("mobile")>-1;
//                    if(isMobile) {
//                        $("#nav-bar").height($("#page").height());
//                }
//            }
//            }
//
//        }






        //return false;
    });



    $(".raiting").hover(function(){
        if (!($(this).hasClass("disabled"))){
            $(this).append("<span></span>");
        }
    },function(){
        $(this).find("span").remove();
    });

    var rating;

    $(".raiting").mousemove (function(e){
        if (!e) e = window.event;
        if (e.pageX){
            x = e.pageX;
        } else if (e.clientX){
            x = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
        }
        var posLeft = 0;
        var obj = this;
        while (obj.offsetParent)
        {
            posLeft += obj.offsetLeft;
            obj = obj.offsetParent;
        }
        var offsetX = x-posLeft,
            modOffsetX = 5*offsetX%this.offsetWidth;
        rating = parseInt(5*offsetX/this.offsetWidth);

        if(modOffsetX > 0) rating+=1;

        $(this).find("span").eq(0).css("width",rating*16+"px");
    });

    $(".raiting").click (function(){
        if (!($(this).hasClass("disabled"))){
            return false;
        }
    });


    //Dima

    $('.inner-pop>p>label').live('click', function() {
        $('a.sort>span.catalog_sort').text($(this).text()+':');
    });

    $('.sort_arrows').live('toggle',function() {
        //top
        $('.sort_arrows>label>div.sort_aright_radio_arrow_bottom').removeClass('active');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_top').addClass('active');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_bottom>input').removeAttr('checked');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_top>input').attr('checked','checked');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_top>input').trigger('change');
        $('.catalog_style').text($('.sort_arrows>label>div.sort_aright_radio_arrow_top').text());

    }, function() {
        $('.sort_arrows>label>div.sort_aright_radio_arrow_top').removeClass('active');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_bottom').addClass('active');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_bottom>input').attr('checked','checked');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_top>input').removeAttr('checked');
        $('.sort_arrows>label>div.sort_aright_radio_arrow_bottom>input').trigger('change');
        $('.catalog_style').text($('.sort_arrows>label>div.sort_aright_radio_arrow_bottom').text());
    });



    /*currency*/
    $(".currency_div").live({
        mouseenter:function(){
                $(this).find('.currency').fadeIn(200);
            },
        mouseleave:function(){
                $(this).find('.currency').fadeOut(200);
            }
        }
    );

    $('.currency_ul>li').live('click', function() {
        $('.currency_ul>li').removeClass('active');
        $('.currency_ul>li input').removeAttr('checked');
        $(this).find('input').attr('checked','checked');
        $(this).addClass('active');
        $(this).find('input').trigger('change');
    });

    $(".stom-check input").live("change",function(){

        if($(this).is(":checked")){
            $(this).parent("span").addClass("stom-checked");
        }
        else {
            $(this).parent("span").removeClass("stom-checked");
        }
    });

    $(".masseger_nav li a").live("click",function(e) {
        visib = $(this).attr("href");

        $(".masseger_nav li a").each(function(index, element) {
            $($(this).attr("href")).hide();
            $(this).parent("li").removeClass("active_li");
        });
        $(visib).show();
        $(this).parent("li").addClass("active_li");

        return false;
    });


//    $(".feedback").live("click",function(event){
//        event.preventDefault();
//        $(".feedback_wind").fadeToggle(600,function(){
//            history.replaceState(2, "Feedback", "/feedback/?timestamp="+now_timestamp());
//        });
//     });

    $(".feedback").click(function(){
        $(this).popoverWindow({
            winid : "pop-feedback",
            arrow : {
                left : "252px",
                position : "right"
            },
            bgcolor : "#99d106",
            url : '/feedback/?timestamp='+now_timestamp()
        });
        return false;
    });

    $(".catalog_settings").live("click",function(event){
        event.preventDefault();
        $(".catalog_settings_wind").fadeToggle(100);
    });

    $(".trader_table_cont").hide();
	$(".item.basket_item .title_name").live("click", function(){
		$(this).closest(".title").siblings("div.trader_table_cont").slideToggle();
	});

	$(".change_number").live("click",function(){
		var valu = parseInt($(this).siblings("input.input_change").val());
		if ($(this).hasClass("plus")){
			$(this).siblings("input.input_change").val(valu+1);
		}
		if ($(this).hasClass("minus")){
			$(this).siblings("input.input_change").val(valu > 1 ? valu-1 : 1)
		}
		$(this).parents("div.trader_table_cont").siblings("p.treder_name").find(".refresh_price").fadeIn("slow");
	});

//    $("body").live("click",function(event){
//        if ($(event.target).closest(".hidden_sale").length ||  $(event.target).closest(".setting_tree").length) return false;
//        $(".hidden_sale").slideUp("slow",$(".hidden_sale").remove())
//    })

	$(".setting_tree").live("click",function(){
        if ($(".hidden_sale").length) $(".hidden_sale").remove();

		var hidden_div;
        var hidden_id = parseInt($(this).siblings("input").val());
        var rel_pash = $(this).attr("rel");
        $("#chandes_tree").attr("action",rel_pash);

        // Создание каталога
        if ($(this).hasClass("add_item_tree")){
            hiddenDiv = $('<div  class="hidden_sale static"></div>');
            //console.log(hiddenDiv);
            $(this).parent("li").append(hiddenDiv);
            $(this).siblings(".hidden_sale").slideDown();
            $.ajax({
                url: "/products/folder/add/"+hidden_id,
                cache: false,
                success: function(html){
                    $(".hidden_sale").html(html);
                }
            });
        }
        // Удаление каталога
        if ($(this).hasClass("del_item_tree")) {
            hiddenDiv = $('<div class="hidden_sale static"></div>');
            $(this).parent("li").append(hiddenDiv);
            $(this).siblings(".hidden_sale").slideDown();
            $.ajax({
                url: "/products/folder/del/"+hidden_id,
                cache: false,
                success: function(html){
                    $(".hidden_sale").html(html);
                }
            });
        }
        // Настройки каталога
        if ($(this).hasClass("open_set_tree")) {
            hiddenDiv = $('<div class="hidden_sale lastic"></div>');
            $(this).parent("li").append(hiddenDiv);
            $(this).siblings(".hidden_sale").slideDown("slow");
            $.ajax({
                url: "/products/folder/settings/"+hidden_id,
                cache: false,
                success: function(html){
                    $(".hidden_sale").html(html);
                }
            });
        }
        // Перемещение товара в другой каталог
        if ($(this).hasClass("icon-purple")) {
            hiddenDiv = $('<div class="hidden_sale lastic"></div>');
            $(this).parent("td").append(hiddenDiv);
            $(this).siblings(".hidden_sale").slideDown("slow");
            $("#chandes_products").attr("action",rel_pash);
            $.ajax({
                url: "/products/folder/transport/"+hidden_id,
                cache: false,
                success: function(html){
                    $(".hidden_sale").html(html);
                }
            });
        }
        // Редактирование заказа продавцом
        if ($(this).hasClass("edit_order")) {
            hiddenDiv = $('<div class="hidden_sale static"></div>');
            $(this).parent("div").append(hiddenDiv);
            $(this).siblings(".hidden_sale").slideDown("slow");
            //alert(rel_pash);
            $("#chandes_ordering").attr("action",rel_pash);
            $.ajax({
                url: rel_pash,
                cache: false,
                success: function(html){
                    $(".hidden_sale").html(html);
                }
            });
        }

	});


	$("input.cencel_pop").live("click",function(event){
		event.preventDefault();
        $(this).parents(".hidden_sale").slideUp("slow",$(this).parents(".hidden_sale").remove());

	});



	$(".ravenst").live("click",function(){
		if ($(this).hasClass("open")){
			$(this).removeClass("open");
			$(this).siblings("ul.tree_lvl_two").slideUp("slow");
		}
		else {
            if (!$(this).siblings("ul.tree_lvl_two").length)return false;
			$(this).addClass("open");
			$(this).siblings("ul.tree_lvl_two").slideDown("slow");
		}
	});

    //var hInfo = $(".text_more").height() ? $(".text_more").height() : 0

    $(".text_more").each(function(){
        if ($(this).height() > 120) {showHideOtrizi($(this),$(this).siblings(".gredient_shadow"),$(this).siblings(".show_detail_otzivi"))}
    });

    //if ($(".text_more").height() > 131) {showHideOtrizi($(".text_more"),$(".gredient_shadow"),$(".show_detail_otzivi"))};

	$(".show_detail_otzivi").live("click", function(event){
		event.preventDefault();
		showHideOtrizi($(this).siblings(".text_more"),$(this).siblings(".gredient_shadow"),$(this))
	});


    $(".sort-tab .coll .custom").tiCheckbox({margin: "12", position: "left"});

	$(".alfavit_sort").live('click', function(e) {
        $(".alfavit_radio").each(function(index, element) {
            if ($(this).is(":checked")){
				$('.alphabetchoice_' + $(this).val()).show();
				$('.alphabetchoice_' + $(this).val()).siblings("ul.alphabet").hide();
			}
        });
    });

});

$(window).resize(function(){
    mediaJS();

    if ($("div").hasClass("single-trade")) singlePos();

//    if ($(".users-sort .switch").hasClass("open")){
//        createAlphabet();
//    }
});

$(window).scroll(function(){
    scroll_top = $(window).scrollTop();
    scrollView();
    if ($("div").hasClass("compare-wrap")) {
        var scroll = $(window).height()-$("#header").height()-$(".a-header").height()-57+scroll_top;
        if (scroll<$(".compare-wrap").height()+80) $(".mCSB_scrollTools").css("top",scroll);
        else $(".mCSB_scrollTools").css("top","");
    }
});

//
//function changeLang(el) {
//    var lang = $(el).val();
//
//    if (lang != '') {
//        var img = $('#lang');
//        var src = img.attr('src');
//        var new_src = src.replace(/\/(\w+)\./g, '/' + lang + '.');
//        img.attr('src', new_src);
//    }
//}

$('.delivery_name').live('keyup', function(){
    $(this).closest('.delivery_row').next('.add-row').trigger('click');
});

$('.tax_name').live('keyup', function(){
    $(this).closest('.tax_row').next('.add-row').trigger('click');
});

$('.include_region').live('keyup', function(){
    $(this).closest('.include_row').next('.add-row').trigger('click');
});

$('.exclude_region').live('keyup', function(){
    $(this).closest('.exclude_row').next('.add-row').trigger('click');
});

$('input[name=tax_region]').live('change', function(){
    $(this).closest('.tax_row').find('.chosen_tax_region').val($(this).val());
});

$('.tax_location').live('click', function(){
    $(this).closest('.tax_row').find('.chosen_tax_location').val($(this).closest('.tax_region').find('input[name=tax_region]').val());
    $(this).closest('.tax_row').find('.chosen_tax_region').val(null);
});

$('input[name="add_manufacturer"]').live('change', function(){
    if ($(this).attr('checked')){
        $('#cuselFrame-id_manufacturer').addClass('classDisCusel');
        $('#cuselFrame-id_manufacturer').css({opacity: 0.5});
    }else{
        $('#cuselFrame-id_manufacturer').removeClass('classDisCusel');
        $('#cuselFrame-id_manufacturer').css({opacity: 1});
    }
});
