//function hide_popup_mess(){
//    $(".popup_messages_container").fadeOut(500);
//}
//
//function popup_messages(messages){
//    $(".popup_messages_container").html(messages);
//    $(".popup_messages_container").show();
//}
//


function initialPage(){
    getScrol();
    initSlider();
    checkIEVersion();
    mediaJS();
    micCheckbox($(".mic_check"));
}

function now_timestamp(){
    return new Date().getTime();
}


var calendar_congig =  {
    changeMonth: true,
    changeYear: true,
    dateFormat: "yy-mm-dd",
};

var datetime_congig =  {
    changeMonth: true,
    changeYear: true,
    changeSecond:true,
    dateFormat: "yy-mm-dd",
    timeFormat: "H:mm:ss",
};

function convert_url(url){
    if (~url.indexOf("?")){
        url = url + "&timestamp=" + now_timestamp();
    }else{
        url = url + "?timestamp=" + now_timestamp();
    }
    return url;
}


function send_html_post(form, container, extra_url){
    var url = "";
    if (extra_url){
        url = convert_url(form.attr("action") + extra_url);
    }else{
        url = convert_url(form.attr("action"));
    }
    $('.loader').show();
    $.ajax({
        url: url,
        data: form.serialize(),
        type: "POST",
        success: function(data, status, xhr){
            $('.loader').hide();
            $(container).html(data);
            var new_path = $(".page_path").text();
            if (new_path.length){
                window.history.pushState(null, null, new_path);
            }
            return false;
        },
        error: function(error){
            $('.loader').hide();
            console.log(error);
            return false;
        }
//        complete : function(){
//            alert(this.url)
//            window.history.pushState(null, null, this.url);
//        }
    });
}

function send_html_get(href,container){
    $('.loader').show();
    $.ajax({
        url: convert_url(href),
        type: "GET",
        success: function(data){
            $('.loader').hide();
            $(container).html(data);
            return false;
        },
        error: function(error){
            $('.loader').hide();
            console.log(error);
            return false;
        }
    });
}

$(document).ready(function(){
    $(".calendar").datepicker(calendar_congig);
    $(".datetime").datetimepicker(datetime_congig);
    $(".popup_mess_close").live("click", function(){
        $(".mess").hide();
    });
    $(".js-captcha-refresh").live("click",function(){
        $.ajax({
            url:$(this).closest("form").attr("action") + "?timestamp="+now_timestamp(),
            type: "GET",
            success: function(data){
                var captcha = $(data).find(".form_captcha").html();
                $(".form_captcha").html(captcha);
                return false;
            }
        });
        return false;
    });

    $(".settings-lnk").live("click", function(){
        $(this).popoverWindow({
            winid : "pop-settings",
            arrow : {
//                left : "29px",
                position : "top"
            },
            bgcolor : "#15191c",
            url : $(this).attr("href") + "?timestamp="+now_timestamp()
        });
        return false;
    });

    $("#id_language").live("change",function(){$(this).closest("form").submit();});
    $("#id_change_currency").live("change",function(){$("#change_currency_go").trigger("click");});


    $(".history a, .a_history").live("click",function(evt){
        if ($(this).hasClass("active")) {
            return false;
        }
        $(".loader").show();
        var h_url = url = $(this).attr("href");
        var url = convert_url(url);

        History.pushState({ url: url, is_ajax:true, container: "#main .container"}, $(this).text(), h_url);
        return false;
    });


    $(".ok_grid thead a, .ok_grid_paginate a").live("click", function(){
        if ($("#grid_filter_form").length == 0){
            send_html_get($(this).attr("href"), "#main .container");
        }else{
            $('#grid_filter_form input:radio[name="spec_action"]').filter('[value="filter"]').attr('checked',true);
            send_html_post($("#grid_filter_form"), "#main .container", $(this).attr("href"));
        }
        return false;
    });


    $('#grid_filter_form input[type="text"]').live("change", function(){
        $('input:radio[name="spec_action"]').filter('[value="filter"]').attr('checked',true);
        send_html_post($(this).closest("form"), "#main .container");
        return false;
    });


    $("#multiple_action_form").live("submit", function(){
        send_html_post($(this), "#main .container");
        return false;
    });


    $("#save_to_xml").live("click", function(){
        $('input:radio[name="spec_action"]').filter('[value="'+ $(this).attr("name") +'"]').attr('checked',true);
    });

    function adding_funds(form){
        var send = 1;
        form.find('input').each(function(){
            if ($(this).val() == ""){
                send = 0;
            }
        });
        if (send == 1){
            $('.loader').show();
            var url = convert_url(form.attr('action'));
            send_html_post(form, "#main .container");
        }
    }

//    $("#adding_funds_form input").live("change", function(){
//        adding_funds($(this).closest('form'));
//        return false;
//    });
//
//    $("#adding_funds_form").live("submit", function(){
//        adding_funds($(this));
//        return false;
//    });
//
//    $("#adding_funds_create_form").live("submit", function(){
//        condole
//
//        send_html_post($(this), ".create_order_form_container");
//        return false;
//    });


//    $("#adding_funds_create_form input[type='submit']").live("click", function(){
//        console.log('1');
//        $('#adding_funds_create_form input:radio[name="spec_action"]').filter('[value="adding"]').attr('checked',true);
//        adding_funds($(this).closest('form'));
//        return true;
//    });

    $("#adding_funds_create_form input").live("change", function(){
        $('#adding_funds_create_form input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        adding_funds($(this).closest('form'));
        return false;
    });


    $("#adding_funds_create_form").live("submit", function(){
        console.log(1);
        $('#adding_funds_create_form input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        adding_funds($(this));
        return false;
    });


    $(".ok_grid .check_all").live("change", function(){
        if ($(this).is(':checked')){
            $(".ok_grid .check_item").attr("checked", true);
        }else{
            $(".ok_grid .check_item").attr("checked", false);
        }
        if ($(this).hasClass("mic_check")){
            micCheckbox($(".mic_check"));
        }
        return false;
    });


    $(".ps_form_QM").live("submit", function(){
        send_html_post($(this), ".ps_form_container");
        return false;
    });


    $("#conversion_funds_create input").live("change", function(){
        $('#conversion_funds input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        if ($(this).attr("name")=="debit_amount"||$(this).attr("name")=="debit_currency"){
            send_html_post($(this).closest("form"), "#main .container");
        }else{
            if ($("#id_debit_amount").val() != ""){
                send_html_post($(this).closest("form"), "#main .container");
            }
        }
        return false;
    });


    $("#conversion_funds_create input[type='submit']").live("click", function(){
        $('#conversion_funds_create input:radio[name="spec_action"]').filter('[value="convert"]').attr('checked',true);
        send_html_post($(this).closest("form"), "#main .container");
        return false;
    });


    $("#withdrawal_funds_create #id_amount_to_withdrawal, #withdrawal_funds_create #id_currency_to_withdrawal").live("change", function(){
        $('input:radio[name="main_amount"]').filter('[value="withdrawal"]').attr('checked',true);
//        if ($("#withdrawal_funds_create #id_currency_to_withdrawal").val() != ""){
//            send_html_post($(this).closest("form"), "#main .container");
//        }
//        return false;
    });

    $("#withdrawal_funds_create #id_amount_to_obtain").live("change", function(){
        $('input:radio[name="main_amount"]').filter('[value="obtain"]').attr('checked',true);
//        if ($("#withdrawal_funds_create #id_currency_to_obtain").val() != ""){
//            send_html_post($(this).closest("form"), "#main .container");
//        }
//        return false;
    });


    $("#withdrawal_funds_create #id_currency_to_obtain").live("change",function(){
        if ($("#withdrawal_funds_create #id_amount_to_obtain").val() == ""){
            $('input:radio[name="main_amount"]').filter('[value="withdrawal"]').attr('checked',true);
        }else{
            $('input:radio[name="main_amount"]').filter('[value="obtain"]').attr('checked',true);
        }

    });

//    $("#withdrawal_funds_create #enter").live("click", function(){
//        $('input:radio[name="spec_action"]').filter('[value="'+ $(this).attr("name") +'"]').attr('checked',true);
//    });


    $("#withdrawal_funds_create input, #withdrawal_funds_create textarea").live("change", function(){
        var send = 1;
        $('#withdrawal_funds_create input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        $(this).closest("form").find("input, textarea").each(function(){
            if ($(this).val() == ""){
                send = 0;
            }
        });
        if (send == 1){
            send_html_post($(this).closest("form"), "#main .container");
            return false;
        }else{

            if ($(this).attr("name") == "amount_to_withdrawal" || $(this).attr("name") == "currency_to_withdrawal"){
                if (($("#withdrawal_funds_create #id_amount_to_withdrawal").val() != "" || $("#withdrawal_funds_create #id_amount_to_obtain").val() != "") && $("#withdrawal_funds_create #id_currency_to_withdrawal").val() != ""){
                    send_html_post($(this).closest("form"), "#main .container");
                }
            }
            else if ($(this).attr("name") == "amount_to_obtain" || $(this).attr("name") == "currency_to_obtain"){
                if (($("#withdrawal_funds_create #id_amount_to_obtain").val() != "" || $("#withdrawal_funds_create #id_amount_to_withdrawal").val() != "") && $("#withdrawal_funds_create #id_currency_to_obtain").val() != ""){
                    send_html_post($(this).closest("form"), "#main .container");
                }
            }
        }
    });


    $("#withdrawal_funds_create #id_payment_system").live("change", function(){
        send_html_get($(this).closest("form").attr("action") + $(this).val() + "/", "#main .container");
        return false;
    });


    $("#transfer_funds_create .recipient").live("djselectableselect", function(event, ui) {
        var recipient_id = ui.item.id;
        send_html_get($(this).closest("form").attr("action") + recipient_id + "/", "#main .container");
        return false;
    });



//    $("#transfer_funds_create #enter").live("click", function(){
//        $('input:radio[name="spec_action"]').filter('[value="'+ $(this).attr("name") +'"]').attr('checked',true);
//    });


    $("#transfer_funds_create input, #transfer_funds_create textarea").live("change", function(){
        var send = 1;
        $('#transfer_funds_create input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        $(this).closest("form").find("input, textarea").each(function(){
            if ($(this).val() == ""){
                send = 0;
            }
        });
        if (send == 1){
            send_html_post($(this).closest("form"), "#main .container");
            return false;
        }else{

        }
    });

    $('.business_type input[type=text]').live('autocompleteselect',function(e, item) {
        if ($('input[name=business_type_1]').length >= 3){
            $(this).autocomplete("disable");
        }
    });

    $("#invoice_pay input").live("change", function(){
        $('#invoice_pay input:radio[name="spec_action"]').filter('[value="estimate"]').attr('checked',true);
        send_html_post($(this).closest("form"), "#main .container");
        return false;
    });


    $(".main_send_html_post").live("submit", function(){
        send_html_post($(this), "#main .container");
        return false;
    });

//    $(".main_send_html_post input, .main_send_html_post textarea").live("change", function(){
//        send_html_post($(this).closest("form"), "#main .container");
//        return false;
//    });


    $("#invoice_discussion_send_form").live("submit", function(){
        console.log(1);
        return false;
        send_html_post($(this), "#main .container");
        return false;
    });


    $("#enter, #send").live("click", function(){
        $('input:radio[name="spec_action"]').filter('[value="'+ $(this).attr("name") +'"]').attr('checked',true);
        send_html_post($(this).closest('form'), "#main .container");
        return false;
    });




});