# --coding: utf-8
import django_settings
from django.contrib.sites.models import get_current_site
from django.utils.translation import ugettext as _
from financial_system.models import *
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.forms.member import LoginForm


def financial_vars(request):
    operation_mode = None
    user_balance = None

    if request.user.is_authenticated():
        operation_mode = get_obj_or_none(OperatingMode, active=True)
        user_balance = UserBalance.objects.select_related("currency").filter(user=request.user, payment_coupon=None)

    return {
        "operation_mode": operation_mode,
        "user_balance": user_balance,
        "login_form": LoginForm(),
    }
