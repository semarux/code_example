from financial_system.models import Shop
from financial_system.api.serializers import ShopSerializer
# from financial_system.api.permission import ShopIsOwnerOrReadOnly
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
import hashlib
from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework import generics
from django.utils.translation import ugettext_lazy as _
from financial_system.helpers.system_functs import  get_obj_or_none

class ShopIsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user


def get_secret_key(obj_id, obj_password):
    secret_key = hashlib.md5()
    secret_key.update("%s%s" % (obj_id, obj_password))
    return secret_key.hexdigest()


def shop_pre_save(request, obj):
    obj.user = request.user
    obj.profile = request.user.profile
    obj.secret_key = get_secret_key(obj.id, obj.password)
    return obj

class ShopList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of shop.
    """
    model = Shop
    serializer_class = ShopSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def pre_save(self, obj):
        obj = shop_pre_save(self.request, obj)

    def get(self, request, format=None):
        shops = Shop.objects.filter(user=request.user)
        serializer = ShopSerializer(shops, many=True)
        return Response(serializer.data)


class ShopDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single shop.
    """

    model = Shop
    serializer_class = ShopSerializer
    permission_classes = (ShopIsOwnerOrReadOnly, permissions.IsAuthenticated)

    def pre_save(self, obj):
        obj = shop_pre_save(self.request, obj)


class ShopCheck(APIView):
    """
    API endpoint that represents a check single shop.
    """

    model = Shop
    serializer_class = ShopSerializer

    def get_object(self, pk):
        return get_obj_or_none(Shop, pk=pk)

    def get(self, request, pk, password, format=None):
        shop = self.get_object(pk)
        secret_key = get_secret_key(pk, password)
        if shop:
            if shop.secret_key == secret_key:
                if shop.status == "na":
                    content = {"data": _("Shop is not active mode or test mode")}
                    return Response(content, status.HTTP_412_PRECONDITION_FAILED)
                elif not shop.shop_informer:
                    content = {"data": _("Shop informer isn't activate")}
                    return Response(content, status.HTTP_412_PRECONDITION_FAILED)
                else:
                    serializer = ShopSerializer(shop)
                    return Response(serializer.data)
            else:
                content = {"data": _("Shop didn't pass on the validity of the data")}
                return Response(content, status.HTTP_412_PRECONDITION_FAILED)
        else:
            content = {"data": _("Shop didn't exists")}
            return Response(content, status.HTTP_412_PRECONDITION_FAILED)

