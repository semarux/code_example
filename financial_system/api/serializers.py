from financial_system.models import *
from rest_framework import serializers
from rest_framework.authtoken.models import Token


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name",)


class UserAuthTokenSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.Field(source="user.username")
    email = serializers.Field(source="user.email")

    class Meta:
        model = Token
        fields = ("key", "username", "email", "created")


class UserBalanceSerializer(serializers.HyperlinkedModelSerializer):
    currency = serializers.Field(source='currency.iso_code')

    class Meta:
        model = UserBalance
        fields = ("id", "state", "currency",)


class ShopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shop
        fields = (
            "id",
            "description",
            "shop_url",
            "password",
            "secret_key",
            "success_url",
            "fail_url",
            "status",
            "date_create",
            "date_update",
        )

        read_only_fields = ("id", "secret_key",)





