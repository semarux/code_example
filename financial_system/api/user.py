from django.contrib.auth.models import User, Group
from financial_system.models import UserBalance

from rest_framework import generics
from rest_framework import views
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from financial_system.api.serializers import UserSerializer, UserBalanceSerializer, UserAuthTokenSerializer
from rest_framework import permissions

from rest_framework.authtoken.models import Token



from rest_framework import renderers
from django.contrib.auth import authenticate
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt


class UserBalanceList(views.APIView):
    """
    API endpoint that represents a list of user balance.
    """
    model = UserBalance
    serializer_class = UserBalanceSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        balances = UserBalance.objects.filter(user=request.user, payment_coupon=None)
        serializer = UserBalanceSerializer(balances, many=True)
        if request.QUERY_PARAMS.get("format") == "html":
            return Response({'balances': balances}, template_name='api/user/balance.html')
        else:
            return Response(serializer.data)


class UserGetAuthTokenByAuthLogin(generics.SingleObjectAPIView):
    model = Token
    serializer_class = UserAuthTokenSerializer


    @csrf_exempt
    def post(self, request, format=None):
        user = authenticate(username=self.request.DATA["username"], password=self.request.DATA["password"])
        if user:
            token = Token.objects.get_or_create(user=user)[0]
            serializer = UserAuthTokenSerializer(token, many=False)
            return Response(serializer.data)
        else:
            content = {"data": "Authentication Failed"}
            # return Response(content, status.HTTP_200_OK)
            return Response(content, status.HTTP_401_UNAUTHORIZED)

from torgplace.settings import GOOGLE_OAUTH2_CLIENT_ID, GOOGLE_OAUTH2_CLIENT_SECRET

class UserLoginGoogle(generics.SingleObjectAPIView):
    pass
#     model = Token
#     serializer_class = UserAuthTokenSerializer
#
#     def get(self, request, format=None):
#         user = authenticate(username=self.request.DATA["username"], password=self.request.DATA["password"])
#         if user:
#             token = Token.objects.get_or_create(user=user)[0]
#             serializer = UserAuthTokenSerializer(token, many=False)
#             return Response(serializer.data)
#         else:
#             content = {"data": "Authentication Failed"}
#             # return Response(content, status.HTTP_200_OK)
#             return Response(content, status.HTTP_401_UNAUTHORIZED)




class UserGetAuthToken(generics.SingleObjectAPIView):
    model = Token
    serializer_class = UserAuthTokenSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        token = Token.objects.get_or_create(user=request.user)[0]
        serializer = UserAuthTokenSerializer(token, many=False)
        return Response(serializer.data)



class UserList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of users.
    """
    model = User
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single user.
    """
    model = User
    serializer_class = UserSerializer