# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        pass
        # Adding model 'InformationBlock'
        #db.create_table(u'financial_system_informationblock', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        #    ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('name_en', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('title', self.gf('django.db.models.fields.CharField')(max_length=250)),
        #    ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
        #    ('title_en', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
        #    ('short_description', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
        #    ('short_description_ru', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
        #    ('short_description_en', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
        #    ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        #    ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        #    ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        #    ('type', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
        #))
        #db.send_create_signal('financial_system', ['InformationBlock'])
        #
        ## Adding model 'PaymentSystem'
        #db.create_table(u'financial_system_paymentsystem', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        #    ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('name_en', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('description_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        #    ('withdrawals_min', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('withdrawals_max', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('withdrawals_currency', self.gf('django.db.models.fields.related.ForeignKey')(related_name='withdrawals_currency', to=orm['main.Currency'])),
        #    ('commission', self.gf('django.db.models.fields.FloatField')()),
        #    ('commission_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        #    ('commission_currency', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='commission_currency', null=True, to=orm['main.Currency'])),
        #    ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        #))
        #db.send_create_signal('financial_system', ['PaymentSystem'])

        #Adding model 'CommissionSetting'
        #db.create_table(u'financial_system_commissionsetting', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
        #    ('commission', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('type_commission', self.gf('django.db.models.fields.CharField')(max_length=1)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'], null=True, blank=True)),
        #))
        #db.send_create_signal('financial_system', ['CommissionSetting'])

        # Adding model 'UserBalance'
        #db.create_table(u'financial_system_userbalance', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        #    ('profile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['main.Profile'], unique=True)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'])),
        #    ('state', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=15, decimal_places=2)),
        #))
        #db.send_create_signal('financial_system', ['UserBalance'])
        #
        ## Adding unique constraint on 'UserBalance', fields ['user', 'currency']
        #db.create_unique(u'financial_system_userbalance', ['user_id', 'currency_id'])
        #
        ## Adding model 'Withdrawal'
        #db.create_table(u'financial_system_withdrawal', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        #    ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Profile'])),
        #    ('manager', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='withdrawal_manager', null=True, to=orm['auth.User'])),
        #    ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'])),
        #    ('payment_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financial_system.PaymentSystem'])),
        #    ('purse', self.gf('django.db.models.fields.CharField')(max_length=50)),
        #    ('comment', self.gf('django.db.models.fields.TextField')()),
        #    ('paid', self.gf('django.db.models.fields.BooleanField')(default=False)),
        #    ('status', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
        #    ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        #    ('date_pay', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        #))
        #db.send_create_signal('financial_system', ['Withdrawal'])
        #
        ## Adding model 'RegularPayment'
        #db.create_table(u'financial_system_regularpayment', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        #    ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Profile'])),
        #    ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'])),
        #    ('payment_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financial_system.PaymentSystem'])),
        #    ('purse', self.gf('django.db.models.fields.CharField')(max_length=50)),
        #    ('comment', self.gf('django.db.models.fields.TextField')()),
        #    ('num_periodicity', self.gf('django.db.models.fields.IntegerField')(default=1)),
        #    ('type_periodicity', self.gf('django.db.models.fields.CharField')(max_length=1)),
        #    ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        #    ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        #))
        #db.send_create_signal('financial_system', ['RegularPayment'])
        #
        ## Adding model 'Shop'
        #db.create_table(u'financial_system_shop', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        #    ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Profile'])),
        #    ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        #    ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('name_en', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        #    ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        #    ('url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        #    ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        #    ('date_update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        #    ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        #))
        #db.send_create_signal('financial_system', ['Shop'])
        #
        ## Adding model 'InvoicePayment'
        #db.create_table(u'financial_system_invoicepayment', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financial_system.Shop'], null=True, blank=True)),
        #    ('order', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        #    ('sender_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='payment_sender', to=orm['auth.User'])),
        #    ('recipient_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='payment_reciplient', to=orm['auth.User'])),
        #    ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'])),
        #    ('short_description', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
        #    ('comment', self.gf('django.db.models.fields.TextField')()),
        #    ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        #    ('date_update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        #    ('paid', self.gf('django.db.models.fields.BooleanField')(default=False)),
        #    ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        #))
        #db.send_create_signal('financial_system', ['InvoicePayment'])
        #
        ## Adding model 'PaymentHisoty'
        #db.create_table(u'financial_system_paymenthisoty', (
        #    (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        #    ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        #    ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Profile'])),
        #    ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
        #    ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        #    ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financial_system.Shop'], null=True, blank=True)),
        #    ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('bill', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        #    ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Currency'])),
        #    ('payment_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['financial_system.PaymentSystem'], null=True, blank=True)),
        #    ('purse', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        #    ('payment_kind', self.gf('django.db.models.fields.CharField')(max_length=1)),
        #    ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        #))
        #db.send_create_signal('financial_system', ['PaymentHisoty'])


    def backwards(self, orm):
        # Removing unique constraint on 'UserBalance', fields ['user', 'currency']
        db.delete_unique(u'financial_system_userbalance', ['user_id', 'currency_id'])

        # Deleting model 'InformationBlock'
        db.delete_table(u'financial_system_informationblock')

        # Deleting model 'PaymentSystem'
        db.delete_table(u'financial_system_paymentsystem')

        # Deleting model 'CommissionSetting'
        db.delete_table(u'financial_system_commissionsetting')

        # Deleting model 'UserBalance'
        db.delete_table(u'financial_system_userbalance')

        # Deleting model 'Withdrawal'
        db.delete_table(u'financial_system_withdrawal')

        # Deleting model 'RegularPayment'
        db.delete_table(u'financial_system_regularpayment')

        # Deleting model 'Shop'
        db.delete_table(u'financial_system_shop')

        # Deleting model 'InvoicePayment'
        db.delete_table(u'financial_system_invoicepayment')

        # Deleting model 'PaymentHisoty'
        db.delete_table(u'financial_system_paymenthisoty')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'financial_system.commissionsetting': {
            'Meta': {'ordering': "('commission',)", 'object_name': 'CommissionSetting'},
            'commission': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type_commission': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'financial_system.informationblock': {
            'Meta': {'ordering': "('name', 'order')", 'object_name': 'InformationBlock'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'short_description': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'short_description_en': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'})
        },
        'financial_system.invoicepayment': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'InvoicePayment'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'paid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'recipient_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payment_reciplient'", 'to': u"orm['auth.User']"}),
            'sender_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payment_sender'", 'to': u"orm['auth.User']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['financial_system.Shop']", 'null': 'True', 'blank': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        'financial_system.paymenthisoty': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'PaymentHisoty'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'bill': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'payment_kind': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'payment_system': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['financial_system.PaymentSystem']", 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Profile']"}),
            'purse': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['financial_system.Shop']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        'financial_system.paymentsystem': {
            'Meta': {'ordering': "('name',)", 'object_name': 'PaymentSystem'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'commission': ('django.db.models.fields.FloatField', [], {}),
            'commission_currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'commission_currency'", 'null': 'True', 'to': "orm['main.Currency']"}),
            'commission_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'withdrawals_currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'withdrawals_currency'", 'to': "orm['main.Currency']"}),
            'withdrawals_max': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'withdrawals_min': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'financial_system.regularpayment': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'RegularPayment'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_periodicity': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'payment_system': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['financial_system.PaymentSystem']"}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Profile']"}),
            'purse': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type_periodicity': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        'financial_system.shop': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'Shop'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Profile']"}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        'financial_system.userbalance': {
            'Meta': {'unique_together': "(('user', 'currency'),)", 'object_name': 'UserBalance'},
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['main.Profile']", 'unique': 'True'}),
            'state': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '15', 'decimal_places': '2'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        'financial_system.withdrawal': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'Withdrawal'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Currency']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_pay': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'withdrawal_manager'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'paid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'payment_system': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['financial_system.PaymentSystem']"}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Profile']"}),
            'purse': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        'main.businesstype': {
            'Meta': {'ordering': "('date_create',)", 'object_name': 'BusinessType'},
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'main.clothesmeasure': {
            'Meta': {'object_name': 'ClothesMeasure'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'main.country': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'main.currency': {
            'Meta': {'ordering': "('name', 'iso_code')", 'object_name': 'Currency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'multiplicity': ('django.db.models.fields.FloatField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'num_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'rate': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        'main.location': {
            'Meta': {'ordering': "('title', 'country_code')", 'object_name': 'Location'},
            'country_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '10'}),
            'lng': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '10'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Region']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'titlefull': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'titlefull_en': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'titlefull_ru': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        'main.measure': {
            'Meta': {'object_name': 'Measure'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'main.profile': {
            'Meta': {'ordering': "('user__username',)", 'object_name': 'Profile'},
            'access_level': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'activated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'activation_key': ('django.db.models.fields.CharField', [], {'default': "'ALREADY_ACTIVATED'", 'max_length': '40'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'avatar': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'avatar'", 'sizes': '((180, 180), (500, 500))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'clothes_measure': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['main.ClothesMeasure']"}),
            'company_address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'company_business_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.BusinessType']", 'null': 'True', 'blank': 'True'}),
            'company_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'company_location': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'company_location'", 'null': 'True', 'to': "orm['main.Location']"}),
            'company_location_str': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'company_zip_code': ('django.db.models.fields.CharField', [], {'default': "'not filled'", 'max_length': '10'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['main.Currency']"}),
            'dealer': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'distributor'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'ru'", 'max_length': '3'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'location'", 'null': 'True', 'to': "orm['main.Location']"}),
            'location_str': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'measure': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['main.Measure']"}),
            'nationality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Country']", 'null': 'True', 'blank': 'True'}),
            'parent_user': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'my_referrals'", 'null': 'True', 'blank': 'True', 'to': u"orm['auth.User']"}),
            'payment_terms': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'phone': ('phonenumber_field.modelfields.PhoneNumberField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'sale_site_store': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'sum_rate': ('django.db.models.fields.FloatField', [], {'default': "'0'"}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'user_code': ('django.db.models.fields.CharField', [], {'default': "'e59e2981e040d4cb6723ce8f7c1149f4'", 'unique': 'True', 'max_length': '32'}),
            'vote_count': ('django.db.models.fields.IntegerField', [], {'default': "'0'"}),
            'zip_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'})
        },
        'main.region': {
            'Meta': {'object_name': 'Region'},
            'clothes_measure': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.ClothesMeasure']", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'measure': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Measure']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'region_children'", 'null': 'True', 'blank': 'True', 'to': "orm['main.Region']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        }
    }

    complete_apps = ['financial_system']