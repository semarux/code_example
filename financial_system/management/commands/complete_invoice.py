# -*- coding: utf-8 -*-
from optparse import make_option
from django.core.management.base import BaseCommand
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.models import Invoice
import django_settings
from main.models import Profile


def run():
    import datetime
    from financial_system.helpers.amount import invoice_complete_in_favor_owner
    if not django_settings.exists('INVOICE_TIME_LIMIT'):
        django_settings.set('Integer', 'INVOICE_TIME_LIMIT', 14)
    invoice_time_limit = django_settings.get('INVOICE_TIME_LIMIT')
    system_owner = get_obj_or_none(Profile, system_owner=1)

    invoices = Invoice.objects.filter(completed=False, date_pay__lte=datetime.datetime.today() - datetime.timedelta(days=invoice_time_limit))
    for invoice in invoices:
        system_owner_balance = get_obj_or_none(UserBalance, user=system_owner.user, currency=invoice.currency, payment_coupon=invoice.payment_coupon)
        if system_owner_balance and system_owner_balance.state >= invoice.amount:
            invoice_complete_in_favor_owner(invoice, system_owner, system_owner_balance)
    return True


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option("--run", action="store_true", dest="run", help="complete invoice"),
    )

    def handle(self, *args, **options):
        if options["run"]:
            run()
        else:
            print "Choose option: [--run]"
        print "Done"

