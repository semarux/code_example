# -*- coding: utf-8 -*-
from optparse import make_option
from django.core.management.base import BaseCommand
from main.models import Profile

def run():
    profiles = Profile.objects.all()
    for profile in profiles:
        profile.special_id = "%s%s" % (1000, profile.user.id)
        profile.save()




class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option("--run", action="store_true", dest="run", help="Run regular payments"),
    )

    def handle(self, *args, **options):
        if options["run"]:
            run()
        else:
            print "Choose option: [--run]"
        print "Done"
