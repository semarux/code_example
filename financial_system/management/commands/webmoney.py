#-- coding: utf-8 --
import hashlib
import pycurl
import cStringIO
from django.core.management.base import BaseCommand
from torgplace import settings
buf = cStringIO.StringIO()

class Command(BaseCommand):
    def handle(self, *args, **options):
        lmi_payment_no = "11111"  # номер платежа, состояние которого запрашивается
        wmid = "384074784738"  # ваш wmid
        lmi_payee_purse = "U134468009447"  # ваш кошелек-получатель, на который совершался платеж
        secret_key = "S4JRV5kR6b"  #SecretKey, заданный в настройках кошелька на WM Merchant
        m = hashlib.md5()
        m.update(wmid + lmi_payee_purse + lmi_payment_no + secret_key)
        md5 = m.hexdigest().upper()
        # формируем xml-запрос
        # т.к. используется хеш, то 2 других метода авторизации - sign и secret_key -
        # оставляем пустыми

        request = """
            <merchant.request>
                <wmid>""" + wmid + """</wmid>
                <lmi_payee_purse>""" + lmi_payee_purse + """</lmi_payee_purse>
                <lmi_payment_no>""" + lmi_payment_no + """</lmi_payment_no>
                <sign>ok</sign>
                <md5>""" + md5 + """</md5>
                <secret_key>""" + secret_key + """</secret_key>
            </merchant.request>"""
        #отправляем запрос с помощью curl методом POST и получаем ответ

        c = pycurl.Curl()
        c.setopt(pycurl.URL, "https://merchant.webmoney.ru/conf/xml/XMLTransGet.asp")
        c.setopt(pycurl.HEADER, 0)
        c.setopt(pycurl.E_HTTP_RETURNED_ERROR, 1)
        c.setopt(pycurl.POST, 1)
        c.setopt(pycurl.POSTFIELDS, request)
        c.setopt(pycurl.CAINFO, settings.MEDIA_ROOT + "/WMunited.cer")
        c.setopt(pycurl.SSL_VERIFYPEER, True)
        c.setopt(pycurl.WRITEFUNCTION, buf.write)
        c.perform()
        print buf.getvalue()
        buf.close()

        # разбираем xml-ответ с помощью simplexml
        #xmlres = simplexml_load_string($result)
        ## смотрим результат выполнения запроса
        #retval = strval($xmlres->retval)
        #
        #if retval == -8: # если результат равен -8, то платежа с таким номером не было
        #    print  u"Платеж $lmi_payment_no не проводился!"
        #
        #elif retval !=0: #если результат не равен -8 и не равен 0, то возникла ошибка при обработке запроса
        #    print "Запрос составлен некорректно!";
        #else: #если результат равен 0, то платеж с таким номером проведен
         #   # вытаскиваем важные параметры платежа
        #    wmtranid=strval($xmlres->operation->attributes()->wmtransid);
        #    date = strval($xmlres->operation->operdate);
        #    payer = strval($xmlres->operation->pursefrom);
        #    ip = strval($xmlres->operation->IPAddress);
        #    paymer_number = strval($xmlres->operation->paymer_number);
        #    # отображаем результаты на экране
        #    print """
        #        Платеж $lmi_payment_no завершился успешно.
        #        Он был произведен $date с кошелька $payer.
        #        Плательщик использовал IP-адрес $ip.
        #        WM-транзакции присвоен идентификатор $wmtranid.
        #    """