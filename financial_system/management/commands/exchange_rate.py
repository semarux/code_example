# -*- coding: utf-8 -*-
from optparse import make_option
from django.core.management.base import BaseCommand
from django.template import loader
from datetime import datetime
from django.contrib.sites.models import Site
from financial_system.helpers.system_functs import get_obj_or_none
from main.models import Currency
from financial_system.models import ExchangeRate
import urllib2
import json


def set():
    currency_list = Currency.objects.all().exclude(ps_special=1)
    for currency_from in currency_list:
        for currency_to in currency_list:
            if currency_from.iso_code != currency_to.iso_code:
                data_response = json.load(urllib2.urlopen("http://rate-exchange.appspot.com/currency?from=%s&to=%s" % (currency_from.iso_code, currency_to.iso_code)))
                if not data_response.has_key("err"):
                    exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (currency_from.iso_code, currency_to.iso_code))
                    if exchange_rate:
                        exchange_rate.rate = data_response.get("rate")
                        exchange_rate.save()
                    else:
                        exchange_rate = ExchangeRate(
                            iso_code="%s%s" % (currency_from.iso_code, currency_to.iso_code),
                            currency_from=currency_from,
                            currency_to=currency_to,
                            rate=data_response.get("rate")
                            )
                        exchange_rate.save()
                else:
                    print "No service on currency pairs(%s-%s)" % (currency_from.iso_code, currency_to.iso_code)


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option("--set", action="store_true", dest="set", help="set exchange rate"),
    )

    def handle(self, *args, **options):
        if options["set"]:
            set()
        else:
            print "Choose option: [--set]"
        print "Done"

