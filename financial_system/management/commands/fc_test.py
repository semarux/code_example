# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from optparse import make_option
from financial_system.tasks import finance_send_mail


def test_redis():
    finance_send_mail.delay()


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--redis', action='store_true', dest='redis', help='Run some redis task'),
        make_option('--all', action='store_true', dest='all', help='Make all actions'),
    )

    def handle(self, *args, **options):
        if options['redis']:
            test_redis()
            print "test_redis"
        elif options['all']:
            print "all"
            pass
        else:
            print "Choose option: [--redis], [--all]"

