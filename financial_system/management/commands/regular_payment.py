# -*- coding: utf-8 -*-
from optparse import make_option
from django.core.management.base import BaseCommand

from django.template import loader
from datetime import datetime
from django.contrib.sites.models import Site
from financial_system.helpers.amount import get_billing_information
from financial_system.helpers.system_functs import get_obj_or_none
from financial_system.models import UserBalance, RegularPayment, PaymentSystem, OrderRegularPayment
from torgplace.settings import MAIN_PAYMENT_SYSTEM
from financial_system.tasks import send_event_message, perform_regular_payment
from financial_system.models import ExchangeRate
from financial_system.helpers.amount import get_operation_mode


def run():
    payment_system = get_obj_or_none(PaymentSystem, sys_code=MAIN_PAYMENT_SYSTEM)
    regular_payments = RegularPayment.objects.filter(active=1, date_next_payment=datetime.today())
    current_site = Site.objects.get_current()
    operation_mode = get_operation_mode()
    for regular_payment in regular_payments:
        order_regular_payment = OrderRegularPayment(regular_payment=regular_payment, amount=regular_payment.amount, commission=0, amount_with_commission=regular_payment.amount, currency=regular_payment.currency)
        order_regular_payment.save()
        if not operation_mode:
            send_event_message.delay(
                user=regular_payment.sender,
                action_type="regular_payment",
                message_type="not_funds_by_this_currency",
                subject=loader.render_to_string("account/regular_payment/event_message/not_operation_mode_subject.txt", {}),
                content=loader.render_to_string("account/regular_payment/event_message/not_operation_mode.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
            )
        else:
            user_balance = get_obj_or_none(UserBalance, user=regular_payment.sender, currency=regular_payment.currency, payment_coupon=None)
            if not user_balance:
                send_event_message.delay(
                    user=regular_payment.sender,
                    action_type="regular_payment",
                    message_type="not_funds_by_this_currency",
                    subject=loader.render_to_string("account/regular_payment/event_message/not_funds_by_this_currency_subject.txt", {}),
                    content=loader.render_to_string("account/regular_payment/event_message/not_funds_by_this_currency_content.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
                )
            else:
                exchange_rate = get_obj_or_none(ExchangeRate, iso_code="%s%s" % (regular_payment.currency.iso_code, regular_payment.currency.iso_code))
                billing_information = get_billing_information(regular_payment.amount, exchange_rate, exchange_rate, regular_payment.sender, payment_system, "regular_payment")
                if user_balance.state < billing_information.get("amount_with_commission", 0):
                    send_event_message.delay(
                        user=regular_payment.sender,
                        action_type="regular_payment",
                        message_type="insufficient_funds",
                        subject=loader.render_to_string("account/regular_payment/event_message/insufficient_funds_subject.txt", {}),
                        content=loader.render_to_string("account/regular_payment/event_message/insufficient_funds_content.html", {"regular_payment_id": regular_payment.id, "site_name": current_site})
                    )
                else:
                    order_regular_payment.commission = billing_information.get("commission", 0)
                    order_regular_payment.amount_with_commission = billing_information.get("amount_with_commission", 0)
                    order_regular_payment.paid = 1
                    order_regular_payment.save()
                    perform_regular_payment.delay(regular_payment, user_balance, billing_information, operation_mode, payment_system)

                regular_payment.date_last_payment = datetime.today()
                regular_payment.save()
        pass
    print "-----"


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option("--run", action="store_true", dest="run", help="Run regular payments"),
    )

    def handle(self, *args, **options):
        if options["run"]:
            run()
        else:
            print "Choose option: [--run]"
        print "Done"
